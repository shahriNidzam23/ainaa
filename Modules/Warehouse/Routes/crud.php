<?php

namespace Modules\Warehouse\Http\Controllers;

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::apiResources([
    'war-price' => WarPriceController::class,
    'war-product' => WarProductController::class,
    'war-stock' => WarStockController::class,
    'war-transfer-product' => WarTransferProductController::class,
    'war-transfer' => WarTransferController::class,
]);