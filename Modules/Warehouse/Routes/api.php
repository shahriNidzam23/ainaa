<?php

namespace Modules\Warehouse\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['as' => 'warehouse.', 'middleware' => ['auth:sanctum']], function () {
    Route::apiResources([
        'product' => WarProductController::class,
        'stock' => WarStockController::class,
        'transfer' => WarTransferController::class,
        'package' => WarPackageController::class,
    ]);
});


// require_once module_path('Warehouse', 'Routes/warehouse.php');
