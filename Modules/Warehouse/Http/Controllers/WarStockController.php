<?php

namespace Modules\Warehouse\Http\Controllers;

use Rekamy\LaravelCore\Crudable\Abstract\CrudController;
use Modules\Warehouse\Bloc\WarStockBloc;

class WarStockController extends CrudController
{
    protected $baseBloc;
    protected $moduleName = '';
    private $result;

    public function __construct(WarStockBloc $bloc)
    {
        // $this->registerPassportScopes();
        $this->baseBloc = $bloc;
        $this->moduleName = 'war_stocks';
    }

    public function registerPassportScopes()
    {
        $this->middleware('scope:war_stocks_index')->only('index');
        $this->middleware('scope:war_stocks_create')->only('store');
        $this->middleware('scope:war_stocks_show')->only('show');
        $this->middleware('scope:war_stocks_update')->only('update');
        $this->middleware('scope:war_stocks_destroy')->only('destroy');
    }
}
