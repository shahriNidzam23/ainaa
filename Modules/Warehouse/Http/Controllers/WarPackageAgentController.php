<?php

namespace Modules\Warehouse\Http\Controllers;

use Rekamy\LaravelCore\Crudable\Abstract\CrudController;
use Modules\Warehouse\Bloc\WarPackageAgentBloc;

class WarPackageAgentController extends CrudController
{
    protected $baseBloc;
    protected $moduleName = '';
    private $result;

    public function __construct(WarPackageAgentBloc $bloc) 
    {
        // $this->registerPassportScopes();
        $this->baseBloc = $bloc;
        $this->moduleName = 'war_package_agents';
    }

    public function registerPassportScopes()
    {
        $this->middleware('scope:war_package_agents_index')->only('index');
        $this->middleware('scope:war_package_agents_create')->only('store');
        $this->middleware('scope:war_package_agents_show')->only('show');
        $this->middleware('scope:war_package_agents_update')->only('update');
        $this->middleware('scope:war_package_agents_destroy')->only('destroy');
    }
}
