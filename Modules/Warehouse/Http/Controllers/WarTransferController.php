<?php

namespace Modules\Warehouse\Http\Controllers;

use Rekamy\LaravelCore\Crudable\Abstract\CrudController;
use Modules\Warehouse\Bloc\WarTransferBloc;

class WarTransferController extends CrudController
{
    protected $baseBloc;
    protected $moduleName = '';
    private $result;

    public function __construct(WarTransferBloc $bloc) 
    {
        // $this->registerPassportScopes();
        $this->baseBloc = $bloc;
        $this->moduleName = 'war_transfers';
    }

    public function registerPassportScopes()
    {
        $this->middleware('scope:war_transfers_index')->only('index');
        $this->middleware('scope:war_transfers_create')->only('store');
        $this->middleware('scope:war_transfers_show')->only('show');
        $this->middleware('scope:war_transfers_update')->only('update');
        $this->middleware('scope:war_transfers_destroy')->only('destroy');
    }
}
