<?php

namespace Modules\Warehouse\Http\Controllers;

use Rekamy\LaravelCore\Crudable\Abstract\CrudController;
use Modules\Warehouse\Bloc\WarTransferProductBloc;

class WarTransferProductController extends CrudController
{
    protected $baseBloc;
    protected $moduleName = '';
    private $result;

    public function __construct(WarTransferProductBloc $bloc) 
    {
        // $this->registerPassportScopes();
        $this->baseBloc = $bloc;
        $this->moduleName = 'war_transfer_products';
    }

    public function registerPassportScopes()
    {
        $this->middleware('scope:war_transfer_products_index')->only('index');
        $this->middleware('scope:war_transfer_products_create')->only('store');
        $this->middleware('scope:war_transfer_products_show')->only('show');
        $this->middleware('scope:war_transfer_products_update')->only('update');
        $this->middleware('scope:war_transfer_products_destroy')->only('destroy');
    }
}
