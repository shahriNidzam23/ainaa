<?php

namespace Modules\Warehouse\Http\Controllers;

use Rekamy\LaravelCore\Crudable\Abstract\CrudController;
use Modules\Warehouse\Bloc\WarProductBloc;

class WarProductController extends CrudController
{
    protected $baseBloc;
    protected $moduleName = '';
    private $result;

    public function __construct(WarProductBloc $bloc)
    {
        // $this->registerPassportScopes();
        $this->baseBloc = $bloc;
        $this->moduleName = 'war_products';
    }

    public function registerPassportScopes()
    {
        $this->middleware('scope:war_products_index')->only('index');
        $this->middleware('scope:war_products_create')->only('store');
        $this->middleware('scope:war_products_show')->only('show');
        $this->middleware('scope:war_products_update')->only('update');
        $this->middleware('scope:war_products_destroy')->only('destroy');
    }
}
