<?php

namespace Modules\Warehouse\Http\Controllers;

use Rekamy\LaravelCore\Crudable\Abstract\CrudController;
use Modules\Warehouse\Bloc\WarPackageBloc;

class WarPackageController extends CrudController
{
    protected $baseBloc;
    protected $moduleName = '';
    private $result;

    public function __construct(WarPackageBloc $bloc)
    {
        // $this->registerPassportScopes();
        $this->baseBloc = $bloc;
        $this->moduleName = 'war_packages';
    }

    public function registerPassportScopes()
    {
        $this->middleware('scope:war_packages_index')->only('index');
        $this->middleware('scope:war_packages_create')->only('store');
        $this->middleware('scope:war_packages_show')->only('show');
        $this->middleware('scope:war_packages_update')->only('update');
        $this->middleware('scope:war_packages_destroy')->only('destroy');
    }
}
