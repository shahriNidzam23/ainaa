<?php

namespace Modules\Warehouse\Http\Requests;

use Modules\Warehouse\Models\WarProduct;
use Modules\Warehouse\Bloc\WarProductBloc;
use Illuminate\Foundation\Http\FormRequest;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Rekamy\LaravelCore\Contracts\CrudableRequest;

class WarProductRequest extends FormRequest implements CrudableRequest
{
    public function model()
    {
        return WarProduct::class;
    }

    public function validateIndex()
    {
        /*
        if (!auth()->check()) throw new \Exception("Unauthorized Access", 401);
        $haveAccess = auth()->user()->can(WarProductBloc::permission('index'));
        if (!$haveAccess) throw new \Exception("Unauthorized Processing Request", 403);
        */
    }

    public function validateStore()
    {
        /*
        if (!auth()->check()) throw new \Exception("Unauthorized Access", 401);
        $haveAccess = auth()->user()->can(WarProductBloc::permission('create'));
        if (!$haveAccess) throw new \Exception("Unauthorized Processing Request", 403);
        */

        validator(request()->all(), WarProduct::rules('store'))->validate();
    }

    public function validateShow()
    {
        /*
        $haveAccess = auth()->user()->can(WarProductBloc::permission('show'));
        if (!$haveAccess) throw new \Exception("Unauthorized Processing Request", 403);
        */
    }

    public function validateUpdate()
    {
        /*
        $haveAccess = auth()->user()->can(WarProductBloc::permission('update'));
        if (!$haveAccess) throw new \Exception("Unauthorized Processing Request", 403);
        */
    }

    public function validateDestroy()
    {
        /*
        $haveAccess = auth()->user()->can(WarProductBloc::permission('destroy'));
        if (!$haveAccess) throw new \Exception("Unauthorized Processing Request", 403);
        */
    }

    public function rules()
    {
        return [];
        // return WarProduct::rules('default');
    }
}
