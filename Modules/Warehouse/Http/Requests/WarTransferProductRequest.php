<?php

namespace Modules\Warehouse\Http\Requests;

use Modules\Warehouse\Models\WarTransferProduct;
use Modules\Warehouse\Bloc\WarTransferProductBloc;
use Illuminate\Foundation\Http\FormRequest;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Rekamy\LaravelCore\Contracts\CrudableRequest;

class WarTransferProductRequest extends FormRequest implements CrudableRequest
{
    public function model() {
        return WarTransferProduct::class;
    }

    public function validateIndex()
    {
        /*
        if (!auth()->check()) throw new \Exception("Unauthorized Access", 401);
        $haveAccess = auth()->user()->can(WarTransferProductBloc::permission('index'));
        if (!$haveAccess) throw new \Exception("Unauthorized Processing Request", 403);
        */
    }

    public function validateStore()
    {
        /*
        if (!auth()->check()) throw new \Exception("Unauthorized Access", 401);
        $haveAccess = auth()->user()->can(WarTransferProductBloc::permission('create'));
        if (!$haveAccess) throw new \Exception("Unauthorized Processing Request", 403);
        */

        validator(request()->all(), WarTransferProduct::rules('store'))->validate();
    }

    public function validateShow()
    {
        /*
        $haveAccess = auth()->user()->can(WarTransferProductBloc::permission('show'));
        if (!$haveAccess) throw new \Exception("Unauthorized Processing Request", 403);
        */
    }

    public function validateUpdate()
    {
        /*
        $haveAccess = auth()->user()->can(WarTransferProductBloc::permission('update'));
        if (!$haveAccess) throw new \Exception("Unauthorized Processing Request", 403);
        */
    }

    public function validateDestroy()
    {
        /*
        $haveAccess = auth()->user()->can(WarTransferProductBloc::permission('destroy'));
        if (!$haveAccess) throw new \Exception("Unauthorized Processing Request", 403);
        */
    }

    public function rules()
    {
        return [];
        // return WarTransferProduct::rules('default');
    }
}
