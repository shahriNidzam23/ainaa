<?php

namespace Modules\Warehouse\Bloc;

use Rekamy\LaravelCore\Crudable\Abstract\CrudBloc;
use Modules\Warehouse\Repositories\WarPackageAgentRepository;
use Modules\Warehouse\Http\Requests\WarPackageAgentRequest;

class WarPackageAgentBloc extends CrudBloc
{
    public function __construct(
        public WarPackageAgentRepository $repo, 
        public WarPackageAgentRequest $request,
    ) {
        $this->repo = $repo;
        $this->request = $request;
    }

    public static function permission($name) {
    //     $permission = [
    //         'index' => 'war_package_agents_index',
    //         'create' => 'war_package_agents_create',
    //         'show' => 'war_package_agents_show',
    //         'update' => 'war_package_agents_update',
    //         'destroy' => 'war_package_agents_destroy',
    //     ];
        
    //     return $permission[$name];
    }
}
