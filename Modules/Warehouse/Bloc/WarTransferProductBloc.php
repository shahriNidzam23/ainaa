<?php

namespace Modules\Warehouse\Bloc;

use Rekamy\LaravelCore\Crudable\Abstract\CrudBloc;
use Modules\Warehouse\Repositories\WarTransferProductRepository;
use Modules\Warehouse\Http\Requests\WarTransferProductRequest;

class WarTransferProductBloc extends CrudBloc
{
    public function __construct(
        public WarTransferProductRepository $repo, 
        public WarTransferProductRequest $request,
    ) {
        $this->repo = $repo;
        $this->request = $request;
    }

    public static function permission($name) {
    //     $permission = [
    //         'index' => 'war_transfer_products_index',
    //         'create' => 'war_transfer_products_create',
    //         'show' => 'war_transfer_products_show',
    //         'update' => 'war_transfer_products_update',
    //         'destroy' => 'war_transfer_products_destroy',
    //     ];
        
    //     return $permission[$name];
    }
}
