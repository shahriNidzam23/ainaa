<?php

namespace Modules\Warehouse\Bloc;

use Rekamy\LaravelCore\Crudable\Abstract\CrudBloc;
use Modules\Warehouse\Repositories\WarTransferRepository;
use Modules\Warehouse\Http\Requests\WarTransferRequest;
use Modules\Common\Models\CommonRefNo;
use Modules\Warehouse\Models\{WarStock, WarTransfer};

class WarTransferBloc extends CrudBloc
{
    public function __construct(
        public WarTransferRepository $repo,
        public WarTransferRequest $request,
    ) {
        $this->repo = $repo;
        $this->request = $request;
    }

    public static function permission($name) {
    }

    public function store($input)
    {
        $input["batch_no"] = CommonRefNo::runningNo("warehouse.transfer");
        $transfer = $this->repo->storeAction($input);
        collect($input["products"])->each(function ($product) use ($input) {
            $stock_buyer = WarStock::where('user_id', $input['user_id'])
                ->where('product_id', $product['product_id'])->first();

            $quantity = $input["type"] == WarTransfer::TRANSFER_INBOUND ? $product['accepted_quantity'] : $product['accepted_quantity'] * -1;

            if ($stock_buyer) {
                $stock_buyer->update([
                    'quantity' => $stock_buyer->quantity + $quantity
                ]);
            } else {
                WarStock::create(
                    [
                        'product_id' => $product['product_id'],
                        'user_id' => $input['user_id'],
                        'quantity' => $quantity
                    ]
                );
            }

        });

        return $transfer;
    }
}
