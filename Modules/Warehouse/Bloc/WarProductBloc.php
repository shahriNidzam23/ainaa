<?php

namespace Modules\Warehouse\Bloc;

use Rekamy\LaravelCore\Crudable\Abstract\CrudBloc;
use Modules\Warehouse\Repositories\WarProductRepository;
use Modules\Warehouse\Http\Requests\WarProductRequest;
use Modules\Common\Bloc\CommonAttachmentBloc;
use DB;
use Modules\User\Models\User;
use Modules\Warehouse\Models\WarStock;

class WarProductBloc extends CrudBloc
{
    public function __construct(
        public WarProductRepository $repo,
        public WarProductRequest $request,
    ) {
        $this->repo = $repo;
        $this->request = $request;
    }

    public static function permission($name)
    {
        //     $permission = [
        //         'index' => 'war_products_index',
        //         'create' => 'war_products_create',
        //         'show' => 'war_products_show',
        //         'update' => 'war_products_update',
        //         'destroy' => 'war_products_destroy',
        //     ];

        //     return $permission[$name];
    }


    public function store($input)
    {
        DB::beginTransaction();
        try {
            abort_if(empty($input["stocks"]), 400, "Stocks cannot be empty");
            $input["stocks"][0]["user_id"] = User::hq()->id;
            $post = $this->repo->storeAction($input);
            if (request()->hasFile('attachments')) {
                $files = request()->file('attachments');
                CommonAttachmentBloc::save($files, [
                    "parent_type" => $this->repo->model(),
                    "parent_id" => $post->id,
                    "folder" => "product"
                ]);
            }

            DB::commit();

            return $post;
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }

    public function update($id, $input)
    {
        $product = $this->repo->updateAction($id, $input);


        if (!empty($input["stocks"])) {
            collect($input["stocks"])->each(function ($stock) use ($product) {
                $stock["user_id"] = User::hq()->id;
                if (empty($stock["id"])) {
                    $product->stocks()->create($stock);
                } else {
                    $product->stocks()->where("id", $stock["id"])->update($stock);
                }
            });
            $product->load("stocks");
        }

        if (request()->hasFile('attachment')) {
            $files = request()->file('attachment');
            CommonAttachmentBloc::save($files, [
                "parent_type" => $this->repo->model(),
                "parent_id" => $product->id,
                "folder" => "product"
            ]);
        }

        return $product;
    }
}
