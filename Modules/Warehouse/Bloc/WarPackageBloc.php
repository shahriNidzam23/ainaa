<?php

namespace Modules\Warehouse\Bloc;

use Rekamy\LaravelCore\Crudable\Abstract\CrudBloc;
use Modules\Warehouse\Repositories\WarPackageRepository;
use Modules\Warehouse\Http\Requests\WarPackageRequest;
use Modules\Warehouse\Models\WarPackage;

class WarPackageBloc extends CrudBloc
{
    public function __construct(
        public WarPackageRepository $repo,
        public WarPackageRequest $request,
    ) {
        $this->repo = $repo;
        $this->request = $request;
    }

    public static function permission($name)
    {
        //     $permission = [
        //         'index' => 'war_packages_index',
        //         'create' => 'war_packages_create',
        //         'show' => 'war_packages_show',
        //         'update' => 'war_packages_update',
        //         'destroy' => 'war_packages_destroy',
        //     ];

        //     return $permission[$name];
    }


    public function store($input)
    {
        if($input["is_all_agent"] && !empty($input["agents"])) {
            // Remove agents from payload if is_all_agent is true
            unset($input["agents"]);
        }
        return $this->repo->storeAction($input);
    }

    public function update($id, $input)
    {
        $package = WarPackage::with("product")
            ->findOrFail($id);

        if (
            !empty($input["product_id"]) &&
            !empty($input["quantity"]) &&
            $package->quantity != $input["quantity"]
        ) {
            WarPackage::validateQuantity($input["product_id"], $input["quantity"], $input["role"]);
        }

        $package =  $this->repo->updateAction($id, $input);

        if (!empty($input["agents"])) {
            collect($input["agents"])->each(function ($agent) use ($package) {
                if (empty($agent["id"])) {
                    $package->agents()->create($agent);
                } else {
                    $package->agents()->where("id", $agent["id"])->update($agent);
                }
            });
            $package->load("agents");
        }

        return $package;
    }
}
