<?php

namespace Modules\Warehouse\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Common\Database\Seeders\RefNoTableSeeder;

class WarehouseDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $this->getRefNo();
        $this->call([
            WarProductsSeeder::class,
            WarPackagesSeeder::class,
            WarStocksSeeder::class,
            WarehouseRolePermissionSeeder::class
        ]);
    }

    public function getRefNo()
    {
        RefNoTableSeeder::register([
            "warehouse.transfer" => "TRF/"
        ]);
    }
}
