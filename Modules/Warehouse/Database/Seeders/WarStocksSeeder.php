<?php

namespace Modules\Warehouse\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Agent\Models\Agent;

class WarStocksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $agent = \DB::table('agents')->where("code", config("app.hq.code"))->first();
        $stocksData = \DB::table('war_products')->get()->map(function ($product) use ($agent) {
            return [
                'id' => \Str::orderedUuid(),
                'product_id' => $product->id,
                'user_id' => $agent->user_id,
                'quantity' => 1000
            ];
        });

        \DB::table('war_stocks')->insert($stocksData->toArray());
    }
}
