<?php

namespace Modules\Warehouse\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Auth\Models\Role;

class WarPackagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = \DB::table('war_products')->get();
        $packages = $products->map(function ($product) {
            return [
                'id' => \Str::orderedUuid(),
                'product_id' => $product->id,
                'name' => "Set A (" . $product->name . ")",
                'quantity' => 50,
                'buying_price' => 10,
                'role' => 'master_stockist',
                'is_all_agent' => true
            ];
        });
        \DB::table('war_packages')->insert($packages->toArray());

        // collect($packages)->each(function ($package) {
        //     $agentData = \DB::table('agents')->whereNot("code", config("app.hq.code"))->get()->map(function ($agent) use ($package) {
        //         return [
        //             'id' => \Str::orderedUuid(),
        //             'package_id' => $package["id"],
        //             'user_id' => $agent->user_id,
        //         ];
        //     });

        //     \DB::table('war_package_agents')->insert($agentData->toArray());
        // });
    }
}
