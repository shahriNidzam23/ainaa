<?php

namespace Modules\Warehouse\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Warehouse\Repositories\WarProductRepository;
use DB;
use Illuminate\Http\UploadedFile;
use Modules\Common\Bloc\CommonAttachmentBloc;

class WarProductsSeeder extends Seeder
{
    private $repo;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $this->repo = app(WarProductRepository::class);

        $productsData = [
            [
                'id' => \Str::orderedUuid(),
                'name' => 'XYZ Sunscreen',
                'sku' => 'Sunscreen/00001',
                'cost' => 10,
                'selling_price' => 20,
                'image' => [
                    "Sunscreen.png"
                ],
            ],
            [
                'id' => \Str::orderedUuid(),
                'name' => 'XYZ Avocado Hydra Booster',
                'sku' => 'Booster/00001',
                'cost' => 10,
                'selling_price' => 20,
                'image' => [
                    "Booster.png"
                ],
            ],
            [
                'id' => \Str::orderedUuid(),
                'name' => 'XYZ Cleanser',
                'sku' => 'Cleanser/00001',
                'cost' => 10,
                'selling_price' => 20,
                'image' => [
                    "Cleanser.png"
                ],
            ],
            [
                'id' => \Str::orderedUuid(),
                'name' => 'XYZ Scrub',
                'sku' => 'Scrub/00001',
                'cost' => 10,
                'selling_price' => 20,
                'image' => [
                    "Scrub.png"
                ],
            ],
            [
                'id' => \Str::orderedUuid(),
                'name' => 'XYZ Serum',
                'sku' => 'Serum/00001',
                'cost' => 10,
                'selling_price' => 20,
                'image' => [
                    "Serum.png"
                ],
            ],
        ];
        collect($productsData)->each(function ($input) {
            $this->store($input);
        });

    }
    public function store($input)
    {
        DB::beginTransaction();
        try {
            $image = $input["image"];
            unset($input["image"]);
            $post = $this->repo->storeAction($input);
            $files = $this->uploadAttachment($image);
            CommonAttachmentBloc::save($files, [
                "parent_type" => $this->repo->model(),
                "parent_id" => $post->id,
                "folder" => "product"
            ]);

            DB::commit();

            return $post;
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }

    private function uploadAttachment($images) {
        return collect($images)->map(function ($image) {
            $file_path = base_path("storage/app/default/" . $image);
            $file_name = $image;
            $file_content = file_get_contents($file_path);
            $mime_type = mime_content_type($file_path);

            return new UploadedFile(
                // File contents
                $file_path,
                // File name
                $file_name,
                // Mime type
                $mime_type,
                // Error code (0 for no error)
                null,
                // Test mode flag (set to true to prevent actual file uploads)
                true
            );
        });
    }
}
