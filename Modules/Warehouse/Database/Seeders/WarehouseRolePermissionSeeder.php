<?php

namespace Modules\Warehouse\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Auth\Database\Seeders\RolePermissionSeeder;

class WarehouseRolePermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $access = [
            'superadmin' => $this->admin(),
            'hq' => $this->admin(),
            'admin' => $this->admin(),
            'store_keeper' => $this->admin(),
            'master_stockist' => $this->agent(),
            'stockist_gold' => $this->agent(),
            'stockist_silver' => $this->agent(),
            'stockist_bronze' => $this->agent(),
            'agent_gold' => $this->agent(),
            'agent_silver' => $this->agent(),
            'agent_bronze' => $this->agent(),
        ];

        RolePermissionSeeder::registerRP("Warehouse", $access);
    }

    private function agent()
    {
        return [
            "war_products:view",

            "war_transfers:view",
            "war_transfers:create",
            "war_transfers:update",

            "war_packages:view",
        ];
    }

    private function admin()
    {
        return [
            "war_products:view",
            "war_products:create",
            "war_products:update",
            "war_products:delete",

            "war_transfers:view",
            "war_transfers:create",
            "war_transfers:update",
            "war_transfers:delete",

            "war_packages:view",
            "war_packages:create",
            "war_packages:update",
            "war_packages:delete",
        ];
    }
}
