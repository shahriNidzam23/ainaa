<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('war_transfer_products', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->foreignUuid('transfer_id')->constrained('war_transfers')->onDelete('cascade');
            $table->foreignUuid('product_id')->constrained('war_products')->onDelete('cascade');
            $table->integer('receive_quantity')->default(0);
            $table->integer('rejected_quantity')->default(0);
            $table->integer('accepted_quantity')->default(0);
            $table->datetime('expiry_date')->nullable();
            $table->auditable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('war_asset_outbound');
    }
};
