<?php

namespace Modules\Warehouse\Models;

use Illuminate\Database\Eloquent\Relations\{BelongsTo};
use App\Models\BaseModel as Model;
use Modules\Agent\Models\Agent;
use Modules\User\Models\User;

class WarStock extends Model
{
    public $table = 'war_stocks';

    public $fillable = [
        'id',
        'product_id',
        'user_id',
        'quantity',
    ];

    public $casts = [
        'id' => 'string',
        'product_id' => 'string',
        'user_id' => 'string',
        'quantity' => 'integer',
    ];

    protected static function boot()
    {

        static::creating(function ($model) {
            self::validateUserStock($model->product_id, $model->user_id);
        });
        // Boot parent last to ensure traits run after BaseModel boot
        parent::boot();

    }

    public static function rules($scenario = 'default')
    {
        $rules['default'] = [
            'product_id' => 'required',
            'user_id' => 'required',
            'quantity' => 'required',
        ];
        $rules['store'] = [
            'product_id' => 'required',
            'user_id' => 'required',
            'quantity' => 'required',
        ];
        $rules['update'] = [
            'product_id' => 'required',
            'user_id' => 'required',
            'quantity' => 'required',
        ];
        return $rules[$scenario];
    }

    /**
     * Get war_stocks product
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product(): BelongsTo
    {
        return $this->belongsTo(WarProduct::class, 'product_id');
    }

    /**
     * Get war_stocks user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function agent(): BelongsTo
    {
        return $this->belongsTo(Agent::class, 'user_id', 'user_id');
    }

    public static function isSufficient($user_id, $product_id, $quantity)
    {

        try {
            $stock_seller = WarStock::where('user_id', $user_id)->where('product_id', $product_id)->first();
            if (empty($stock_seller)) throw new \Exception('Insufficient Stock');
            if ($stock_seller->quantity < $quantity) throw new \Exception('Insufficient Stock');
        } catch (\Throwable $th) {
            return abort(400, $th->getMessage());
        }
    }

    public function scopeLowStock($query)
    {
        $query
            ->where("user_id", auth()->user()->id)
            ->where('quantity', "<", 30)->orderBy('quantity');
    }


    public static function validateUserStock($product_id, $user_id) {
        $stock = WarStock::with(["product", "user"])
            ->where("user_id", $user_id)
            ->where("product_id", $product_id)
            ->first();

        if(!empty($stock)) {
            abort(400, $stock->product->name . " with Agent " . $stock->user->display_name . " already exist");
        }

    }
}
