<?php

namespace Modules\Warehouse\Models;

use Illuminate\Database\Eloquent\{SoftDeletes};
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\{BelongsTo, HasMany};
use Illuminate\Support\Str;
use App\Models\BaseModel as Model;


class WarTransferProduct extends Model
{
	public $table = 'war_transfer_products';

	public $fillable = [
		'id',
		'transfer_id',
		'product_id',
		'receive_quantity',
		'rejected_quantity',
		'accepted_quantity',
        'expiry_date'
	];

	public $casts = [
		'id' => 'string',
		'transfer_id' => 'string',
		'product_id' => 'string',
		'receive_quantity' => 'integer',
		'rejected_quantity' => 'integer',
		'accepted_quantity' => 'integer',
		'expiry_date' => 'date:Y-m-d H:i',
	];

	public static function rules($scenario = 'default') {
		$rules['default'] = [
			'transfer_id' => 'required',
			'product_id' => 'required',
			'receive_quantity' => 'required',
			'rejected_quantity' => 'required',
			'accepted_quantity' => 'required',
		];
		$rules['store'] = [
			'transfer_id' => 'required',
			'product_id' => 'required',
			'receive_quantity' => 'required',
			'rejected_quantity' => 'required',
			'accepted_quantity' => 'required',
		];
		$rules['update'] = [
			'transfer_id' => 'required',
			'product_id' => 'required',
			'receive_quantity' => 'required',
			'rejected_quantity' => 'required',
			'accepted_quantity' => 'required',
		];
		return $rules[$scenario];
	}

	/**
	 * Get war_transfer_products product
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function product() : BelongsTo
	{
	    return $this->belongsTo(WarProduct::class, 'product_id');
	}

	/**
	 * Get war_transfer_products transfer
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function transfer() : BelongsTo
	{
	    return $this->belongsTo(WarTransfer::class, 'transfer_id');
	}
}
