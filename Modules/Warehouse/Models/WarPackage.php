<?php

namespace Modules\Warehouse\Models;

use Illuminate\Database\Eloquent\{SoftDeletes};
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\{BelongsTo, HasMany};
use Illuminate\Support\Str;
use App\Models\BaseModel as Model;
use App\Traits\DataPolicy;

class WarPackage extends Model
{
    use DataPolicy;
    public $table = 'war_packages';

    const STATUS_ACTIVE = "active";
    const STATUS_DRAFT = "draft";
    public $fillable = [
        'id',
        'product_id',
        'quantity',
        'name',
        'buying_price',
        'role',
        'is_all_agent',
        'status',
    ];

    public $casts = [
        'id' => 'string',
        'product_id' => 'string',
        'quantity' => 'integer',
        'name' => 'string',
        'buying_price' => 'decimal:2',
        'status' => 'string',
    ];

    protected static function boot()
    {

        static::creating(function ($model) {
            self::validateQuantity($model->product_id, $model->quantity, $model->role);
        });
        // Boot parent last to ensure traits run after BaseModel boot
        parent::boot();
    }

    public static function rules($scenario = 'default')
    {
        $rules['default'] = [
            'product_id' => 'required',
            'quantity' => 'required',
            'name' => 'required',
            'buying_price' => 'required',
            'role' => 'required',
            'is_all_agent' => 'required',
        ];
        $rules['store'] = [
            'product_id' => 'required',
            'quantity' => 'required',
            'name' => 'required',
            'buying_price' => 'required',
            'role' => 'required',
            'is_all_agent' => 'required',
        ];
        $rules['update'] = [
            'product_id' => 'required',
            'quantity' => 'required',
            'name' => 'required',
            'buying_price' => 'required',
            'role' => 'required',
            'is_all_agent' => 'required',
        ];
        return $rules[$scenario];
    }

    /**
     * Get war_packages warPackageAgents
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function agents(): HasMany
    {
        return $this->hasMany(WarPackageAgent::class, 'package_id');
    }

    /**
     * Get war_packages product
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product(): BelongsTo
    {
        return $this->belongsTo(WarProduct::class, 'product_id');
    }

    public static function validateQuantity($product_id, $quantity, $role)
    {
        $package = WarPackage::with("product")
            ->where("quantity", $quantity)
            ->where("product_id", $product_id)
            ->where("role", $role)
            ->first();


        if (!empty($package)) {
            abort(400, $package->product->name . " with quantity " . $quantity . " already exist");
        }
    }

    public function scopeActive($query)
    {
        return $query->where("status", WarPackage::STATUS_ACTIVE);
    }
}
