<?php

namespace Modules\Warehouse\Models;

use Illuminate\Database\Eloquent\{SoftDeletes};
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\{BelongsTo, HasMany};
use Illuminate\Support\Str;
use App\Models\BaseModel as Model;


class WarPrice extends Model
{
	public $table = 'war_prices';

	public $fillable = [
		'id',
		'product_id',
		'code',
		'value',
		'active',
	];

	public $casts = [
		'id' => 'string',
		'product_id' => 'string',
		'code' => 'string',
		'value' => 'decimal:2',
		'active' => 'boolean',
	];

	public static function rules($scenario = 'default')
	{
		$rules['default'] = [
			'product_id' => 'required',
			'code' => 'required',
			'value' => 'required',
			'active' => 'required',
		];
		$rules['store'] = [
			'product_id' => 'required',
			'code' => 'required',
			'value' => 'required',
			'active' => 'required',
		];
		$rules['update'] = [
			'product_id' => 'required',
			'code' => 'required',
			'value' => 'required',
			'active' => 'required',
		];
		return $rules[$scenario];
	}

	/**
	 * Get war_prices product
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function product(): BelongsTo
	{
		return $this->belongsTo(WarProduct::class, 'product_id');
	}
}
