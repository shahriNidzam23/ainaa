<?php

namespace Modules\Warehouse\Models;

use Illuminate\Database\Eloquent\{SoftDeletes};
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\{BelongsTo, HasMany};
use Illuminate\Support\Str;
use App\Models\BaseModel as Model;


class WarPackageAgent extends Model
{
	public $table = 'war_package_agents';

	public $fillable = [
		'id',
		'package_id',
		'user_id',
	];

	public $casts = [
		'id' => 'string',
		'package_id' => 'string',
		'user_id' => 'string',
	];

	public static function rules($scenario = 'default') {
		$rules['default'] = [
			'package_id' => 'required',
			'user_id' => 'required',
		];
		$rules['store'] = [
			'package_id' => 'required',
			'user_id' => 'required',
		];
		$rules['update'] = [
			'package_id' => 'required',
			'user_id' => 'required',
		];
		return $rules[$scenario];
	}

	/**
	 * Get war_package_agents package
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function package() : BelongsTo
	{
	    return $this->belongsTo(WarPackage::class, 'package_id');
	}

	/**
	 * Get war_package_agents user
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function user() : BelongsTo
	{
	    return $this->belongsTo(User::class, 'user_id');
	}
}
