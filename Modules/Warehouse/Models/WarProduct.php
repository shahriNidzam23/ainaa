<?php

namespace Modules\Warehouse\Models;

use Illuminate\Database\Eloquent\Relations\{HasMany};
use App\Models\BaseModel as Model;
use App\Traits\DataPolicy;
use Modules\Common\Models\CommonAttachment;
use DB;
use Modules\Agent\Models\Agent;
use Modules\Order\Models\OrdOrder;
use Modules\Order\Models\OrdSale;
use Modules\User\Models\User;

class WarProduct extends Model
{
    use DataPolicy;

    public $table = 'war_products';

    const STATUS_ACTIVE = "active";
    const STATUS_DRAFT = "draft";

    public $fillable = [
        'id',
        'name',
        'sku',
        'cost',
        'selling_price',
        'description',
        'status',
    ];

    public $casts = [
        'id' => 'string',
        'name' => 'string',
        'sku' => 'string',
        'description' => 'string',
        'status' => 'string',
    ];


    public static function rules($scenario = 'default')
    {
        $rules['default'] = [
            'name' => 'required',
            'sku' => 'required',
            'selling_price' => 'required',
        ];
        $rules['store'] = [
            'name' => 'required',
            'sku' => 'required',
            'selling_price' => 'required',
        ];
        $rules['update'] = [
            'name' => 'required',
            'sku' => 'required',
            'selling_price' => 'required',
        ];
        return $rules[$scenario];
    }

    /**
     * Get war_products warStocks
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function stocks(): HasMany
    {
        return $this->hasMany(WarStock::class, 'product_id');
    }

    public function attachments()
    {
        return $this->morphMany(CommonAttachment::class, 'parent');
    }

    public function packages()
    {
        return $this->hasMany(WarPackage::class, 'product_id');
    }

    public function scopeTopProduct($query)
    {
        return $query->select('war_products.*')->selectSub(function ($query) {
            $query
                ->select(DB::raw('COALESCE(SUM(quantity), 0)'))
                ->from('ord_products')
                ->leftJoin("ord_orders", 'ord_orders.id', 'ord_products.parent_id')
                ->leftJoin("ord_sales", 'ord_sales.id', 'ord_products.parent_id')
                ->whereColumn('ord_products.sku', 'war_products.sku')
                ->where(function ($query) {
                    $query->orWhere("ord_orders.status", OrdOrder::STATUS_COLLECTED)
                        ->orWhere("ord_sales.status", OrdSale::STATUS_PAID);
                });
        }, 'total_sold')
            ->orderByDesc('total_sold')
            ->take(3);
    }

    public function scopeRecomended($query)
    {
        return $query->whereHas("stocks", function ($query) {
            $query
                ->whereHas("agent", function ($query) {
                    $query->where("code", config("app.hq.code"));
                })
                ->where('quantity', "<", 30)
                ->orderBy('quantity');
        })->take(3);
    }

    public function scopeFilterDetail($query)
    {
        $user = auth()->user();
        $user->load("agent.upline");
        return $query->with([
            "stocks" => function ($query) use ($user) {
                $query->whereIn('user_id', [$user->id, $user->agent->upline_id]);
            },
            "packages" => function ($query) use ($user) {
                $query
                    ->whereIn("role", $user->getRoleNames()->toArray())
                    ->where(function ($query) use ($user){
                        $query
                            ->orWhere("is_all_agent", true)
                            ->orWhereHas("agents", function ($query) use ($user) {
                                $query->where('user_id', $user->id);
                            });
                    })
                    ->where("status", WarPackage::STATUS_ACTIVE);
            },
        ]);
    }

    public function scopeMyStock($query)
    {
        $user = auth()->user();
        return $query->with([
            "stocks" => function ($query) use ($user) {
                $query->where('user_id', $user->id);
            },
            "packages" => function ($query) use ($user) {
                $query
                    ->whereIn("role", $user->getRoleNames()->toArray())
                    ->where(function ($query) use ($user) {
                        $query
                            ->orWhere("is_all_agent", true)
                            ->orWhereHas("agents", function ($query) use ($user) {
                                $query->where('user_id', $user->id);
                            });
                    })
                    ->where("status", WarPackage::STATUS_ACTIVE);
            },
        ])->whereHas("stocks", function ($query) use ($user) {
            $query->where('user_id', $user->id);
        });
    }

    public function scopeHqStock($query)
    {
        return $query->with([
            "stocks" => function ($query) {
                $query->where('user_id', User::hq()->id);
            },
            "attachments"
        ]);
    }

    public function scopeActive($query)
    {
        return $query
            ->where("status", WarProduct::STATUS_ACTIVE)
            ->with([
                "packages" => function ($query) {
                    return $query->where("status", WarPackage::STATUS_ACTIVE);
                }
            ]);
    }

    public function scopeWithQuantity($query)
    {
        $agent = Agent::where("code", config("app.hq.code"))->first();
        return $query
            ->select("war_products.*")
            ->selectSub(function ($query) use ($agent) {
                $query->select("quantity")
                    ->from('war_stocks')
                    ->whereColumn('war_stocks.product_id', 'war_products.id')
                    ->where('war_stocks.user_id', $agent->user_id)
                    ->limit(1);
            }, 'quantity');
    }
}
