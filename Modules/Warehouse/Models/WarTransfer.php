<?php

namespace Modules\Warehouse\Models;

use Illuminate\Database\Eloquent\{SoftDeletes};
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\{BelongsTo, HasMany};
use Illuminate\Support\Str;
use App\Models\BaseModel as Model;
use App\Traits\DataPolicy;

class WarTransfer extends Model
{
    use DataPolicy;

    public $table = 'war_transfers';

    public const TRANSFER_INBOUND = "inbound";
    public const TRANSFER_OUTBOUND = "outbound";

    public $fillable = [
        'id',
        'user_id',
        'type',
        'batch_no',
        'arrival'
    ];

    public $casts = [
        'id' => 'string',
        'user_id' => 'string',
        'batch_no' => 'string',
        'arrival' => 'date:Y-m-d H:i'
    ];

    public static function rules($scenario = 'default')
    {
        $rules['default'] = [
            'user_id' => 'required',
            'arrival' => 'required',
        ];
        $rules['store'] = [
            'user_id' => 'required',
            'arrival' => 'required',
        ];
        $rules['update'] = [
            'user_id' => 'required',
            'arrival' => 'required',
        ];
        return $rules[$scenario];
    }

    public $appends = [
        'total_received',
        'total_accept',
    ];

    public function products(): HasMany
    {
        return $this->hasMany(WarTransferProduct::class, 'transfer_id');
    }

    public function batchProducts()
    {
        return $this->hasManyThrough(WarTransferProduct::class, WarTransfer::class, 'batch_no', 'transfer_id', 'batch_no', 'id');
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function getTotalReceivedAttribute()
    {
        return empty($this->id) ? $this->batchProducts->sum("receive_quantity") : $this->products->sum("receive_quantity");
    }

    public function getTotalAcceptAttribute()
    {
        return empty($this->id) ? $this->batchProducts->sum("accepted_quantity") : $this->products->sum("accepted_quantity");
    }

    public function scopeTable($query)
    {
        $productId = request()->get("table::product_id");
        $batchNo = request()->get("table::batch_no");
        $start_arrival = request()->get("table::start");
        $end_arrival = request()->get("table::end");
        $query
            ->with("batchProducts")
            ->select("batch_no")
            ->selectSub(function ($query) {
                $query
                    ->select("users.display_name")
                    ->from('war_transfers as wti')
                    ->join('users', 'users.id', '=', 'wti.user_id')
                    ->where('wti.type', 'inbound')
                    ->whereColumn('wti.batch_no', 'war_transfers.batch_no')
                    ->limit(1);
            }, 'destination')
            ->selectSub(function ($query) {
                $query
                    ->select("users.display_name")
                    ->from('war_transfers as wto')
                    ->join('users', 'users.id', '=', 'wto.user_id')
                    ->where('wto.type', 'outbound')
                    ->whereColumn('wto.batch_no', 'war_transfers.batch_no')
                    ->limit(1);
            }, 'source')
            ->selectSub(function ($query) {
                $query->select("arrival")
                    ->from('war_transfers as wta')
                    ->whereColumn('wta.batch_no', 'war_transfers.batch_no')
                    ->limit(1);
            }, 'arrival');

        if (!empty($batchNo)) {
            $query->where("batch_no", $batchNo);
        }

        if (!empty($productId)) {
            $query->whereHas("batchProducts", function ($query) use ($productId) {
                $query->where("product_id", $productId);
            });
        }

        if (!empty($start_arrival) && !empty($end_arrival)) {
            $query->where(function ($query) use ($start_arrival, $end_arrival) {
                $query->whereRaw('arrival >= ?', $start_arrival);
                $query->whereRaw('arrival <= ?', $end_arrival);
            });
        }

        $query->where(function($query) {
            $query->where(function ($query) {
                $query
                    ->select("users.display_name")
                    ->from('war_transfers as wtic')
                    ->join('users', 'users.id', '=', 'wtic.user_id')
                    ->where('wtic.type', 'inbound')
                    ->whereColumn('wtic.batch_no', 'war_transfers.batch_no')
                    ->limit(1);
            }, 'HQ');
            $query->orWhere(function ($query) {
                $query
                    ->select("users.display_name")
                    ->from('war_transfers as wtoc')
                    ->join('users', 'users.id', '=', 'wtoc.user_id')
                    ->where('wtoc.type', 'outbound')
                    ->whereColumn('wtoc.batch_no', 'war_transfers.batch_no')
                    ->limit(1);
            }, 'HQ');
        });
        $query
            ->orderBy('arrival')
            ->distinct();
    }
}
