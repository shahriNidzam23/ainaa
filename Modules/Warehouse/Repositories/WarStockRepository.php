<?php

namespace Modules\Warehouse\Repositories;

use Rekamy\LaravelCore\Crudable\Concern\CrudableRepository;
use Rekamy\LaravelCore\Contracts\CrudableRepository as CrudableRepositoryContract;
use Modules\Warehouse\Models\WarStock;
use Rekamy\LaravelCore\Override\Repository;

/**
 * Class WarStockRepository
 * @package Modules\Warehouse\Repositories
 *
 * @method WarStock find($id, $columns = ['*'])
 * @method WarStock find($id, $columns = ['*'])
 * @method WarStock first($columns = ['*'])
 */
class WarStockRepository extends Repository implements CrudableRepositoryContract
{
    use CrudableRepository;

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'product_id',
        'user_id',
        'quantity'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return WarStock::class;
    }
}
