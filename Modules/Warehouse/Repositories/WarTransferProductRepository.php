<?php

namespace Modules\Warehouse\Repositories;

use Rekamy\LaravelCore\Crudable\Concern\CrudableRepository;
use Rekamy\LaravelCore\Contracts\CrudableRepository as CrudableRepositoryContract;
use Modules\Warehouse\Models\WarTransferProduct;
use Rekamy\LaravelCore\Override\Repository;

/**
 * Class WarTransferProductRepository
 * @package Modules\Warehouse\Repositories
 *
 * @method WarTransferProduct find($id, $columns = ['*'])
 * @method WarTransferProduct find($id, $columns = ['*'])
 * @method WarTransferProduct first($columns = ['*'])
*/
class WarTransferProductRepository extends Repository implements CrudableRepositoryContract
{
    use CrudableRepository;

    /**
     * @var array
     */
    protected $fieldSearchable = [
		'transfer_id',
		'product_id',
		'receive_quantity',
		'rejected_quantity',
		'accepted_quantity',
	];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return WarTransferProduct::class;
    }
    
}

