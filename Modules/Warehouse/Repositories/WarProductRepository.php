<?php

namespace Modules\Warehouse\Repositories;

use Rekamy\LaravelCore\Crudable\Concern\CrudableRepository;
use Rekamy\LaravelCore\Contracts\CrudableRepository as CrudableRepositoryContract;
use Modules\Warehouse\Models\WarProduct;
use Rekamy\LaravelCore\Override\Repository;

/**
 * Class WarProductRepository
 * @package Modules\Warehouse\Repositories
 *
 * @method WarProduct find($id, $columns = ['*'])
 * @method WarProduct find($id, $columns = ['*'])
 * @method WarProduct first($columns = ['*'])
 */
class WarProductRepository extends Repository implements CrudableRepositoryContract
{
    use CrudableRepository;

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'sku',
        'quantity',
        'status',
        'stocks.user_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return WarProduct::class;
    }
}
