<?php

namespace Modules\Warehouse\Repositories;

use Rekamy\LaravelCore\Crudable\Concern\CrudableRepository;
use Rekamy\LaravelCore\Contracts\CrudableRepository as CrudableRepositoryContract;
use Modules\Warehouse\Models\WarPackage;
use Rekamy\LaravelCore\Override\Repository;

/**
 * Class WarPackageRepository
 * @package Modules\Warehouse\Repositories
 *
 * @method WarPackage find($id, $columns = ['*'])
 * @method WarPackage find($id, $columns = ['*'])
 * @method WarPackage first($columns = ['*'])
*/
class WarPackageRepository extends Repository implements CrudableRepositoryContract
{
    use CrudableRepository;

    /**
     * @var array
     */
    protected $fieldSearchable = [
		'product_id',
		'quantity',
		'name',
		'buying_price',
        'status'
	];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return WarPackage::class;
    }

}

