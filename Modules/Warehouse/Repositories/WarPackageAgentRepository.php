<?php

namespace Modules\Warehouse\Repositories;

use Rekamy\LaravelCore\Crudable\Concern\CrudableRepository;
use Rekamy\LaravelCore\Contracts\CrudableRepository as CrudableRepositoryContract;
use Modules\Warehouse\Models\WarPackageAgent;
use Rekamy\LaravelCore\Override\Repository;

/**
 * Class WarPackageAgentRepository
 * @package Modules\Warehouse\Repositories
 *
 * @method WarPackageAgent find($id, $columns = ['*'])
 * @method WarPackageAgent find($id, $columns = ['*'])
 * @method WarPackageAgent first($columns = ['*'])
*/
class WarPackageAgentRepository extends Repository implements CrudableRepositoryContract
{
    use CrudableRepository;

    /**
     * @var array
     */
    protected $fieldSearchable = [
		'package_id',
		'user_id',
	];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return WarPackageAgent::class;
    }
    
}

