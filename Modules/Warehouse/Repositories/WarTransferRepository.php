<?php

namespace Modules\Warehouse\Repositories;

use Rekamy\LaravelCore\Crudable\Concern\CrudableRepository;
use Rekamy\LaravelCore\Contracts\CrudableRepository as CrudableRepositoryContract;
use Modules\Warehouse\Models\WarTransfer;
use Rekamy\LaravelCore\Override\Repository;

/**
 * Class WarTransferRepository
 * @package Modules\Warehouse\Repositories
 *
 * @method WarTransfer find($id, $columns = ['*'])
 * @method WarTransfer find($id, $columns = ['*'])
 * @method WarTransfer first($columns = ['*'])
*/
class WarTransferRepository extends Repository implements CrudableRepositoryContract
{
    use CrudableRepository;

    /**
     * @var array
     */
    protected $fieldSearchable = [
		'user_id',
		'batch_no',
		'arrival',
        'type',
        'products.product.name'
	];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return WarTransfer::class;
    }

}

