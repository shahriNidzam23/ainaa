<?php

namespace Modules\Warehouse\Listeners;

use Modules\Warehouse\Models\{WarStock, WarTransfer};
use Exception;
use Modules\Order\Models\OrdOrder;

class OrderTransferListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        if (
            WarTransfer::where("batch_no", $event->order->ref_no)->exists() || $event->order->status != OrdOrder::STATUS_COLLECTED
        ) return;
        $event->order->products->each(fn ($product) => $product->load(['detail', 'package']));
        $this->updateStock($event);
        $buyer_id = $event->order->buyer_id;
        $seller_id = $event->order->seller_id;
        $this->createTransfer($event, WarTransfer::TRANSFER_INBOUND, $buyer_id);
        $this->createTransfer($event, WarTransfer::TRANSFER_OUTBOUND, $seller_id);
    }

    private function updateStock($event)
    {
        $order = $event->order;
        $event->order->products->each(function ($product) use ($order){
            //update seller
            $stock_seller = WarStock::where(
                'user_id',
                $order->seller_id
            )->where('product_id', $product->detail->id)->first();
            $quantity = $product->quantity * $product->package->quantity;
            WarStock::isSufficient($order->seller_id, $product->detail->id, $quantity);


            $stock_seller->update([
                'quantity' => $stock_seller->quantity - $quantity
            ]);

            //update or create stock for buyer
            $stock_buyer = WarStock::where(
                'user_id',
                $order->buyer_id
            )->where('product_id', $product->detail->id)->first();

            try {
                if (empty($stock_buyer)) throw new Exception('Create Stock');


                $stock_buyer->update([
                    'quantity' => $stock_buyer->quantity + $quantity
                ]);
            } catch (\Throwable $th) {
                WarStock::create(
                    [
                        'product_id' => $product->detail->id,
                        'user_id' => $order->buyer_id,
                        'quantity' => $quantity
                    ]
                );
            }

        });
    }

    private function createTransfer($event, $type, $user_id)
    {
        //insert inbound
        $order = $event->order;
        $transfer = WarTransfer::create(
            [
                'batch_no' => $order->ref_no,
                'user_id' => $user_id,
                'arrival' => now(),
                "type" => $type
            ]
        );

        $products = $event->order->products->map(function ($product){
            return [
                'product_id' => $product->detail->id,
                'receive_quantity' => $product->quantity,
                'rejected_quantity' => 0,
                'accepted_quantity' => $product->quantity,
            ];
        });
        $transfer->products()->createMany($products->toArray());

    }
}
