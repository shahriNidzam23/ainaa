<?php

namespace Modules\Warehouse\Listeners;

use Modules\Warehouse\Models\WarTransfer;
use Modules\Warehouse\Models\{WarStock, WarProduct};
use Exception;
use Modules\Order\Models\OrdSale;

class SaleTransferListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {

        $ifExist = WarTransfer::where("batch_no", $event->orderSale->ref_no)->exists();
        if ($event->orderSale->status == OrdSale::STATUS_CANCEL && $ifExist) {
            $this->updateStock($event->previous, true);
            WarTransfer::where("batch_no", $event->orderSale->ref_no)->delete();
        }
        if ($event->orderSale->status == OrdSale::STATUS_PAID && !$ifExist) {
            $event->orderSale->products->each(fn ($product) => $product->load('detail'));
            $this->updateStock($event->orderSale);
            $this->createOutboundTransfer($event);
        }
    }

    private function updateStock($sale, $isReverted = false)
    {
        $sale->products->each(function ($saleProduct) use ($sale, $isReverted) {
            // Deduct if reverted
            $quantity = $saleProduct->quantity * ($isReverted ? -1 : 1);

            //update seller
            $stock_seller = WarStock::where(
                'user_id',
                $sale->user_id
            )->where('product_id', $saleProduct->detail->id)->first();


            WarStock::isSufficient($sale->user_id, $saleProduct->detail->id, $quantity);

            $stock_seller->update([
                'quantity' => $stock_seller->quantity - $quantity
            ]);

            $product = WarProduct::where("id", $saleProduct->detail->id)->first();
            $product->update([
                'quantity' => $product->quantity - $quantity,
            ]);
        });
    }

    private function createOutboundTransfer($event)
    {
        $sale = $event->orderSale;
        $transfer = WarTransfer::create(
            [
                'batch_no' => $sale->ref_no,
                'user_id' => $sale->user_id,
                'arrival' => now(),
                "type" => WarTransfer::TRANSFER_OUTBOUND
            ]
        );

        $products = $event->orderSale->products->map(function ($product) {
            return [
                'product_id' => $product->detail->id,
                'receive_quantity' => $product->quantity,
                'rejected_quantity' => 0,
                'accepted_quantity' => $product->quantity,
            ];
        });
        $transfer->products()->createMany($products->toArray());
    }
}
