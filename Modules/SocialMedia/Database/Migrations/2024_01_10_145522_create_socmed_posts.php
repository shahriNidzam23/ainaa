<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // - id
        // - url_posting
        // - screenshot
        // - social_media
        //     - Facebook
        //     - Instagram
        //     - X
        //     - TikTok
        // - Data
        // - Social media username
        Schema::create('socmed_posts', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('url_posting');
            $table->string('social_media');
            $table->string('username')->nullable();
            $table->auditable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('');
    }
};
