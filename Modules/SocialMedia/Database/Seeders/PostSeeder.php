<?php

namespace Modules\SocialMedia\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\SocialMedia\Models\SocmedPost;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        collect([
            [
                "id" => \Str::orderedUuid(),
                "url_posting" => "https://example.com/post_a",
                "social_media" => "Instagram",
                "username" => "@example",
                "created_by" => \DB::table('users')->first()->id
            ]
        ])->each(function ($row) {
            \DB::table('socmed_posts')->insert([$row]);

            \DB::table("common_attachments")->insert([
                [
                    'id' => \Str::orderedUuid(),
                    'parent_type' => SocmedPost::class,
                    'parent_id' => $row["id"],
                    'name' => "default",
                    'path' => "default/default.png",
                    'created_at' => now(),
                    'created_by' => \DB::table('users')->first()->id
                ]
            ]);
        });

    }
}
