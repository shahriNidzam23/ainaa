<?php

namespace Modules\SocialMedia\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Auth\Database\Seeders\RolePermissionSeeder;

class SocialMediaRolePermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $access = [
            'superadmin' => [
                "socmed_posts:view",
                "socmed_posts:create",
                "socmed_posts:update",
                "socmed_posts:delete"
            ],
            'hq' => [
                "socmed_posts:view",
                "socmed_posts:create",
                "socmed_posts:update",
                "socmed_posts:delete"
            ],
            'admin' => [
                "socmed_posts:view",
                "socmed_posts:create",
                "socmed_posts:update",
                "socmed_posts:delete"
            ],
            'master_stockist' => $this->agent(),
            'stockist_gold' => $this->agent(),
            'stockist_silver' => $this->agent(),
            'stockist_bronze' => $this->agent(),
            'agent_gold' => $this->agent(),
            'agent_silver' => $this->agent(),
            'agent_bronze' => $this->agent(),
        ];

        RolePermissionSeeder::registerRP("Social Media", $access);
    }

    private function agent() {
        return [
            "socmed_posts:view:created_by",
            "socmed_posts:create:created_by",
            "socmed_posts:update:created_by",
            "socmed_posts:delete:created_by"
        ];
    }
}
