<?php

namespace Modules\SocialMedia\Http\Controllers;

use Rekamy\LaravelCore\Crudable\Abstract\CrudController;
use Modules\SocialMedia\Bloc\SocmedPostBloc;
use Illuminate\Http\Request;
use DB;

class SocmedPostController extends CrudController
{
    protected $baseBloc;
    protected $moduleName = '';
    private $result;

    public function __construct(SocmedPostBloc $bloc)
    {
        // $this->registerPassportScopes();
        $this->baseBloc = $bloc;
        $this->moduleName = 'socmed_posts';
    }

    public function registerPassportScopes()
    {
        $this->middleware('scope:socmed_posts_index')->only('index');
        $this->middleware('scope:socmed_posts_create')->only('store');
        $this->middleware('scope:socmed_posts_show')->only('show');
        $this->middleware('scope:socmed_posts_update')->only('update');
        $this->middleware('scope:socmed_posts_destroy')->only('destroy');
    }

    public function bulk(Request $request)
    {
        DB::beginTransaction();
        try {

            $this->result = $this->baseBloc->bulk($request->all());

            DB::commit();
            return $this->success('Succesfull Insert Data', $this->result);
        } catch (\Throwable $th) {
            DB::rollBack();
            throw $th;
        }
    }
}
