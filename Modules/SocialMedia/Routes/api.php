<?php

namespace Modules\SocialMedia\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::middleware(['auth:sanctum'])->group(function () {
    Route::post('post/bulk', [SocmedPostController::class, 'bulk'])->name('post.bulk');
    Route::apiResources([
        'post' => SocmedPostController::class,
    ]);
});

// require_once module_path('SocialMedia', 'Routes/socialmedia.php');
