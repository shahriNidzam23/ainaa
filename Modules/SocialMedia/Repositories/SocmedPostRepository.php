<?php

namespace Modules\SocialMedia\Repositories;

use Rekamy\LaravelCore\Crudable\Concern\CrudableRepository;
use Rekamy\LaravelCore\Contracts\CrudableRepository as CrudableRepositoryContract;
use Modules\SocialMedia\Models\SocmedPost;
use Rekamy\LaravelCore\Override\Repository;

/**
 * Class SocmedPostRepository
 * @package Modules\SocialMedia\Repositories
 *
 * @method SocmedPost find($id, $columns = ['*'])
 * @method SocmedPost find($id, $columns = ['*'])
 * @method SocmedPost first($columns = ['*'])
*/
class SocmedPostRepository extends Repository implements CrudableRepositoryContract
{
    use CrudableRepository;

    /**
     * @var array
     */
    protected $fieldSearchable = [
		'url_posting',
		'social_media',
		'username',
        'created_by'
	];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return SocmedPost::class;
    }

}

