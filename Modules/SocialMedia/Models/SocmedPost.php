<?php
namespace Modules\SocialMedia\Models;

use Illuminate\Database\Eloquent\{SoftDeletes};
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\{BelongsTo, HasMany};
use Illuminate\Support\Str;
use App\Models\BaseModel as Model;
use App\Traits\DataPolicy;
use Modules\Common\Models\CommonAttachment;
use Modules\User\Models\User;

class SocmedPost extends Model
{
    use DataPolicy;

	public $table = 'socmed_posts';

	public $fillable = [
		'id',
		'url_posting',
		'social_media',
		'username',
	];

	public $casts = [
		'id' => 'string',
		'url_posting' => 'string',
		'social_media' => 'string',
		'username' => 'string',
	];

	public static function rules($scenario = 'default') {
		$rules['default'] = [
			'url_posting' => 'required',
			'social_media' => 'required',
			'username' => 'required',
		];
		$rules['store'] = [
			'url_posting' => 'required',
			'social_media' => 'required',
			'username' => 'required',
		];
		$rules['update'] = [
			'url_posting' => 'required',
			'social_media' => 'required',
			'username' => 'required',
		];
		return $rules[$scenario];
	}

    public function attachments()
    {
        return $this->morphMany(CommonAttachment::class, 'parent');
    }

    public function user(){
        return $this->belongsTo(User::class, 'created_by');
    }

    public function topPlatform() {
        if(!$this->exists()) return "";
        $top = $this->select(["social_media", \DB::raw("COUNT(social_media) As occurence")])->groupBy('social_media')->orderByDesc('occurence')->first();
		return $top->social_media;
    }
}
