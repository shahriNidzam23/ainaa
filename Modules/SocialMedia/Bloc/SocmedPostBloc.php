<?php

namespace Modules\SocialMedia\Bloc;

use Rekamy\LaravelCore\Crudable\Abstract\CrudBloc;
use Modules\SocialMedia\Repositories\SocmedPostRepository;
use Modules\SocialMedia\Http\Requests\SocmedPostRequest;
use Modules\Common\Bloc\CommonAttachmentBloc;
use DB;

class SocmedPostBloc extends CrudBloc
{
    public function __construct(
        public SocmedPostRepository $repo,
        public SocmedPostRequest $request,
    ) {
        $this->repo = $repo;
        $this->request = $request;
    }

    public static function permission($name) {
    //     $permission = [
    //         'index' => 'socmed_posts_index',
    //         'create' => 'socmed_posts_create',
    //         'show' => 'socmed_posts_show',
    //         'update' => 'socmed_posts_update',
    //         'destroy' => 'socmed_posts_destroy',
    //     ];

    //     return $permission[$name];
    }



    public function store($input)
    {
        DB::beginTransaction();
        try {
            $post = $this->repo->storeAction($input);

            if (request()->hasFile('attachments')) {
                $files = request()->file('attachments');
                CommonAttachmentBloc::save($files, [
                    "parent_type" => $this->repo->model(),
                    "parent_id" => $post->id,
                    "folder" => "socmed"
                ]);
            }
            DB::commit();

            return $post;
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }



    public function bulk($inputs)
    {
        DB::beginTransaction();
        try {
            $posts = collect($inputs["payload"])->map(function($input, $key){
                $post = $this->repo->storeAction($input);

                if (request()->hasFile("payload.$key.attachments")) {
                    $files = request()->file("payload.$key.attachments");
                    CommonAttachmentBloc::save($files, [
                        "parent_type" => $this->repo->model(),
                        "parent_id" => $post->id,
                        "folder" => "socmed"
                    ]);
                }

                return $post;
            });
            DB::commit();
            return $posts;
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }

    public function update($id, $input)
    {
        DB::beginTransaction();
        try {
            $spot = $this->repo->find($id);
            $spot->update($input);

            if (request()->hasFile('attachments')) {
                $files = request()->file('attachments');

                CommonAttachmentBloc::save($files, [
                    "parent_type" => $this->repo->model(),
                    "parent_id" => $spot->id,
                    "folder" => "socmed"
                ]);
            }
            DB::commit();
            return $spot;
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }

    public function destroy($id)
    {
        $spot = $this->repo->find($id);
        CommonAttachmentBloc::unlinkByParent([
            "parent_type" => $this->repo->model(),
            "parent_id" => $spot->id,
        ]);

        return $this->repo->destroyAction($id);
    }
}
