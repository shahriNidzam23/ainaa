<?php

namespace Modules\Order\Events;

use Illuminate\Queue\SerializesModels;
use Modules\Order\Models\OrderSales;


class SaleUpdated
{
    use SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */

    public $orderSale, $previous;

    public function __construct($orderSale, $previous)
    {
        $this->orderSale = $orderSale;
        $this->previous = $previous;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
