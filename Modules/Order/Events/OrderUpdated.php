<?php

namespace Modules\Order\Events;

use Illuminate\Queue\SerializesModels;
use Modules\Order\Models\Order;

class OrderUpdated
{
    use SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */

    public $order, $previous;

    public function __construct($order, $previous)
    {
        $this->order = $order;
        $this->previous = $previous;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
