<?php

namespace Modules\Order\Events;

use Illuminate\Queue\SerializesModels;
use Modules\Order\Models\OrderSales;

class SaleCreated
{
    use SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */

    public $orderSale;

    public function __construct($orderSale)
    {
        //
        $this->orderSale = $orderSale;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
