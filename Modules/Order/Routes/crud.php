<?php

namespace Modules\Order\Http\Controllers;

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::apiResources([
    'ord-customer' => OrdCustomerController::class,
    'ord-order' => OrdOrderController::class,
    'ord-product' => OrdProductController::class,
    'ord-sale' => OrdSaleController::class,
    'ord-tax' => OrdTaxController::class,
]);