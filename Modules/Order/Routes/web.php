<?php

namespace Modules\Order\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['as' => 'order.'], function () {
    Route::get('/{id}/invoice', [OrdOrderController::class, 'invoice'])->name("invoice");
    Route::get('/{id}/receipt', [OrdOrderController::class, 'receipt'])->name("receipt");
});



// require_once module_path('Order', 'Routes/order.php');
