<?php

namespace Modules\Order\Repositories;

use Rekamy\LaravelCore\Crudable\Concern\CrudableRepository;
use Rekamy\LaravelCore\Contracts\CrudableRepository as CrudableRepositoryContract;
use Modules\Order\Models\OrdProduct;
use Rekamy\LaravelCore\Override\Repository;

/**
 * Class OrdProductRepository
 * @package Modules\Order\Repositories
 *
 * @method OrdProduct find($id, $columns = ['*'])
 * @method OrdProduct find($id, $columns = ['*'])
 * @method OrdProduct first($columns = ['*'])
*/
class OrdProductRepository extends Repository implements CrudableRepositoryContract
{
    use CrudableRepository;

    /**
     * @var array
     */
    protected $fieldSearchable = [
		'parent_type',
		'parent_id',
		'name',
		'sku',
		'price',
		'quantity',
	];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return OrdProduct::class;
    }
    
}

