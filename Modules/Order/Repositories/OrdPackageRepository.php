<?php

namespace Modules\Order\Repositories;

use Rekamy\LaravelCore\Crudable\Concern\CrudableRepository;
use Rekamy\LaravelCore\Contracts\CrudableRepository as CrudableRepositoryContract;
use Modules\Order\Models\OrdPackage;
use Rekamy\LaravelCore\Override\Repository;

/**
 * Class OrdPackageRepository
 * @package Modules\Order\Repositories
 *
 * @method OrdPackage find($id, $columns = ['*'])
 * @method OrdPackage find($id, $columns = ['*'])
 * @method OrdPackage first($columns = ['*'])
*/
class OrdPackageRepository extends Repository implements CrudableRepositoryContract
{
    use CrudableRepository;

    /**
     * @var array
     */
    protected $fieldSearchable = [
		'order_id',
		'name',
		'quantity',
		'buying_price',
	];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return OrdPackage::class;
    }
    
}

