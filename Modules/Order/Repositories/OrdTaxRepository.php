<?php

namespace Modules\Order\Repositories;

use Rekamy\LaravelCore\Crudable\Concern\CrudableRepository;
use Rekamy\LaravelCore\Contracts\CrudableRepository as CrudableRepositoryContract;
use Modules\Order\Models\OrdTax;
use Rekamy\LaravelCore\Override\Repository;

/**
 * Class OrdTaxRepository
 * @package Modules\Order\Repositories
 *
 * @method OrdTax find($id, $columns = ['*'])
 * @method OrdTax find($id, $columns = ['*'])
 * @method OrdTax first($columns = ['*'])
*/
class OrdTaxRepository extends Repository implements CrudableRepositoryContract
{
    use CrudableRepository;

    /**
     * @var array
     */
    protected $fieldSearchable = [
		'parent_type',
		'parent_id',
		'name',
		'value',
	];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return OrdTax::class;
    }
    
}

