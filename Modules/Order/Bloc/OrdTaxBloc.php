<?php

namespace Modules\Order\Bloc;

use Rekamy\LaravelCore\Crudable\Abstract\CrudBlocRepo;
use Modules\Order\Repositories\OrdTaxRepository;
use Modules\Order\Http\Requests\OrdTaxRequest;
use Modules\Order\Models\OrderTaxes;
use Illuminate\Support\Str;

class OrdTaxBloc extends CrudBlocRepo
{
    public function __construct(
        public OrdTaxRequest $request,
    ) {
        $this->model = OrderTaxes::class;
        $this->request = $request;
    }

    public static function permission($name) {
    //     $permission = [
    //         'index' => 'ord_order_taxes_index',
    //         'create' => 'ord_order_taxes_create',
    //         'show' => 'ord_order_taxes_show',
    //         'update' => 'ord_order_taxes_update',
    //         'destroy' => 'ord_order_taxes_destroy',
    //     ];

    //     return $permission[$name];
    }

}
