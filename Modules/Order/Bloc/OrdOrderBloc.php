<?php

namespace Modules\Order\Bloc;

use Rekamy\LaravelCore\Crudable\Abstract\CrudBlocRepo;
use Modules\Order\Http\Requests\OrdOrderRequest;
use Modules\Common\Bloc\CommonAttachmentBloc;
use Modules\Order\Events\{
    OrderCreated,
    OrderUpdated,
    OrderDeleted
};
use Illuminate\Support\Facades\DB;
use Modules\Common\Models\CommonRefNo;
use Modules\Order\Models\OrdOrder;
use Modules\Order\Models\OrdProduct;
use Modules\Order\Notifications\OrderCreatedNotification;
use Modules\Warehouse\Models\{WarPackage, WarProduct, WarStock};
use Barryvdh\DomPDF\Facade\Pdf;

class OrdOrderBloc extends CrudBlocRepo
{
    private $result;
    protected $baseBloc;

    public function __construct(
        public OrdOrderRequest $request,
    ) {
        $this->model = OrdOrder::class;
        $this->request = $request;
    }

    public static function permission($name)
    {
    }

    public function store($input)
    {

        if (empty($input["products"])) return abort(400, "Atleast 1 Product to restock");

        collect($input["products"])->each(function ($product) use ($input) {
            $productSource = WarProduct::with([
                "attachments",
                "packages" => function ($query) use ($product) {
                    $query->where("id", $product["package"]);
                }
            ])->where("sku", $product["sku"])->first();

            WarStock::isSufficient($input["seller_id"], $productSource->id, $product["quantity"] * $productSource->packages->first()->quantity);
        });

        DB::beginTransaction();
        try {
            $input['ref_no'] = CommonRefNo::runningNo("order.ref_no");
            $input["status"] = OrdOrder::STATUS_PROCESSING;
            $order = $this->getBlocRepo()->storeAction($input);

            if (request()->hasFile('payment_evidence')) {
                $files = request()->file('payment_evidence');

                CommonAttachmentBloc::save([$files], [
                    "parent_type" => $this->getBlocRepo()->model(),
                    "parent_id" => $order->id,
                    "folder" => "order"
                ]);
            }

            if (!empty($input["tax"])) {
                $order->tax()->create($input["tax"]);
            }


            collect($input["products"])->each(function ($product) use ($order, $input) {
                $productSource = WarProduct::with("attachments")->where("sku", $product["sku"])->first();
                $product["name"] = $productSource->name;
                $product["expiry_date"] = $productSource->expiry_date;
                $package = $product["package"];
                unset($product["package"]);
                $parent = $order->products()->create($product);
                // Copy Product Attachment
                CommonAttachmentBloc::copy([
                    "parent_type" => OrdProduct::class,
                    "parent_id" => $parent->id,
                    "folder" => "order",
                    "copy_from" => $productSource->attachments
                ]);

                $package = WarPackage::where("id", $package)->firstOrFail()->toArray();
                unset($package["id"]);
                $parent->package()->create($package);
            });

            $order->seller->notify(new OrderCreatedNotification($order));

            DB::commit();

            return $order;
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }

    public function update($id, $input)
    {
        DB::beginTransaction();
        try {
            $order = $this->getBlocRepo()->find($id);

            $previous = clone ($order);

            if ($order->status == OrdOrder::STATUS_COLLECTED || $order->status == OrdOrder::STATUS_CANCEL) {
                abort(403, "Cannot update a completed/cancelled order");
            }

            $order->update($input);

            event(new OrderUpdated($order, $previous));
            DB::commit();


            return $order;
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }

    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $order = $this->getBlocRepo()->find($id);
            CommonAttachmentBloc::unlinkByParent([
                "parent_type" => $this->getBlocRepo()->model(),
                "parent_id" => $order->id,
            ]);

            $order = $this->getBlocRepo()->destroyAction($id);
            $order->prices()->delete();
            $product->tax()->delete();
            DB::commit();

            return $order;

            // event(new OrderDeleted($order));
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }

    public function invoice($id)
    {
        $order = OrdOrder::invoiceReceipt($id);

        abort_if(empty($order), 404, "Order not found");

        switch ($order["status"]) {
            case OrdOrder::STATUS_PROCESSING:
                return abort(403, "Order in process");
            case OrdOrder::STATUS_ACCEPT:
            case OrdOrder::STATUS_READY:
            case OrdOrder::STATUS_COLLECTED:
                $order["statusText"] = "confirmed";
                $order["statusDesc"] = "Your order has been confirmed and will be process by the seller.";
                break;
            case OrdOrder::STATUS_CANCEL:
                $order["statusText"] = "cancelled";
                $order["statusDesc"] = "We regret to inform you that your recent order with us has been cancelled.";
                break;
        }

        // dd($order);
        $pdf = Pdf::loadView('order::invoice-receipt', $order);
        return $pdf->stream();
    }

    public function receipt($id)
    {
        $order = OrdOrder::invoiceReceipt($id);

        abort_if(empty($order), 404, "Order not found");

        switch ($order["status"]) {
            case OrdOrder::STATUS_COLLECTED:
                $order["statusText"] = "completed";
                $order["statusDesc"] = "We're pleased to inform you that your order has been successfully completed.";
                break;
            default:
                abort(403, "Order Has not complete");
        }

        $pdf = Pdf::loadView('order::invoice-receipt', $order);
        return $pdf->stream();
    }
}
