<?php

namespace Modules\Order\Bloc;

use Rekamy\LaravelCore\Crudable\Abstract\CrudBlocRepo;
use Modules\Order\Http\Requests\OrdCustomerRequest;
use Modules\Order\Models\OrdCustomer;

class OrdCustomerBloc extends CrudBlocRepo
{
    public function __construct(
        public OrdCustomerRequest $request,
    ) {
        $this->model = OrdCustomer::class;
        $this->request = $request;
    }

    public static function permission($name) {
    //     $permission = [
    //         'index' => 'ord_customers_index',
    //         'create' => 'ord_customers_create',
    //         'show' => 'ord_customers_show',
    //         'update' => 'ord_customers_update',
    //         'destroy' => 'ord_customers_destroy',
    //     ];

    //     return $permission[$name];
    }
}
