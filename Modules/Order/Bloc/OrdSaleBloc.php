<?php

namespace Modules\Order\Bloc;

use Rekamy\LaravelCore\Crudable\Abstract\CrudBlocRepo;
use Modules\Order\Http\Requests\OrdSaleRequest;
use Illuminate\Support\Facades\DB;
use Modules\Order\Events\{
    SaleCreated,
    SaleUpdated,
};
use Modules\Common\Models\CommonRefNo;
use Modules\Order\Models\OrdSale;
use Modules\Common\Bloc\CommonAttachmentBloc;
use Modules\Warehouse\Models\{WarProduct, WarStock};
use Modules\Order\Models\OrdProduct;

class OrdSaleBloc extends CrudBlocRepo
{
    public function __construct(
        public OrdSaleRequest $request,
    ) {
        $this->model = OrdSale::class;
        $this->request = $request;
    }

    public static function permission($name)
    {
        //     $permission = [
        //         'index' => 'ord_sale_index',
        //         'create' => 'ord_sale_create',
        //         'show' => 'ord_sale_show',
        //         'update' => 'ord_sale_update',
        //         'destroy' => 'ord_sale_destroy',
        //     ];

        //     return $permission[$name];
    }

    public function store($input)
    {
        if (empty($input["products"])) return abort(400, "Atleast 1 Product to sale");

        collect($input["products"])->each(function ($product) use ($input) {
            $productSource = WarProduct::with("attachments")->where("sku", $product["sku"])->first();
            WarStock::isSufficient($input["user_id"], $productSource->id, $product["quantity"]);
        });

        DB::beginTransaction();
        try {
            $input['ref_no'] = CommonRefNo::runningNo("order.sale_ref_no");
            $input["status"] = OrdSale::STATUS_PAID;
            $order = $this->getBlocRepo()->storeAction($input);


            if(!empty($input["tax"])) {
                $order->tax()->create($input["tax"]);
            }

            if(!empty($input["customer"])) {
                $order->customer()->create($input["customer"]);
            }

            if(!empty($input["products"])) {
                collect($input["products"])->each(function($product) use ($order, $input) {
                    $productSource = WarProduct::with("attachments")->where("sku", $product["sku"])->first();
                    $product["name"] = $productSource->name;
                    $product["expiry_date"] = $productSource->expiry_date;
                    $parent = $order->products()->create($product);
                    // Copy Product Attachment
                    CommonAttachmentBloc::copy([
                        "parent_type" => OrdProduct::class,
                        "parent_id" => $parent->id,
                        "folder" => "order",
                        "copy_from" => $productSource->attachments
                    ]);
                });
            }

            event(new SaleCreated($order));
            DB::commit();

            return $order;
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }

    public function update($id, $request)
    {
        DB::beginTransaction();
        try {
            $order = $this->getBlocRepo()->find($id);

            if ($order->status == OrdSale::STATUS_CANCEL) {
                return abort(400, "Cannot update cancelled sale");
            }
            $previous = clone ($order);

            $order = $this->getBlocRepo()->updateAction($id, $request);

            if (request()->has('tax')) {
                OrdTaxBloc::save([
                    "parent_type" => $this->getBlocRepo()->model(),
                    "parent_id" => $order->id,
                    "name" => $request['tax']["name"],
                    "value" => $request['tax']["name"],
                ]);
            }

            event(new SaleUpdated($order, $previous));
            DB::commit();


            return $order;
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }
}
