<?php

namespace Modules\Order\Bloc;

use Rekamy\LaravelCore\Crudable\Abstract\CrudBloc;
use Modules\Order\Repositories\OrdPackageRepository;
use Modules\Order\Http\Requests\OrdPackageRequest;

class OrdPackageBloc extends CrudBloc
{
    public function __construct(
        public OrdPackageRepository $repo, 
        public OrdPackageRequest $request,
    ) {
        $this->repo = $repo;
        $this->request = $request;
    }

    public static function permission($name) {
    //     $permission = [
    //         'index' => 'ord_packages_index',
    //         'create' => 'ord_packages_create',
    //         'show' => 'ord_packages_show',
    //         'update' => 'ord_packages_update',
    //         'destroy' => 'ord_packages_destroy',
    //     ];
        
    //     return $permission[$name];
    }
}
