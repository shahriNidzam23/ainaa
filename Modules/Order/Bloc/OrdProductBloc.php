<?php

namespace Modules\Order\Bloc;

use Rekamy\LaravelCore\Crudable\Abstract\CrudBlocRepo;
use Modules\Order\Http\Requests\OrdProductRequest;
use Modules\Order\Models\OrdProduct;

class OrdProductBloc extends CrudBlocRepo
{
    public function __construct(
        public OrdProductRequest $request,
    ) {
        $this->model = OrdProduct::class;
        $this->request = $request;
    }

    public static function permission($name) {
    //     $permission = [
    //         'index' => 'ord_products_index',
    //         'create' => 'ord_products_create',
    //         'show' => 'ord_products_show',
    //         'update' => 'ord_products_update',
    //         'destroy' => 'ord_products_destroy',
    //     ];

    //     return $permission[$name];
    }
}
