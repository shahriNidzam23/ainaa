<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ord_orders', function (Blueprint $table) {
            $table->uuid('id')->primary();

            $table->foreignUuid('seller_id')->constrained('users')->onDelete('cascade');
            $table->foreignUuid('buyer_id')->constrained('users')->onDelete('cascade');

            $table->string('payment_type');
            $table->string('status');
            $table->string('ref_no')->unique();

            $table->decimal('total_price', 10, 2);
            $table->text('notes')->nullable();
            $table->auditable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ord_orders');
    }
};
