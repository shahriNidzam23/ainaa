<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ord_taxes', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('parent_type');
            $table->uuid('parent_id');
            $table->string('name');
            $table->float('value');
            $table->auditable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ord_order_taxes');
    }
};
