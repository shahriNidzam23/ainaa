<?php

namespace Modules\Order\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Modules\Warehouse\Models\WarProduct;
use Modules\User\Models\User;
use Illuminate\Support\Facades\DB;
use Modules\Common\Models\CommonRefNo;
use Modules\Order\Models\OrdSale;
use Modules\Agent\Models\Agent;

class SeedOrdSalesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $sales = collect();
        $customers = collect();
        $taxes = collect();
        $products = collect();

        for ($i = 0; $i < 10; $i++) {
            $product = WarProduct::inRandomOrder()->first();
            $salesid = Str::orderedUuid();
            $agent = Agent::where("code", "MS01/00001")->first();
            $sales->push([
                'id' => $salesid,
                'user_id' => $agent->user_id,
                'ref_no' => CommonRefNo::runningNo("order.sale_ref_no"),
                'total_price' => $product->selling_price + ($agent->tax_enabled ? 6 / 100 : 0),
                'status' => OrdSale::STATUS_PAID,
                "created_at" => now(),
            ]);

            $customers->push([
                'id' => Str::orderedUuid(),
                "sale_id" => $salesid,
                "name" => "Ejam",
                "phone_number" => "0123456789",
                "email" => "ejam@example.com",
                "created_at" => now(),
            ]);

            $products->push([
                'id' => Str::orderedUuid(),
                'parent_type' => OrdSale::class,
                'parent_id' => $salesid,
                'name' => $product->name,
                'sku' => $product->sku,
                'price' => $product->selling_price,
                'quantity' => 1,
                "created_at" => now(),
            ]);


            if ($agent->tax_enabled) {
                $taxes->push([
                    'id' => Str::orderedUuid(),
                    'parent_type' => OrdSale::class,
                    'parent_id' => $salesid,
                    'name' => 'SST',
                    'value' => 6,
                    "created_at" => now(),
                ]);
            }
        }
        DB::table('ord_sales')->insert($sales->toArray());
        DB::table('ord_customers')->insert($customers->toArray());
        DB::table('ord_taxes')->insert($taxes->toArray());
        DB::table('ord_products')->insert($products->toArray());
    }
}
