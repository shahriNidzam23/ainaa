<?php

namespace Modules\Order\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Common\Database\Seeders\RefNoTableSeeder;



class OrderDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->getRefNo();
        $this->call([
            OrderRolePermissionSeeder::class,
            // SeedOrdOrderSeeder::class,
            // SeedOrdSalesSeeder::class,
        ]);
    }

    public function getRefNo() {
        RefNoTableSeeder::register([
            "order.ref_no" => "ORD/",
            "order.sale_ref_no" => "SALE/",
        ]);
    }
}
