<?php

namespace Modules\Order\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Faker\Factory as Faker;
use Modules\Common\Models\CommonRefNo;
use Modules\Agent\Models\Agent;
use Modules\Order\Models\OrdOrder;
use Modules\Warehouse\Models\WarProduct;
use Modules\Common\Bloc\CommonAttachmentBloc;
use Modules\Order\Models\OrdProduct;

use Modules\Order\Events\{
    OrderCreated,
    OrderUpdated,
    OrderDeleted
};

class SeedOrdOrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $orders = collect();
        $products = collect();
        $packages = collect();
        $taxes = collect();

        $faker = Faker::create();

        for ($i = 0; $i < 10; $i++) {

            $agent = Agent::with("upline")->where("code", "MS01/00001")->first();
            $war_products = WarProduct::with(["packages"])
                ->whereHas("packages")
                ->inRandomOrder()->limit(2)->get();

            $order_id = Str::orderedUuid();
            $price = collect();
            switch ($i) {
                case 0:
                case 1:
                    $status = OrdOrder::STATUS_PROCESSING;
                    break;
                case 2:
                case 3:
                    $status = OrdOrder::STATUS_ACCEPT;
                    break;
                case 4:
                case 5:
                    $status = OrdOrder::STATUS_READY;
                    break;
                case 6:
                case 7:
                    $status = OrdOrder::STATUS_COLLECTED;
                    break;
                case 8:
                case 9:
                    $status = OrdOrder::STATUS_CANCEL;
                    break;
            }

            $war_products->each(function ($product) use ($products, $packages, $order_id, $price) {
                $product_id = Str::orderedUuid();
                $price->push($product->packages->first()->buying_price);
                $products->push([
                    'id' => $product_id,
                    'parent_type' => OrdOrder::class,
                    'parent_id' => $order_id,
                    'name' => $product->name,
                    'sku' => $product->sku,
                    'price' => $product->packages->first()->buying_price,
                    'quantity' => 1,
                    "created_at" => now(),
                ]);


                $packages->push([
                    'id' => Str::orderedUuid(),
                    'product_id' => $product_id,
                    'name' => $product->packages->first()->name,
                    'quantity' => $product->packages->first()->quantity,
                    'buying_price' => $product->packages->first()->buying_price,
                    "created_at" => now(),
                ]);
            });
            $price = $price->sum();

            if ($agent->tax_enabled) {
                $price = $price + (6/100 * $price);
                $taxes->push([
                    'id' => Str::orderedUuid(),
                    'parent_type' => OrdOrder::class,
                    'parent_id' => $order_id,
                    'name' => 'SST',
                    'value' => 6,
                    "created_at" => now(),
                ]);
            }
            $orders->push([
                "id" => $order_id,
                "seller_id" => $agent->upline->id,
                "buyer_id" => $agent->user_id,
                "payment_type" => "cash",
                "status" => $status,
                'ref_no' => CommonRefNo::runningNo("order.ref_no"),
                "total_price" => $price,
                "notes" => $faker->sentence,
                "created_at" => now(),
            ]);
        }

        \DB::table('ord_orders')->insert($orders->toArray());
        collect($products)->each(function ($product) {
            $productSource = WarProduct::with("attachments")->where("sku", $product["sku"])->first();
            CommonAttachmentBloc::copy([
                "parent_type" => OrdProduct::class,
                "parent_id" => $product["id"],
                "folder" => "order",
                "copy_from" => $productSource->attachments
            ]);
        });
        \DB::table('ord_products')->insert($products->toArray());
        \DB::table('ord_packages')->insert($packages->toArray());
        \DB::table('ord_taxes')->insert($taxes->toArray());

        $orders->each(function ($order) {
            $order = OrdOrder::find($order["id"]);
            event(new OrderUpdated($order, $order));
        });
    }
}
