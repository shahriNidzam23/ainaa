<?php

namespace Modules\Order\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Auth\Database\Seeders\RolePermissionSeeder;

class OrderRolePermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $access = [
            'superadmin' => $this->admin(),
            'hq' => $this->admin(),
            'admin' => $this->admin(),
            'master_stockist' => $this->agent(),
            'stockist_gold' => $this->agent(),
            'stockist_silver' => $this->agent(),
            'stockist_bronze' => $this->agent(),
            'agent_gold' => $this->agent(),
            'agent_silver' => $this->agent(),
            'agent_bronze' => $this->agent(),
        ];

        RolePermissionSeeder::registerRP("Order", $access);
    }

    private function agent()
    {
        return [

            "ord_orders:view",
            "ord_orders:create:buyer_id",
            "ord_orders:update:buyer_id,seller_id",

            "ord_sales:view:user_id",
            "ord_sales:create:user_id",
            "ord_sales:update:user_id",
        ];
    }

    private function admin()
    {
        return [
            "ord_orders:view",
            "ord_orders:create",
            "ord_orders:update",
            "ord_orders:delete",

            "ord_sales:view",
            "ord_sales:create",
            "ord_sales:update",
            "ord_sales:delete",
        ];
    }
}
