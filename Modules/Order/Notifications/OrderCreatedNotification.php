<?php

namespace Modules\Order\Notifications;

use Kutia\Larafirebase\Messages\FirebaseMessage;
use Illuminate\Notifications\Messages\MailMessage;
use Modules\Order\Models\OrdOrder;
use Carbon\Carbon;
use Illuminate\Notifications\Notification;

class OrderCreatedNotification extends Notification
{
    public function __construct(private OrdOrder $order)
    {
        $this->order->load(["buyer", "products.detail"]);
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array<int, string>
     */
    public function via(object $notifiable): array
    {
        return ['mail', 'firebase'];
    }

    public function toMail(object $notifiable): MailMessage
    {
        $message =  (new MailMessage)
            ->subject('New Order!')
            ->greeting("Hello $notifiable->display_name,")
            ->line($this->order->buyer->display_name . " has just placed an order")
            ->line("Details are as below:")
            ->line("")
            ->line("Order Reference Number: " . $this->order->ref_no)
            ->line("Customer: " . $this->order->buyer->display_name)
            ->line("Total Amount: RM" . $this->order->total_price)
            ->line("Product Details: ");

        $this->order->products->each(function ($product) use ($message) {
            $message
                ->line("Name: $product->name (Quantity: $product->quantity)");
        });
        return $message;
    }

    /**
     * Get the firebase representation of the notification.
     */
    public function toFirebase($notifiable)
    {
        // Reference : https://github.com/kutia-software-company/larafirebase
        return (new FirebaseMessage)
            ->withTitle('New Order!')
            ->withBody($this->order->buyer->display_name . " has just placed an order")
            ->withPriority('high')
            ->asNotification($notifiable->fcm_token);
    }

    public function shouldSend(object $notifiable, string $channel): bool
    {

        if($channel === 'firebase') {
            $notifiable->append('fcm_token');
            return !empty($notifiable->fcm_token);
        }

        return true;
    }
}
