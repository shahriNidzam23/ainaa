<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <title>{{ $ref_no }}</title>
    <style type="text/css">
        @font-face {
            font-family: 'Inter';
            font-style: normal;
        }

        @font-face {
            font-family: 'Inter';
            font-style: italic;
        }

        /* Generate @font-face rules for each font weight */

        /* Inter Black */
        @font-face {
            font-family: 'Inter';
            font-weight: 900;
            src: url('{{ public_path('/fonts/Inter/Inter-Black.ttf') }}') format('truetype');
        }

        /* Inter ExtraBold */
        @font-face {
            font-family: 'Inter';
            font-weight: 800;
            src: url('{{ public_path('/fonts/Inter/Inter-ExtraBold.ttf') }}') format('truetype');
        }

        /* Inter Bold */
        @font-face {
            font-family: 'Inter';
            font-weight: 700;
            src: url('{{ public_path('/fonts/Inter/Inter-Bold.ttf') }}') format('truetype');
        }

        /* Inter SemiBold */
        @font-face {
            font-family: 'Inter';
            font-weight: 600;
            src: url('{{ public_path('/fonts/Inter/Inter-SemiBold.ttf') }}') format('truetype');
        }

        /* Inter Medium */
        @font-face {
            font-family: 'Inter';
            font-weight: 500;
            src: url('{{ public_path('/fonts/Inter/Inter-Medium.ttf') }}') format('truetype');
        }

        /* Inter Regular */
        @font-face {
            font-family: 'Inter';
            font-weight: 400;
            src: url('{{ public_path('/fonts/Inter/Inter-Regular.ttf') }}') format('truetype');
        }

        /* Inter Light */
        @font-face {
            font-family: 'Inter';
            font-weight: 300;
            src: url('{{ public_path('/fonts/Inter/Inter-Light.ttf') }}') format('truetype');
        }

        /* Inter ExtraLight */
        @font-face {
            font-family: 'Inter';
            font-weight: 200;
            src: url('{{ public_path('/fonts/Inter/Inter-ExtraLight.ttf') }}') format('truetype');
        }

        /* Inter Thin */
        @font-face {
            font-family: 'Inter';
            font-weight: 100;
            src: url('{{ public_path('/fonts/Inter/Inter-Thin.ttf') }}') format('truetype');
        }
    </style>
    <style>
        /* Define your CSS styles for the invoice here */
        body {
            font-family: Inter, sans-serif;
        }

        .invoice-container {
            width: 80%;
            margin: 0 auto;
            padding: 20px;
            border: 1px solid #ccc;
            border-radius: 5px;
        }

        .invoice-header {
            text-align: center;
            margin-bottom: 20px;
        }

        .invoice-details {
            margin-bottom: 20px;
        }

        .invoice-items {
            width: 100%;
            border-collapse: collapse;
            margin-bottom: 20px;
        }

        .invoice-items th,
        .invoice-items td {
            border: 1px solid #ddd;
            padding: 8px;
            text-align: left;
        }

        .invoice-items th {
            background-color: #f2f2f2;
        }
    </style>
</head>

<body>
    <div
        style=" padding-left: 36px; padding-right: 36px; padding-top: 69px; padding-bottom: 69px; background: white; flex-direction: column; justify-content: flex-start; align-items: flex-start; gap: 30px; display: inline-flex">
        <div
            style="flex-direction: column; justify-content: flex-start; align-items: flex-start; gap: 32px; display: flex">
            <img style="width: 154px; height: 29.98px" src="{{ public_path('logo.png') }}" />
            <div
                style="margin-top:20px;color: #2A2A2A; font-size: 24px; font-family: Inter; font-weight: 600; word-wrap: break-word">
                Your Order {{ ucfirst($statusText) }}!</div>
            <div
                style="margin-top:20px;flex-direction: column; justify-content: flex-start; align-items: flex-start; gap: 10px; display: flex">
                <div
                    style="color: #2A2A2A; font-size: 18px; font-family: Inter; font-weight: 600; word-wrap: break-word">
                    Hello {{ $buyer['display_name'] }},</div>
                <div
                    style="color: #2A2A2A; font-size: 14px; font-family: Inter; font-weight: 400; word-wrap: break-word">
                    {{ $statusDesc }}</div>
            </div>
        </div>
        <div
            style="margin-top:20px;align-self: stretch; height: 383px; flex-direction: column; justify-content: flex-start; align-items: flex-start; gap: 20px; display: flex">
            <div style="align-self: stretch; height: 1px; background: #D9D9D9"></div>

            <table style="margin-top:20px;width: 100%;">
                <tr>
                    <td style="width: 20%;">
                        <div
                            style="color: #89A0B5; font-size: 14px; font-family: Inter; font-weight: 500; word-wrap: break-word">
                            Order date</div>
                    </td>
                    <td style="width: 20%;">
                        <div
                            style="color: #89A0B5; font-size: 14px; font-family: Inter; font-weight: 500; word-wrap: break-word">
                            Order Id</div>
                    </td>
                    <td style="width: 20%;">
                        <div
                            style="color: #89A0B5; font-size: 14px; font-family: Inter; font-weight: 500; word-wrap: break-word">
                            Payment</div>
                    </td>
                    <td style="width: 20%;">
                        <div
                            style="color: #89A0B5; font-size: 14px; font-family: Inter; font-weight: 500; word-wrap: break-word">
                            Status</div>
                    </td>
                </tr>
                <tr>
                    <td style="width: 20%;">
                        <div
                            style="color: #2A2A2A; font-size: 14px; font-family: Inter; font-weight: 400; word-wrap: break-word">
                            {{ $created_at }}</div>
                    </td>
                    <td style="width: 20%;">
                        <div
                            style="color: #2A2A2A; font-size: 14px; font-family: Inter; font-weight: 400; word-wrap: break-word">
                            #{{ $ref_no }}</div>
                    </td>
                    <td style="width: 20%;">
                        <div
                            style="color: #2A2A2A; font-size: 14px; font-family: Inter; font-weight: 400; word-wrap: break-word">
                            {{ ucwords($payment_type) }}</div>
                    </td>
                    <td style="width: 20%;">
                        <div
                            style="color: #2A2A2A; font-size: 14px; font-family: Inter; font-weight: 400; word-wrap: break-word">
                            {{ ucwords($status) }}</div>
                    </td>
                </tr>
            </table>
            @foreach ($products as $product)
                <div style="margin-top:20px;align-self: stretch; height: 1px; background: #D9D9D9"></div>
                <table style="margin-top:20px;width: 100%;">
                    <tr>
                        <td style="width: 20%;">
                            @if (!empty($product['attachment']))
                                <img style="width: 60px; height: 60px; position: absolute; border-radius: 4px; border: 0.30px #E5E5E5 solid;"
                                    src="{{ \Modules\Common\Models\CommonAttachment::toBase64($product['attachment']['path']) }}" />
                            @else
                                <img style="width: 60px; height: 60px; position: absolute; border-radius: 4px; border: 0.30px #E5E5E5 solid;"
                                    src="https://via.placeholder.com/60x60" />
                            @endif
                        </td>
                        <td style="width: 70%;">
                            <div
                                style="color: #212121; font-size: 14px; font-family: Inter; font-weight: 600; line-height: 25px;">
                                {{ $product['name'] }}
                            </div>
                            <div
                                style="color: #2A2A2A; font-size: 12px; font-family: Inter; font-weight: 400; line-height: 15px;">
                                {{ $product['package']['name'] }}
                            </div>
                            <div
                                style="color: #2A2A2A; font-size: 12px; font-family: Inter; font-weight: 400; line-height: 15px;">
                                RM{{ $product['package']['buying_price'] }} x {{ $product['quantity'] }}
                            </div>
                        </td>
                        <td style="width: 10%;">
                            <div
                                style="color: #212121; font-size: 14px; font-family: Inter; font-weight: 500; line-height: 25px; margin-left: auto;">
                                RM{{ number_format($product['price'], 2) }}
                            </div>
                        </td>
                    </tr>
                </table>
            @endforeach
            <div style="margin-top:20px;align-self: stretch; height: 1px; background: #D9D9D9"></div>
            <table style="width: 100%;">
                <tr>
                    <td style="width: 60%;"></td>
                    <td style="width: 20%;">
                        <div
                            style="text-align: center; color: #89A0B5; font-size: 14px; font-family: Inter; font-weight: 400; line-height: 25px; word-wrap: break-word">
                            Subtotal
                        </div>
                    </td>
                    <td style="width: 20%;">
                        <div
                            style="text-align: right; color: #212121; font-size: 14px; font-family: Inter; font-weight: 400; line-height: 25px; word-wrap: break-word">
                            RM{{ number_format($subtotal, 2) }}
                        </div>
                </tr>
                @if (!empty($tax))
                    <tr>
                        <td style="width: 60%;"></td>
                        <td style="width: 20%;">
                            <div
                                style="text-align: center; color: #89A0B5; font-size: 14px; font-family: Inter; font-weight: 400; line-height: 25px; word-wrap: break-word">
                                {{ $tax['name'] }}({{ $tax['value'] }}%)
                            </div>
                        </td>
                        <td style="width: 20%;">
                            <div
                                style="text-align: right; color: #212121; font-size: 14px; font-family: Inter; font-weight: 400; line-height: 25px; word-wrap: break-word">
                                RM{{ number_format($tax_value, 2) }}
                            </div>
                        </td>
                    </tr>
                @endif
                <tr>
                    <td style="width: 60%;"></td>
                    <td style="width: 20%;">
                        <div
                            style="text-align: center; color: #2A2A2A; font-size: 14px; font-family: Inter; font-weight: 600; line-height: 25px; word-wrap: break-word">
                            Total
                        </div>
                    </td>
                    <td style="width: 20%;">
                        <div
                            style="text-align: right; color: #212121; font-size: 14px; font-family: Inter; font-weight: 600; line-height: 25px; word-wrap: break-word">
                            RM{{ number_format($total_price, 2) }}</div>
                    </td>
                </tr>
            </table>
            <table style="margin-top:20px;width: 100%;">
                <tr>
                    <td>
                        <div
                            style="margin-top:20;color: #2A2A2A; font-size: 14px; font-family: Inter; font-weight: 400; word-wrap: break-word">
                            If you have any queries, please don't hesitate to contact your upline for further assistance
                            about this issue.</div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div
                            style="margin-top:20;color: #2A2A2A; font-size: 14px; font-family: Inter; font-weight: 600; word-wrap: break-word">
                            Thank you for using XYZ!</div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</body>

</html>
