<?php

namespace Modules\Order\Models;

use Illuminate\Database\Eloquent\{SoftDeletes};
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\{BelongsTo, HasMany};
use Illuminate\Support\Str;
use App\Models\BaseModel as Model;


class OrdTax extends Model
{
	public $table = 'ord_taxes';

	public $fillable = [
		'id',
		'parent_type',
		'parent_id',
		'name',
		'value',
	];

	public $casts = [
		'id' => 'string',
		'parent_type' => 'string',
		'parent_id' => 'string',
		'name' => 'string',
		'value' => 'float',
	];

	public static function rules($scenario = 'default') {
		$rules['default'] = [
			'parent_type' => 'required',
			'parent_id' => 'required',
			'name' => 'required',
			'value' => 'required',
		];
		$rules['store'] = [
			'parent_type' => 'required',
			'parent_id' => 'required',
			'name' => 'required',
			'value' => 'required',
		];
		$rules['update'] = [
			'parent_type' => 'required',
			'parent_id' => 'required',
			'name' => 'required',
			'value' => 'required',
		];
		return $rules[$scenario];
	}

    public function parentable()
    {
        return $this->morphTo('parent');
    }
}
