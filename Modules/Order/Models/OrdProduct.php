<?php

namespace Modules\Order\Models;

use App\Models\BaseModel as Model;
use DB;
use Modules\Warehouse\Models\WarProduct;
use Modules\Common\Models\CommonAttachment;

class OrdProduct extends Model
{
    public $table = 'ord_products';

    public $fillable = [
        'id',
        'parent_type',
        'parent_id',
        'name',
        'sku',
        'price',
        'quantity',
    ];

    public $casts = [
        'id' => 'string',
        'parent_type' => 'string',
        'parent_id' => 'string',
        'product_id' => 'string',
        'price' => 'float',
        'quantity' => 'integer',
    ];

    public $appends = [
        'total'
    ];

    public static function rules($scenario = 'default')
    {
        $rules['default'] = [
            'parent_type' => 'required',
            'parent_id' => 'required',
            'product_id' => 'required',
            'price' => 'required',
            'quantity' => 'required',
        ];
        $rules['store'] = [
            'parent_type' => 'required',
            'parent_id' => 'required',
            'product_id' => 'required',
            'price' => 'required',
            'quantity' => 'required',
        ];
        $rules['update'] = [
            'parent_type' => 'required',
            'parent_id' => 'required',
            'product_id' => 'required',
            'price' => 'required',
            'quantity' => 'required',
        ];
        return $rules[$scenario];
    }

    public function parentable()
    {
        return $this->morphTo('parent');
    }

    public function attachments()
    {
        return $this->morphMany(CommonAttachment::class, 'parent');
    }

    public function attachment()
    {
        return $this->morphOne(CommonAttachment::class, 'parent');
    }

    public function package()
    {
        return $this->hasOne(OrdPackage::class, 'product_id');
    }

    public function detail()
    {
        return $this->hasOne(WarProduct::class, 'sku', 'sku');
    }

    public function getTotalAttribute() {
        return number_format($this->price * $this->quantity, 2);
    }
}
