<?php

namespace Modules\Order\Models;

use Modules\Common\Models\CommonAttachment;
use App\Models\BaseModel as Model;
use App\Traits\DataPolicy;
use Modules\User\Models\User;

class OrdOrder extends Model
{
    use DataPolicy;

    public const STATUS_PROCESSING = "processing";
    public const STATUS_ACCEPT = "accept";
    public const STATUS_READY = "ready";
    public const STATUS_COLLECTED = "collected";
    public const STATUS_CANCEL = "cancelled";

    public const PAYMENT_CASH = "cash";
    public const PAYMENT_BANK_TRANSFER = "bank transfer";
    public const PAYMENT_PAYMENT_GATEWAY = "payment gateway";

    public $table = 'ord_orders';

    public $fillable = [
        'id',
        'seller_id',
        'buyer_id',
        'payment_type',
        'status',
        'ref_no',
        'total_price',
        'notes',
    ];

    public $fieldSearchable = [
        'seller_id',
        'buyer_id',
        'payment_type',
        'status',
        'ref_no',
        'total_price',
        'notes',
    ];

    public $casts = [
        'id' => 'string',
        'seller_id' => 'string',
        'buyer_id' => 'string',
        'payment_type' => 'string',
        'status' => 'string',
        'ref_no' => 'string',
        'total_price' => 'float',
        'notes' => 'string',
    ];

    protected $appends = [
        'total_quantity',
        'invoice',
        'receipt'
    ];

    public static function rules($scenario = 'default')
    {
        $rules['default'] = [
            'seller_id' => 'required',
            'buyer_id' => 'required',
            'payment_type' => 'required',
            'status' => 'required',
            'total_price' => 'required',
            'ref_no' => 'required',
        ];
        $rules['store'] = [
            'seller_id' => 'required',
            'buyer_id' => 'required',
            'payment_type' => 'required',
            'status' => 'required',
            'total_price' => 'required',
            'ref_no' => 'required',
        ];
        $rules['update'] = [
            'seller_id' => 'required',
            'buyer_id' => 'required',
            'payment_type' => 'required',
            'status' => 'required',
            'total_price' => 'required',
            'ref_no' => 'required',
        ];
        return $rules[$scenario];
    }

    public function buyer()
    {
        return $this->belongsTo(User::class, 'buyer_id');
    }

    public function seller()
    {
        return $this->belongsTo(User::class, 'seller_id');
    }

    public function tax()
    {
        return $this->morphOne(OrdTax::class, 'parent');
    }

    public function paymentEvidence()
    {
        return $this->morphOne(CommonAttachment::class, 'parent');
    }

    public function products()
    {
        return $this->morphMany(OrdProduct::class, 'parent');
    }

    public function getTotalQuantityAttribute()
    {
        return $this->products->sum('quantity');
    }

    public function getSubtotalAttribute()
    {
        return $this->products->sum('total');
    }

    public function getTaxValueAttribute()
    {
        return empty($this->tax) ? 0 : $this->tax->value / 100 * $this->products->sum('total');
    }


    public static function invoiceReceipt($id)
    {
        return OrdOrder::bypassDataPolicy(function () use ($id) {
            $order = OrdOrder::with([
                "buyer", "tax", "products.package", "products.attachment"
            ])
                ->find($id)
                ->append(["subtotal", "tax_value"])
                ->toArray();
            $order["created_at"] = \Carbon\Carbon::createFromFormat('Y-m-d\TH:i:s.u\Z', $order["created_at"])->format('d M, Y');
            return $order;
        });
    }

    public function getInvoiceAttribute() {
        return $this->status == OrdOrder::STATUS_PROCESSING ? null : route('order.invoice', ['id' => $this->id]);
    }

    public function getReceiptAttribute() {
        return $this->status == OrdOrder::STATUS_COLLECTED ? route('order.receipt', ['id' => $this->id]) : null;
    }

    public function scopeHq($query) {
        $hq = User::hq();
        return $query->where("seller_id", $hq->id);
    }
}
