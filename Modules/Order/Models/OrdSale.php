<?php

namespace Modules\Order\Models;

use Illuminate\Database\Eloquent\Relations\{BelongsTo, HasMany};
use App\Models\BaseModel as Model;
use App\Traits\DataPolicy;
use Modules\User\Models\User;

class OrdSale extends Model
{
    use DataPolicy;

    public const STATUS_PAID = "paid";
    public const STATUS_CANCEL = "cancel";

    public $table = 'ord_sales';

    public $fillable = [
        'id',
        'user_id',
        'total_price',
        'ref_no',
        'status',
        'notes',
    ];

    public $casts = [
        'id' => 'string',
        'user_id' => 'string',
        'ref_no' => 'string',
    ];

    protected $appends = [
        'total_quantity',
    ];

    public static function rules($scenario = 'default')
    {
        $rules['default'] = [
            'user_id' => 'required',
            'ref_no' => 'required',
        ];
        $rules['store'] = [
            'user_id' => 'required',
            'ref_no' => 'required',
        ];
        $rules['update'] = [
            'user_id' => 'required',
            'ref_no' => 'required',
        ];
        return $rules[$scenario];
    }

    /**
     * Get ord_sales ordCustomers
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function customer()
    {
        return $this->hasOne(OrdCustomer::class, 'sale_id');
    }

    public function tax()
    {
        return $this->morphOne(OrdTax::class, 'parent');
    }

    /**
     * Get ord_sales user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function products()
    {
        return $this->morphMany(OrdProduct::class, 'parent');
    }

    public function getTotalQuantityAttribute(){
        return $this->products->sum('quantity');
    }
}
