<?php

namespace Modules\Order\Models;

use Illuminate\Database\Eloquent\{SoftDeletes};
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\{BelongsTo, HasMany};
use Illuminate\Support\Str;
use App\Models\BaseModel as Model;


class OrdPackage extends Model
{
	public $table = 'ord_packages';

	public $fillable = [
		'id',
		'order_id',
		'name',
		'quantity',
		'sku',
		'buying_price',
	];

	public $casts = [
		'id' => 'string',
		'order_id' => 'string',
		'name' => 'string',
		'quantity' => 'integer',
		'buying_price' => 'decimal:2',
	];

	public static function rules($scenario = 'default') {
		$rules['default'] = [
			'order_id' => 'required',
			'name' => 'required',
			'quantity' => 'required',
			'buying_price' => 'required',
		];
		$rules['store'] = [
			'order_id' => 'required',
			'name' => 'required',
			'quantity' => 'required',
			'buying_price' => 'required',
		];
		$rules['update'] = [
			'order_id' => 'required',
			'name' => 'required',
			'quantity' => 'required',
			'buying_price' => 'required',
		];
		return $rules[$scenario];
	}

	public function product() : BelongsTo
	{
	    return $this->belongsTo(OrdProduct::class, 'product_id');
	}
}
