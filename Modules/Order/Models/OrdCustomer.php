<?php

namespace Modules\Order\Models;

use Illuminate\Database\Eloquent\{SoftDeletes};
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\{BelongsTo, HasMany};
use Illuminate\Support\Str;
use App\Models\BaseModel as Model;


class OrdCustomer extends Model
{
	public $table = 'ord_customers';

	public $fillable = [
		'id',
		'sale_id',
		'name',
		'phone_number',
		'email',
	];

	public $casts = [
		'id' => 'string',
		'sale_id' => 'string',
		'name' => 'string',
		'phone_number' => 'string',
		'email' => 'string',
	];

	public static function rules($scenario = 'default') {
		$rules['default'] = [
			'sale_id' => 'required',
			'name' => 'required',
			'phone_number' => 'required',
		];
		$rules['store'] = [
			'sale_id' => 'required',
			'name' => 'required',
			'phone_number' => 'required',
		];
		$rules['update'] = [
			'sale_id' => 'required',
			'name' => 'required',
			'phone_number' => 'required',
		];
		return $rules[$scenario];
	}

	/**
	 * Get ord_customers sale
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function sale() : BelongsTo
	{
	    return $this->belongsTo(OrdSale::class, 'sale_id');
	}
}
