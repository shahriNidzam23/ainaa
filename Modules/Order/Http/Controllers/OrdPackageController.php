<?php

namespace Modules\Order\Http\Controllers;

use Rekamy\LaravelCore\Crudable\Abstract\CrudController;
use Modules\Order\Bloc\OrdPackageBloc;

class OrdPackageController extends CrudController
{
    protected $baseBloc;
    protected $moduleName = '';
    private $result;

    public function __construct(OrdPackageBloc $bloc) 
    {
        // $this->registerPassportScopes();
        $this->baseBloc = $bloc;
        $this->moduleName = 'ord_packages';
    }

    public function registerPassportScopes()
    {
        $this->middleware('scope:ord_packages_index')->only('index');
        $this->middleware('scope:ord_packages_create')->only('store');
        $this->middleware('scope:ord_packages_show')->only('show');
        $this->middleware('scope:ord_packages_update')->only('update');
        $this->middleware('scope:ord_packages_destroy')->only('destroy');
    }
}
