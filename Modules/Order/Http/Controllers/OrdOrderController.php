<?php

namespace Modules\Order\Http\Controllers;

use Rekamy\LaravelCore\Crudable\Abstract\CrudController;
use Modules\Order\Bloc\OrdOrderBloc;

class OrdOrderController extends CrudController
{
    protected $baseBloc;
    protected $moduleName = '';
    private $result;

    public function __construct(OrdOrderBloc $bloc)
    {
        // $this->registerPassportScopes();
        $this->baseBloc = $bloc;
        $this->moduleName = 'order';
    }

    public function registerPassportScopes()
    {
        $this->middleware('scope:order_index')->only('index');
        $this->middleware('scope:order_create')->only('store');
        $this->middleware('scope:order_show')->only('show');
        $this->middleware('scope:order_update')->only('update');
        $this->middleware('scope:order_destroy')->only('destroy');
    }

    public function invoice($id)
    {
        return $this->baseBloc->invoice($id);
    }

    public function receipt($id)
    {
        return $this->baseBloc->receipt($id);
    }
}
