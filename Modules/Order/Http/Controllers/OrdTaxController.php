<?php

namespace Modules\Order\Http\Controllers;

use Rekamy\LaravelCore\Crudable\Abstract\CrudController;
use Modules\Order\Bloc\OrdTaxBloc;

class OrdTaxController extends CrudController
{
    protected $baseBloc;
    protected $moduleName = '';
    private $result;

    public function __construct(OrdTaxBloc $bloc) 
    {
        // $this->registerPassportScopes();
        $this->baseBloc = $bloc;
        $this->moduleName = 'ord_taxes';
    }

    public function registerPassportScopes()
    {
        $this->middleware('scope:ord_taxes_index')->only('index');
        $this->middleware('scope:ord_taxes_create')->only('store');
        $this->middleware('scope:ord_taxes_show')->only('show');
        $this->middleware('scope:ord_taxes_update')->only('update');
        $this->middleware('scope:ord_taxes_destroy')->only('destroy');
    }
}
