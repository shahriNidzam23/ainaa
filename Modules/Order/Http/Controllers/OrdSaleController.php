<?php

namespace Modules\Order\Http\Controllers;

use Rekamy\LaravelCore\Crudable\Abstract\CrudController;
use Modules\Order\Bloc\OrdSaleBloc;

class OrdSaleController extends CrudController
{
    protected $baseBloc;
    protected $moduleName = '';
    private $result;

    public function __construct(OrdSaleBloc $bloc)
    {
        // $this->registerPassportScopes();
        $this->baseBloc = $bloc;
        $this->moduleName = 'order_sale';
    }

    public function registerPassportScopes()
    {
        $this->middleware('scope:order_sale_index')->only('index');
        $this->middleware('scope:order_sale_create')->only('store');
        $this->middleware('scope:order_sale_show')->only('show');
        $this->middleware('scope:order_sale_update')->only('update');
        $this->middleware('scope:order_sale_destroy')->only('destroy');
    }
}
