<?php

namespace Modules\Order\Http\Controllers;

use Rekamy\LaravelCore\Crudable\Abstract\CrudController;
use Modules\Order\Bloc\OrdCustomerBloc;

class OrdCustomerController extends CrudController
{
    protected $baseBloc;
    protected $moduleName = '';
    private $result;

    public function __construct(OrdCustomerBloc $bloc) 
    {
        // $this->registerPassportScopes();
        $this->baseBloc = $bloc;
        $this->moduleName = 'ord_customers';
    }

    public function registerPassportScopes()
    {
        $this->middleware('scope:ord_customers_index')->only('index');
        $this->middleware('scope:ord_customers_create')->only('store');
        $this->middleware('scope:ord_customers_show')->only('show');
        $this->middleware('scope:ord_customers_update')->only('update');
        $this->middleware('scope:ord_customers_destroy')->only('destroy');
    }
}
