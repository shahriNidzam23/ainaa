<?php

namespace Modules\Order\Http\Requests;

use Modules\Order\Models\OrdProduct;
use Modules\Order\Bloc\OrdProductBloc;
use Illuminate\Foundation\Http\FormRequest;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Rekamy\LaravelCore\Contracts\CrudableRequest;

class OrdProductRequest extends FormRequest implements CrudableRequest
{
    public function model() {
        return OrdProduct::class;
    }

    public function validateIndex()
    {
        /*
        if (!auth()->check()) throw new \Exception("Unauthorized Access", 401);
        $haveAccess = auth()->user()->can(OrdProductBloc::permission('index'));
        if (!$haveAccess) throw new \Exception("Unauthorized Processing Request", 403);
        */
    }

    public function validateStore()
    {
        /*
        if (!auth()->check()) throw new \Exception("Unauthorized Access", 401);
        $haveAccess = auth()->user()->can(OrdProductBloc::permission('create'));
        if (!$haveAccess) throw new \Exception("Unauthorized Processing Request", 403);
        */

        validator(request()->all(), OrdProduct::rules('store'))->validate();
    }

    public function validateShow()
    {
        /*
        $haveAccess = auth()->user()->can(OrdProductBloc::permission('show'));
        if (!$haveAccess) throw new \Exception("Unauthorized Processing Request", 403);
        */
    }

    public function validateUpdate()
    {
        /*
        $haveAccess = auth()->user()->can(OrdProductBloc::permission('update'));
        if (!$haveAccess) throw new \Exception("Unauthorized Processing Request", 403);
        */
    }

    public function validateDestroy()
    {
        /*
        $haveAccess = auth()->user()->can(OrdProductBloc::permission('destroy'));
        if (!$haveAccess) throw new \Exception("Unauthorized Processing Request", 403);
        */
    }

    public function rules()
    {
        return [];
        // return OrdProduct::rules('default');
    }
}
