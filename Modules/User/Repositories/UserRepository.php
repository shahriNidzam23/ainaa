<?php

namespace Modules\User\Repositories;

use Rekamy\LaravelCore\Crudable\Concern\CrudableRepository;
use Rekamy\LaravelCore\Contracts\CrudableRepository as CrudableRepositoryContract;
use Modules\User\Models\User;
use Rekamy\LaravelCore\Override\Repository;


use Rekamy\LaravelCore\Criteria\DataTableCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Rekamy\LaravelCore\Criteria\QueryableRequestCriteria;
use Modules\User\Criteria\NullRequestCriteria;
/**
 * Class UserRepository
 * @package App\Repositories
 *
 * @method User find($id, $columns = ['*'])
 * @method User find($id, $columns = ['*'])
 * @method User first($columns = ['*'])
*/
class UserRepository extends Repository implements CrudableRepositoryContract
{
    use CrudableRepository;

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'display_name',
        'email',
        'roles.name',
        'email_verified_at'
	];

    public function indexAction($input)
    {
        $this->pushCriteria(app(RequestCriteria::class));
        $this->pushCriteria(app(QueryableRequestCriteria::class));
        $this->pushCriteria(app(NullRequestCriteria::class));
        if (
            request()->has('first') ||
            request()->get('first') === true ||
            request()->get('first') == '1'
        ) return $this->firstOrFail();
        if (
            !request()->has('no-paginate') ||
            request()->get('no-paginate') === false ||
            request()->get('no-paginate') == '0'
        ) return $this->paginate();

        return $this->get();
    }

    /**
     * Configure the Model
     **/
    public function model()
    {

        return User::class;
    }

}

