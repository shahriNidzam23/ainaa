<?php

namespace Modules\User\Repositories;

use Modules\User\Models\UserAudit;
use Rekamy\LaravelCore\Crudable\Concern\CrudableRepository;
use Rekamy\LaravelCore\Contracts\CrudableRepository as CrudableRepositoryContract;
use Rekamy\LaravelCore\Override\Repository;

/**
 * Class UserAuditRepository
 * @package Modules\User\Repositories
 *
 * @method UserRefno find($id, $columns = ['*'])
 * @method UserRefno find($id, $columns = ['*'])
 * @method UserRefno first($columns = ['*'])
*/
class UserAuditRepository extends Repository implements CrudableRepositoryContract
{
    use CrudableRepository;

    /**
     * @var array
     */
    protected $fieldSearchable = [
		'event',
		'auditable_type',
	];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return UserAudit::class;
    }

}

