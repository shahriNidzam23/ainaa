<?php

namespace Modules\User\Repositories;

use Rekamy\LaravelCore\Crudable\Concern\CrudableRepository;
use Rekamy\LaravelCore\Contracts\CrudableRepository as CrudableRepositoryContract;
use Modules\User\Models\UserProfile;
use Rekamy\LaravelCore\Override\Repository;

/**
 * Class UserProfileRepository
 * @package App\Repositories
 *
 * @method UserProfile find($id, $columns = ['*'])
 * @method UserProfile find($id, $columns = ['*'])
 * @method UserProfile first($columns = ['*'])
*/
class UserProfileRepository extends Repository implements CrudableRepositoryContract
{
    use CrudableRepository;

    /**
     * @var array
     */
    protected $fieldSearchable = [
		'userId',
		'firstName',
		'lastName',
		'phoneNumber',
        'state',
		'state',
		'region',
	];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return UserProfile::class;
    }

}

