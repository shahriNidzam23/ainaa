<?php

namespace Modules\User\Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Modules\User\Models\User;
use Illuminate\Support\Str;
use Modules\Agent\Models\Agent;
use Modules\Auth\Models\Role;


class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        collect([
            [
                "display_name" => "HQ",
                "email"        => config("app.hq.email"),
                "fullname"   => "HQ",
                "phone_number" => "60123123123",
                "ott"          => "10000",
                "role"       => 'hq',
            ],
            [
                "display_name" => "Superadmin",
                "email"        => "superadmin@example.com",
                "fullname"   => "Super Admin",
                "phone_number" => "60123123123",
                "ott"          => "10000",
                "role"       => 'superadmin',
            ],
            // [
            //     "display_name" => "Admin",
            //     "email"        => "admin@ainaabiz.com",
            //     "fullname"   => "Admin",
            //     "phone_number" => "60123123123",
            //     "ott"          => "10000",
            //     "role"       => 'admin',
            // ],
            [
                "display_name" => "Master Stockist",
                "email"        => "master_stockist@example.com",
                "fullname"   => "Master Stockist",
                "phone_number" => "60123123123",
                "ott"          => "10000",
                "role"       => 'master_stockist',
            ],
            [
                "display_name" => "Stockist Gold",
                "email"        => "stockist_gold@example.com",
                "fullname"   => "Stockist Gold",
                "phone_number" => "60123123123",
                "ott"          => "10000",
                "role"       => 'stockist_gold',
            ],
            [
                "display_name" => "Stockist Silver",
                "email"        => "stockist_silver@example.com",
                "fullname"   => "Stockist Silver",
                "phone_number" => "60123123123",
                "ott"          => "10000",
                "role"       => 'stockist_silver',
            ],
            [
                "display_name" => "Stockist Bronze",
                "email"        => "stockist_bronze@example.com",
                "fullname"   => "Stockist Bronze",
                "phone_number" => "60123123123",
                "ott"          => "10000",
                "role"       => 'stockist_bronze',
            ],
            [
                "display_name" => "Agent Gold",
                "email"        => "agent_gold@example.com",
                "fullname"   => "Agent Gold",
                "phone_number" => "60123123123",
                "ott"          => "10000",
                "role"       => 'agent_gold',
            ],
            [
                "display_name" => "Agent Silver",
                "email"        => "agent_silver@example.com",
                "fullname"   => "Agent Silver",
                "phone_number" => "60123123123",
                "ott"          => "10000",
                "role"       => 'agent_silver',
            ],
            [
                "display_name" => "Agent Bronze",
                "email"        => "agent_bronze@example.com",
                "fullname"   => "Agent Bronze",
                "phone_number" => "60123123123",
                "ott"          => "10000",
                "role"       => 'agent_bronze',
            ],
        ])->each(function ($userDetail) {
            $user = User::create([
                "display_name" => $userDetail["display_name"],
                "email"        => $userDetail["email"],
                "ott"        => $userDetail["ott"],
                "password"     => bcrypt($userDetail["ott"]),
                "email_verified_at" => now()
            ]);
            if (! empty($userDetail['role'])) {
                $user->assignRole(Role::findByName($userDetail['role']));
            }
            $user->profile()->create([
                "fullname"   => $userDetail["fullname"],
                "phone_number" => $userDetail["phone_number"],
                "state" => "Selangor",
                "region" => "Puchong"
            ]);
        });
    }
}
