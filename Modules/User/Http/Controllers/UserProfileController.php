<?php

namespace Modules\User\Http\Controllers;

use Rekamy\LaravelCore\Crudable\Abstract\CrudController;
use Modules\User\Bloc\UserProfileBloc;

class UserProfileController extends CrudController
{
    protected $baseBloc;
    protected $moduleName = '';
    private $result;

    public function __construct(UserProfileBloc $bloc)
    {
        // $this->registerPassportScopes();
        $this->baseBloc = $bloc;
        $this->moduleName = 'user_profiles';
    }

    public function registerPassportScopes()
    {
        $this->middleware('scope:user_profiles_index')->only('index');
        $this->middleware('scope:user_profiles_create')->only('store');
        $this->middleware('scope:user_profiles_show')->only('show');
        $this->middleware('scope:user_profiles_update')->only('update');
        $this->middleware('scope:user_profiles_destroy')->only('destroy');
    }
}
