<?php

namespace Modules\User\Http\Controllers;

use Rekamy\LaravelCore\Crudable\Abstract\CrudController;
use Modules\User\Bloc\UserAuditBloc;

class UserAuditController extends CrudController
{
    protected $baseBloc;
    protected $moduleName = '';
    private $result;

    public function __construct(UserAuditBloc $bloc)
    {
        // $this->registerPassportScopes();
        $this->baseBloc = $bloc;
        $this->moduleName = 'common_attachments';
    }


    public function attachment($id)
    {
        return $this->baseBloc->attachment($id);
    }
}
