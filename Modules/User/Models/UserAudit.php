<?php

namespace Modules\User\Models;

use Illuminate\Database\Eloquent\{SoftDeletes};
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\{BelongsTo, HasMany};
use Illuminate\Support\Str;
use App\Models\BaseModel as Model;
use Illuminate\Support\Facades\Storage;
use Modules\Agent\Models\Agent;
use Modules\CMS\Models\CmsPrivacyPolicy;
use Modules\CMS\Models\CmsTnc;
use Modules\Order\Models\OrdOrder;
use Modules\Order\Models\OrdSale;
use Modules\SocialMedia\Models\SocmedPost;
use Modules\User\Models\User;
use Modules\Warehouse\Models\WarProduct;
use Modules\Warehouse\Models\WarStock;
use Modules\Warehouse\Models\WarTransfer;
use Illuminate\Database\Eloquent\Builder;

class UserAudit extends Model
{
    public $table = 'audits';

    public $casts = [
        'old_values' => 'json',
        'new_values' => 'json',
    ];

    protected $hidden = [
        "updated_at",
        "tags",
        "user_agent",
        "ip_address",
        'url',
        'auditable_id',
        'auditable_type',
    ];

    public $appends = ["name"];

    public function __construct()
    {

        static::addGlobalScope('audit_scope', function (Builder $builder) {
            $builder->whereIn("auditable_type", [
                Agent::class,
                CmsTnc::class,
                CmsPrivacyPolicy::class,
                OrdOrder::class,
                OrdSale::class,
                SocmedPost::class,
                WarProduct::class,
                WarPrice::class,
                WarStock::class,
                WarTransfer::class
            ]);
        });
    }

    public static function rules($scenario = 'default')
    {
        $rules['default'] = [];
        $rules['store'] = [];
        $rules['update'] = [];
        return $rules[$scenario];
    }

    public function user()
    {
        return $this->morphTo('user');
    }

    public function auditable()
    {
        return $this->morphTo('auditable');
    }

    public function getNameAttribute() {
        $events = [
            Agent::class => "Agent",
            CmsTnc::class => "Term And Condition",
            CmsPrivacyPolicy::class => "Privacy Policy",
            OrdOrder::class => "Order / Restocking",
            OrdSale::class => "Sale",
            SocmedPost::class => "Social Media Post",
            WarProduct::class => "Product",
            WarPrice::class => "Product Price",
            WarStock::class => "Product Stock",
            WarTransfer::class => "In/Out"
        ];

        return $events[$this->auditable_type];
    }
}
