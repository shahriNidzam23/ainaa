<?php

namespace Modules\User\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Permission\Traits\HasPermissions;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Eloquent\Relations\{BelongsTo, HasMany};
use Modules\Agent\Models\Agent;
use Modules\Common\Models\CommonAttachment;
use Modules\Warehouse\Models\WarStock;
use Modules\Common\Models\CommonNotification;
use Modules\Order\Models\OrdOrder;
use Modules\Order\Models\OrdSale;
use Modules\SocialMedia\Models\SocmedPost;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasUuids, HasApiTokens, HasFactory, Notifiable, HasRoles, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'display_name',
        'email',
        'password',
        'ott',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
        'ott',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'id' => 'string',
        'email_verified_at' => 'datetime:Y-m-d H:i',
        'created_at' => 'datetime:Y-m-d H:i',
        'updated_at' => 'datetime:Y-m-d H:i',
        'deleted_at' => 'datetime:Y-m-d H:i',
        'password' => 'hashed',
        'status' => 'string',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'email_verified',
    ];

    protected static function boot()
    {

        static::creating(function ($model) {
            if (empty($model->{$model->getKeyName()})) {
                $model->{$model->getKeyName()} = (string) Str::orderedUuid();
            }

            if (auth()->check() && Schema::hasColumn($model->table, 'created_by')) {
                $model->created_by = auth()->id();
            }
        });
        parent::boot();
    }


    public function agent() {
        return $this->hasOne(Agent::class, 'user_id', 'id');
    }

    public function profile()
    {
        return $this->hasOne(UserProfile::class);
    }

	public function socialmedias() : HasMany
	{
	    return $this->hasMany(UserSocialmedia::class, 'user_id');
	}

    public function attachment()
    {
        return $this->morphOne(CommonAttachment::class, 'parent');
    }

    public function notification()
    {
        return $this->hasOne(CommonNotification::class, 'user_id');
    }

	public function socialMediaPost() : HasMany
	{
	    return $this->hasMany(SocmedPost::class, 'created_by');
	}

	/**
	 * Get users warStocks
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function stocks() : HasMany
	{
	    return $this->hasMany(WarStock::class, 'user_id');
	}

	public function orders()
	{
	    return $this->hasMany(OrdOrder::class, 'seller_id');
	}

	public function sales()
	{
	    return $this->hasMany(OrdSale::class, 'user_id');
	}

    public function getEmailVerifiedAttribute()
    {
        return !empty($this->email_verified_at);
    }

    public function getFcmTokenAttribute()
    {
        $this->load('notification');
        return empty($this->notification) ? null :  $this->notification->token ;
    }

    public function scopeIsManagement($query) {
        return $query->whereDoesntHave('agent');
    }

    public function scopeIsAgent($query) {
        return $query->whereHas('agent')->whereHas('roles', function($query){
            $query->whereNot("name", 'hq');
        });
    }

    public function scopeByRole($query) {
        if(empty(request()->get("byRole::rolename"))) return abort(400, "Role Not Provided");

        return $query->whereHas('agent')->whereHas('roles', function($query){
            $query->where("name", request()->get("byRole::rolename"));
        });
    }

    public static function hq() {
        $hq = User::where("email", config("app.hq.email"))->first();
        return $hq;
    }

    public function deleteAll($isSoftDelete = false) {
        $this->sales()->delete();
        $this->orders()->delete();
        $this->socialMediaPost()->delete();
        $this->stocks()->delete();
        $this->attachment()->delete();
        $this->socialmedias()->delete();
        $this->profile()->delete();
        $this->agent()->delete();
        if($isSoftDelete){
            return $this->delete();
        }

        return $this->forceDelete();
    }

    public function isAgent() {
        $this->load("agent");
        return !empty($this->agent);
    }
}
