<?php
namespace Modules\User\Models;

use Illuminate\Database\Eloquent\{SoftDeletes};
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\{BelongsTo, HasMany};
use Illuminate\Support\Str;
use App\Models\BaseModel as Model;
use App\Traits\DataPolicy;

class UserProfile extends Model
{
	public $table = 'user_profiles';

	public $fillable = [
		'id',
		'user_id',
		'fullname',
		'phone_number',
		'state',
		'region',
	];

	public $casts = [
		'id' => 'string',
		'user_id' => 'string',
		'fullname' => 'string',
		'phone_number' => 'string',
		'state' => 'string',
		'region' => 'string',
        'created_at' => 'datetime:Y-m-d H:i',
        'updated_at' => 'datetime:Y-m-d H:i',
        'deleted_at' => 'datetime:Y-m-d H:i',
	];

	public static function rules($scenario = 'default') {
		$rules['default'] = [
			'user_id' => 'required',
			'fullname' => 'required',
		];
		$rules['store'] = [
			'user_id' => 'required',
			'fullname' => 'required',
		];
		$rules['update'] = [
			'user_id' => 'required',
			'fullname' => 'required',
		];
		return $rules[$scenario];
	}

	/**
	 * Get user_profiles user
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function user() : BelongsTo
	{
	    return $this->belongsTo(User::class, 'user_id');
	}
}
