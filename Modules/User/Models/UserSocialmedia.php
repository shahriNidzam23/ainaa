<?php
namespace Modules\User\Models;

use Illuminate\Database\Eloquent\{SoftDeletes};
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\{BelongsTo, HasMany};
use Illuminate\Support\Str;
use App\Models\BaseModel as Model;
use App\Traits\DataPolicy;

class UserSocialmedia extends Model
{
	public $table = 'user_socialmedias';

	public $fillable = [
		'id',
		'user_id',
		'platform',
		'link',
	];

	public $casts = [
		'id' => 'string',
		'user_id' => 'string',
		'platform' => 'string',
		'username' => 'link',
	];

	public static function rules($scenario = 'default') {
		$rules['default'] = [
			'user_id' => 'required',
			'platform' => 'required',
		];
		$rules['store'] = [
			'user_id' => 'required',
			'platform' => 'required',
		];
		$rules['update'] = [
			'user_id' => 'required',
			'platform' => 'required',
		];
		return $rules[$scenario];
	}

	/**
	 * Get user_socialmedias user
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function user() : BelongsTo
	{
	    return $this->belongsTo(User::class, 'user_id');
	}
}
