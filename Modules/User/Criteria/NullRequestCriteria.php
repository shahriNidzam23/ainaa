<?php

namespace Modules\User\Criteria;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;
use Exception;

class NullRequestCriteria implements CriteriaInterface
{
    /**
     * @var \Illuminate\Http\Request
     */
    protected $request;
    protected $query;
    protected $columns;
    protected $repository;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }


    public function bindModel($model, RepositoryInterface $repository)
    {
        $this->query = $model;
        $this->repository = $repository;
    }

    /**
     * Apply criteria in query repository
     *
     * @param         Builder|Model     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     * @throws \Exception
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $this->bindModel($model, $repository);

        $this->applyNull();
        $this->applyNotNull();

        return $this->query;
    }

    public function applyNull()
    {
        if (!$this->request->has('isNull')) return;

        $scopes = $this->request->get('isNull');
        $columns = explode(';', $scopes);
        foreach ($columns as $column) {
            $this->query = $this->query->whereNull($column);
        }
    }

    public function applyNotNull()
    {
        if (!$this->request->has('isNotNull')) return;

        $scopes = $this->request->get('isNotNull');
        $columns = explode(';', $scopes);
        foreach ($columns as $column) {
            $this->query = $this->query->whereNotNull($column);
        }
    }
}
