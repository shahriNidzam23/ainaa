<?php

namespace Modules\User\Bloc;

use Rekamy\LaravelCore\Crudable\Abstract\CrudBloc;
use Modules\User\Repositories\UserAuditRepository;
use Modules\User\Http\Requests\UserAuditRequest;

class UserAuditBloc extends CrudBloc
{
    public function __construct(
        public UserAuditRepository $repo,
        public UserAuditRequest $request,
    ) {
        $this->repo = $repo;
        $this->request = $request;
    }

    public static function permission($name) {
    //     $permission = [
    //         'index' => 'common_refnos_index',
    //         'create' => 'common_refnos_create',
    //         'show' => 'common_refnos_show',
    //         'update' => 'common_refnos_update',
    //         'destroy' => 'common_refnos_destroy',
    //     ];

    //     return $permission[$name];
    }
}
