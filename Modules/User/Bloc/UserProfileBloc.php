<?php

namespace Modules\User\Bloc;

use Rekamy\LaravelCore\Crudable\Abstract\CrudBloc;
use Modules\User\Repositories\UserProfileRepository;
use Modules\User\Http\Requests\UserProfileRequest;

class UserProfileBloc extends CrudBloc
{
    public function __construct(
        public UserProfileRepository $repo,
        public UserProfileRequest $request,
    ) {
        $this->repo = $repo;
        $this->request = $request;
    }

    public static function permission($name) {
    //     $permission = [
    //         'index' => 'user_profiles_index',
    //         'create' => 'user_profiles_create',
    //         'show' => 'user_profiles_show',
    //         'update' => 'user_profiles_update',
    //         'destroy' => 'user_profiles_destroy',
    //     ];

    //     return $permission[$name];
    }
}
