<?php

namespace Modules\User\Bloc;

use Rekamy\LaravelCore\Crudable\Abstract\CrudBloc;
use Modules\User\Repositories\UserSocialmediaRepository;
use Modules\User\Http\Requests\UserSocialmediaRequest;

class UserSocialmediaBloc extends CrudBloc
{
    public function __construct(
        public UserSocialmediaRepository $repo,
        public UserSocialmediaRequest $request,
    ) {
        $this->repo = $repo;
        $this->request = $request;
    }

    public static function permission($name) {
    //     $permission = [
    //         'index' => 'user_socialmedias_index',
    //         'create' => 'user_socialmedias_create',
    //         'show' => 'user_socialmedias_show',
    //         'update' => 'user_socialmedias_update',
    //         'destroy' => 'user_socialmedias_destroy',
    //     ];

    //     return $permission[$name];
    }
}
