<?php

namespace Modules\User\Bloc;

use Rekamy\LaravelCore\Crudable\Abstract\CrudBloc;
use Illuminate\Support\Facades\Hash;
use Modules\User\Http\Requests\UserRequest;
use Modules\User\Repositories\UserRepository;
use Modules\User\Models\{
    User,
    UserProfile,
};
use Modules\Auth\Models\Role;
use Modules\Common\Bloc\CommonAttachmentBloc;
use Modules\Agent\Models\Agent;

class UserBloc extends CrudBloc
{
    public function __construct(
        public UserRepository $repo,
        public UserRequest $request,
    ) {
        $this->repo = $repo;
        $this->request = $request;
    }

    public static function permission($name)
    {
        //     $permission = [
        //         'index' => 'users_index',
        //         'create' => 'users_create',
        //         'show' => 'users_show',
        //         'update' => 'users_update',
        //         'destroy' => 'users_destroy',
        //     ];

        //     return $permission[$name];
    }

    function create($request): User
    {
        $ott = rand(100000, 999999);
        $displayName = explode('@', $request["email"])[0];
        $displayName = preg_replace('/[^a-zA-Z]/', '', $displayName);
        $displayName = !empty($request["display_name"]) ? $request["display_name"] : \Str::title($displayName);
        $user = User::create([
            'display_name' => $displayName,
            'email' => $request["email"],
            'password' => $ott,
            'ott' => $ott
        ]);

        UserProfile::create([
            'user_id' => $user->id,
            'fullname' => $displayName,
            'phone_number' => empty($request["phone_number"]) ? null : $request["phone_number"]
        ]);

        if(empty($request["role"])){
            $request["role"] = "agent_bronze";
        }
        $user->assignRole(Role::findByName($request["role"], 'api'));

        return $user;
    }

    public function update($id, $input)
    {
        $user = $this->repo->updateAction($id, $input);

        if(!empty($input["profile"])) {
            $user->profile()->where("user_id", $id)->update($input['profile']);
        }


        if(!empty($input["socialmedias"])) {
            collect($input["socialmedias"])->each(function($socialmedia) use ($user) {
                if(empty($socialmedia["id"])) {
                    $user->socialmedias()->create($socialmedia);
                } else {
                    $user->socialmedias()->where("id", $socialmedia["id"])->update($socialmedia);
                }
            });
        }

        if (request()->hasFile('attachment')) {
            CommonAttachmentBloc::unlinkByParent([
                "parent_type" => $this->repo->model(),
                "parent_id" => $id
            ]);

            $files = request()->file('attachment');

            CommonAttachmentBloc::save([$files], [
                "parent_type" => $this->repo->model(),
                "parent_id" => $user->id,
                "folder" => "profile_pic"
            ]);
        }

        if (!empty($input["role"]["name"])){
            $user->syncRoles([Role::findByName($input["role"]["name"], 'api')]);

            Agent::bypassDataPolicy(function() use ($user){
                Agent::validateAgentRegistration($user);
            });
        }

        if(!auth()->user()->email_verified) {
            auth()->user()->markEmailAsVerified();
        }
        return $user;
    }
}
