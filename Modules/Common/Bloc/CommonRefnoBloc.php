<?php

namespace Modules\Common\Bloc;

use Rekamy\LaravelCore\Crudable\Abstract\CrudBloc;
use Modules\Common\Repositories\CommonRefnoRepository;
use Modules\Common\Http\Requests\CommonRefnoRequest;

class CommonRefnoBloc extends CrudBloc
{
    public function __construct(
        public CommonRefnoRepository $repo,
        public CommonRefnoRequest $request,
    ) {
        $this->repo = $repo;
        $this->request = $request;
    }

    public static function permission($name) {
    //     $permission = [
    //         'index' => 'common_refnos_index',
    //         'create' => 'common_refnos_create',
    //         'show' => 'common_refnos_show',
    //         'update' => 'common_refnos_update',
    //         'destroy' => 'common_refnos_destroy',
    //     ];

    //     return $permission[$name];
    }
}
