<?php

namespace Modules\Common\Bloc;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Rekamy\LaravelCore\Crudable\Abstract\CrudBloc;
use Modules\Common\Repositories\CommonAttachmentRepository;
use Modules\Common\Http\Requests\CommonAttachmentRequest;
use Modules\Common\Models\CommonAttachment;

class CommonAttachmentBloc extends CrudBloc
{
    public function __construct(
        public CommonAttachmentRepository $repo,
        public CommonAttachmentRequest $request,
    ) {
        $this->repo = $repo;
        $this->request = $request;
    }

    public static function permission($name)
    {
        //     $permission = [
        //         'index' => 'common_attachments_index',
        //         'create' => 'common_attachments_create',
        //         'show' => 'common_attachments_show',
        //         'update' => 'common_attachments_update',
        //         'destroy' => 'common_attachments_destroy',
        //     ];

        //     return $permission[$name];
    }

    public function attachment($id)
    {
        $attachment = CommonAttachment::find($id);
        if (empty($attachment)) {
            abort(404, 'File not found');
        }

        return Storage::response($attachment->path);
    }

    private static function validate($files)
    {
        foreach ($files as $key => $file) {
            $ext = ['png', 'jpeg', 'jpg', 'pdf'];
            if (!in_array(strtolower($file->getClientOriginalExtension()), $ext)) {
                throw new \Exception($file->getClientOriginalName() . ' Extension Error. Acceptable Extension is png, jpeg, jpg, and pdf only');
            }
        }
    }

    public static function save($files, $config)
    {
        CommonAttachmentBloc::validate($files);
        $isStorage = empty($config['storage']) ? false : $config['storage'];
        $files_path = [];
        foreach ($files as $key => $file) {
            $time = Str::orderedUuid();
            $filename = $file->getClientOriginalName();
            $path = $isStorage
                ? self::path($config["folder"] . "/" . $filename)
                : self::path($config["folder"] . "/" . $time . '-' . $filename);
            $files_path[] = [
                'id' => Str::orderedUuid(),
                'parent_type' => $config["parent_type"],
                'parent_id' => $config['parent_id'],
                'path' => $path,
                'name' => $filename,
                'created_at' => now()
            ];
            if (!Storage::exists($path)) Storage::put($path, file_get_contents($file->getRealPath()));
        }
        CommonAttachment::insert($files_path);

        return $files_path;
    }

    public static function copy($config)
    {
        $isStorage = empty($config['storage']) ? false : $config['storage'];
        $files_path = [];
        foreach ($config["copy_from"] as $key => $attachment) {

            $time = Str::orderedUuid();
            $filename = $attachment->name;
            $path = $isStorage
                ? self::path($config["folder"] . "/" . $filename)
                : self::path($config["folder"] . "/" . $time . '-' . $filename);

            $files_path[] = [
                'id' => Str::orderedUuid(),
                'parent_type' => $config["parent_type"],
                'parent_id' => $config['parent_id'],
                'path' => $path,
                'name' => $filename,
                'created_at' => now()
            ];

            Storage::copy($attachment->path, $path);
        }
        CommonAttachment::insert($files_path);

        return $files_path;
    }

    public static function saveFromStorage($paths, $config)
    {
        $config['storage'] = true;
        $files = collect($paths)->map(function ($path, $index) {
            if (!file_exists($path)) return;
            $originalName = basename($path);
            $file = new UploadedFile($path, $originalName);

            return $file;
        })->filter()->toArray();

        return self::save($files, $config);
    }

    public static function path($filename = '')
    {
        return $filename;
    }

    public static function unlink($path = '')
    {
        Storage::delete($path);
        CommonAttachment::where('path', $path)->delete();
    }

    public static function unlinkByParent($parent)
    {

        $attachments = CommonAttachment::where([
            'parent_type' => $parent['parent_type'],
            'parent_id' => $parent['parent_id'],
        ])->get();


        $attachments->each(function ($attachment) {
            Storage::delete($attachment->path);
            $attachment->delete();
        });
    }
}
