<?php

namespace Modules\Common\Repositories;

use Rekamy\LaravelCore\Crudable\Concern\CrudableRepository;
use Rekamy\LaravelCore\Contracts\CrudableRepository as CrudableRepositoryContract;
use Modules\Common\Models\CommonAttachment;
use Rekamy\LaravelCore\Override\Repository;

/**
 * Class CommonAttachmentRepository
 * @package Modules\Common\Repositories
 *
 * @method CommonAttachment find($id, $columns = ['*'])
 * @method CommonAttachment find($id, $columns = ['*'])
 * @method CommonAttachment first($columns = ['*'])
*/
class CommonAttachmentRepository extends Repository implements CrudableRepositoryContract
{
    use CrudableRepository;

    /**
     * @var array
     */
    protected $fieldSearchable = [
		'parent_type',
		'parent_id',
		'name',
		'path',
	];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CommonAttachment::class;
    }

}

