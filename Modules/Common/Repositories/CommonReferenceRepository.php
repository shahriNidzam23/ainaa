<?php

namespace Modules\Common\Repositories;

use Rekamy\LaravelCore\Crudable\Concern\CrudableRepository;
use Rekamy\LaravelCore\Contracts\CrudableRepository as CrudableRepositoryContract;
use Modules\Common\Models\CommonReference;
use Rekamy\LaravelCore\Override\Repository;

/**
 * Class CommonReferenceRepository
 * @package Modules\Common\Repositories
 *
 * @method CommonReference find($id, $columns = ['*'])
 * @method CommonReference find($id, $columns = ['*'])
 * @method CommonReference first($columns = ['*'])
*/
class CommonReferenceRepository extends Repository implements CrudableRepositoryContract
{
    use CrudableRepository;

    /**
     * @var array
     */
    protected $fieldSearchable = [
		'name',
		'value',
		'code',
		'order',
	];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CommonReference::class;
    }

}

