<?php

namespace Modules\Common\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Kutia\Larafirebase\Messages\FirebaseMessage;
use Illuminate\Notifications\Messages\MailMessage;

class BaseNotification extends Notification
{
    use Queueable;
    private $title = 'Title';
    private $body = 'Subtitle';
    private $metadata = null;
    private $condition = true;
    /**
     * Create a new notification instance.
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array<int, string>
     */
    public function via(object $notifiable): array
    {
        return ['mail', 'firebase', 'database'];
    }

    /**
     * Get the firebase representation of the notification.
     */
    public function toFirebase($notifiable)
    {
        // Reference : https://github.com/kutia-software-company/larafirebase
        return (new FirebaseMessage)
            ->withTitle('Hey, ' . $notifiable->display_name)
            ->withBody('Happy Birthday!')
            ->withPriority('high')
            ->asNotification($notifiable->fcm_token);
    }

    public function toMail(object $notifiable): MailMessage
    {
        return (new MailMessage)
            ->subject($this->title)
            ->line($this->body)
            ->line('Terima kasih kerana menggunakan aplikasi kami!');
    }

    public function toArray($notifiable)
    {
        return [
            'title' => $this->title,
            'body' => $this->body,
            'created_at' => now(),
            'metadata' => $this->metadata,
        ];
    }

    public function shouldSend(object $notifiable, string $channel): bool
    {

        if($channel === 'firebase') {
            $notifiable->append('fcm_token');
            return !empty($notifiable->fcm_token);
        }

        return true;
    }

    public function title($title)
    {
        $this->title = $title;
        return $this;
    }

    public function body($subtitle)
    {
        $this->body = $subtitle;
        return $this;
    }

    public function metadata($metadata)
    {
        $this->metadata = $metadata;
        return $this;
    }
}
