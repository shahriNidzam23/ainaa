<?php

use Illuminate\Support\Str;
use Modules\Common\Models\CommonRefNo;

if (! function_exists('settingRunningNo')) {
    function settingRunningNo($name)
    {
        return CommonRefNo::runningNo($name);
    }
}
