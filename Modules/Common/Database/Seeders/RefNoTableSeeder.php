<?php

namespace Modules\Common\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class RefNoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    }

    public static function register($refNos) {
        $data = collect($refNos)
            ->map(fn ($prefix, $name) => self::restruct($prefix, $name));

        DB::table('common_refnos')->insert($data->toArray());

    }

    public static function restruct($prefix, $name)
    {
        return [
            'id' => Str::orderedUuid(),
            'name' => $name,
            'prefix' => $prefix,
            'running_no' => 0,
        ];
    }
}
