<?php

namespace Modules\Common\Http\Controllers;

use Rekamy\LaravelCore\Crudable\Abstract\CrudController;
use Modules\Common\Bloc\CommonAttachmentBloc;

class CommonAttachmentController extends CrudController
{
    protected $baseBloc;
    protected $moduleName = '';
    private $result;

    public function __construct(CommonAttachmentBloc $bloc)
    {
        // $this->registerPassportScopes();
        $this->baseBloc = $bloc;
        $this->moduleName = 'common_attachments';
    }


    public function attachment($id)
    {
        return $this->baseBloc->attachment($id);
    }
}
