<?php

namespace Modules\Common\Http\Controllers;

use Rekamy\LaravelCore\Crudable\Abstract\CrudController;
use Modules\Common\Bloc\CommonNotificationBloc;
use Modules\User\Models\User;
use Modules\Common\Notifications\BaseNotification;

class CommonNotificationController extends CrudController
{
    protected $baseBloc;
    protected $moduleName = '';
    private $result;

    public function __construct(CommonNotificationBloc $bloc)
    {
        // $this->registerPassportScopes();
        $this->baseBloc = $bloc;
        $this->moduleName = 'common_notifications';
    }

    public function registerPassportScopes()
    {
        $this->middleware('scope:common_notifications_index')->only('index');
        $this->middleware('scope:common_notifications_create')->only('store');
        $this->middleware('scope:common_notifications_show')->only('show');
        $this->middleware('scope:common_notifications_update')->only('update');
        $this->middleware('scope:common_notifications_destroy')->only('destroy');
    }

    function test_notification() {
        $user = User::where('email', 'hq@ainaabiz.com')->get()->first();
        $user->notify(new BaseNotification);
        return $user;
    }
}
