<?php

namespace Modules\Common\Http\Controllers;

use Rekamy\LaravelCore\Crudable\Abstract\CrudController;
use Modules\Common\Bloc\CommonReferenceBloc;

class CommonReferenceController extends CrudController
{
    protected $baseBloc;
    protected $moduleName = '';
    private $result;

    public function __construct(CommonReferenceBloc $bloc)
    {
        // $this->registerPassportScopes();
        $this->baseBloc = $bloc;
        $this->moduleName = 'common_references';
    }

    public function registerPassportScopes()
    {
        $this->middleware('scope:common_references_index')->only('index');
        $this->middleware('scope:common_references_create')->only('store');
        $this->middleware('scope:common_references_show')->only('show');
        $this->middleware('scope:common_references_update')->only('update');
        $this->middleware('scope:common_references_destroy')->only('destroy');
    }
}
