<?php

namespace Modules\Common\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('notification/test', [CommonNotificationController::class, 'test_notification']);

Route::apiResources([
    'attachment' => CommonAttachmentController::class,
    'refno' => CommonRefnoController::class,
    'reference' => CommonReferenceController::class,
    'notification' => CommonNotificationController::class,
]);
