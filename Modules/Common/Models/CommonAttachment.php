<?php

namespace Modules\Common\Models;

use Illuminate\Database\Eloquent\{SoftDeletes};
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\{BelongsTo, HasMany};
use Illuminate\Support\Str;
use App\Models\BaseModel as Model;
use Illuminate\Support\Facades\Storage;

class CommonAttachment extends Model
{
    public $table = 'common_attachments';

    public $fillable = [
        'id',
        'parent_type',
        'parent_id',
        'name',
        'path',
    ];

    public $casts = [
        'id' => 'string',
        'parent_type' => 'string',
        'parent_id' => 'string',
        'name' => 'string',
        'path' => 'string',
    ];

    protected $appends = [
        'url',
        'type',
    ];

    protected $hidden = [
        "created_at",
        "updated_at",
        "deleted_at",
        "created_by",
        "updated_by",
        "deleted_by",
        'parent_type',
        'parent_id',
    ];

    public static function rules($scenario = 'default')
    {
        $rules['default'] = [
            'parent_type' => 'required',
            'parent_id' => 'required',
            'name' => 'required',
            'path' => 'required',
        ];
        $rules['store'] = [
            'parent_type' => 'required',
            'parent_id' => 'required',
            'name' => 'required',
            'path' => 'required',
        ];
        $rules['update'] = [
            'parent_type' => 'required',
            'parent_id' => 'required',
            'name' => 'required',
            'path' => 'required',
        ];
        return $rules[$scenario];
    }

    public function parentable()
    {
        return $this->morphTo('parent');
    }


    public function getUrlAttribute()
    {
        return route('common.attachment', ['id' => $this->id]);
    }

    public function getTypeAttribute()
    {
        try {
            return Storage::mimeType($this->path);
        } catch (\Throwable $th) {
            return '';
        }
    }

    public static function toBase64($path) {
        try {
            $fileContents = Storage::get($path);
            return 'data:' . Storage::mimeType($path) . ';base64,' . base64_encode($fileContents);
        } catch (\Throwable $th) {
            return '';
        }
    }
}
