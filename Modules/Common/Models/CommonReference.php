<?php
namespace Modules\Common\Models;

use Illuminate\Database\Eloquent\{SoftDeletes};
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\{BelongsTo, HasMany};
use Illuminate\Support\Str;
use App\Models\BaseModel as Model;


class CommonReference extends Model
{
	public $table = 'common_references';

	public $fillable = [
		'id',
		'name',
		'value',
		'code',
		'order',
	];

	public $casts = [
		'id' => 'string',
		'name' => 'string',
		'value' => 'string',
		'code' => 'string',
		'order' => 'integer',
	];

	public static function rules($scenario = 'default') {
		$rules['default'] = [
			'name' => 'required',
			'value' => 'required',
			'code' => 'required',
			'order' => 'required',
		];
		$rules['store'] = [
			'name' => 'required',
			'value' => 'required',
			'code' => 'required',
			'order' => 'required',
		];
		$rules['update'] = [
			'name' => 'required',
			'value' => 'required',
			'code' => 'required',
			'order' => 'required',
		];
		return $rules[$scenario];
	}
}
