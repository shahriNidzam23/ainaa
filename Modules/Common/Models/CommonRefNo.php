<?php

namespace Modules\Common\Models;

use App\Models\BaseModel as Model;
use App\Traits\DataPolicy;
use Illuminate\Database\Eloquent\{SoftDeletes};

class CommonRefNo extends Model
{
    // use DataPolicy;
    public $table = 'common_refnos';

    public $fillable = [
        'id',
        'name',
        'prefix',
        'running_no',
    ];

    public $casts = [
        'id' => 'string',
        'name' => 'string',
        'prefix' => 'string',
        'running_no' => 'integer',
    ];

    public static function rules($scenario = 'default')
    {
        $rules['default'] = [
            'name' => 'required',
            'prefix' => 'required',
            'running_no' => 'required',
        ];
        $rules['store'] = [
            'name' => 'required',
            'prefix' => 'required',
            'running_no' => 'required',
        ];
        $rules['update'] = [
            'name' => 'required',
            'prefix' => 'required',
            'running_no' => 'required',
        ];

        return $rules[$scenario];
    }

    public static function getPrefix($name)
    {
        $refNo = CommonRefNo::where('name', $name)->firstOrFail();

        return $refNo->prefix;
    }

    public static function runningNo($name)
    {
        $refNo = CommonRefNo::where('name', $name)
            ->firstOrFail();
        $runninNo = intval($refNo->running_no) + 1;
        $refNo->running_no = $runninNo;
        $refNo->save();

        return $refNo->prefix.sprintf('%05d', $runninNo);
    }


    public static function register($refNos) {
        $data = collect($refNos)
            ->map(function ($prefix, $name) {
                return [
                    'id' => \Str::orderedUuid(),
                    'name' => $name,
                    'prefix' => $prefix,
                    'running_no' => 0,
                ];
            });
        CommonRefNo::query()->insert($data->toArray());
    }
}
