<?php

namespace Modules\Agent\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/biolink', [AgentController::class, 'biolink']);

Route::group(['as' => 'agent.', 'middleware' => ['auth:sanctum']], function () {
    Route::get('', [AgentController::class, 'index']);
    Route::get('/analytic', [AgentController::class, 'analytic']);
    Route::get('/hq-analytic', [AgentController::class, 'hqAnalytic']);
    Route::get('/{id}', [AgentController::class, 'show']);
    Route::put('/{id}', [AgentController::class, 'update']);
});


// require_once module_path('Agent', 'Routes/agent.php');
