<?php

namespace Modules\Agent\Http\Controllers;

use Rekamy\LaravelCore\Crudable\Abstract\CrudController;
use Modules\Agent\Bloc\AgentBloc;
use Illuminate\Http\Request;

class AgentController extends CrudController
{
    protected $baseBloc;
    protected $moduleName = '';
    private $result;

    public function __construct(AgentBloc $bloc)
    {
        // $this->registerPassportScopes();
        $this->baseBloc = $bloc;
        $this->moduleName = 'agents';
    }

    public function registerPassportScopes()
    {
        $this->middleware('scope:agents_index')->only('index');
        $this->middleware('scope:agents_create')->only('store');
        $this->middleware('scope:agents_show')->only('show');
        $this->middleware('scope:agents_update')->only('update');
        $this->middleware('scope:agents_destroy')->only('destroy');
    }

    public function analytic()
    {
        try {
            $this->result = $this->baseBloc->analytic();


            return $this->success('Succesfull Retrieved Data', $this->result);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function hqAnalytic()
    {
        try {
            $this->result = $this->baseBloc->hqAnalytic();


            return $this->success('Succesfull Retrieved Data', $this->result);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function biolink(Request $request)
    {
        try {
            $this->result = $this->baseBloc->biolink($request->all());


            return $this->success('Succesfull Retrieved Data', $this->result);
        } catch (\Throwable $th) {
            throw $th;
        }
    }
}
