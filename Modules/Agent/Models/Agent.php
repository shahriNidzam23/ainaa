<?php

namespace Modules\Agent\Models;

use App\Models\BaseModel as Model;
use App\Traits\DataPolicy;
use Modules\User\Models\User;
use Carbon\Carbon;
use Modules\Auth\Models\Role;
use DB;
use Modules\SocialMedia\Models\SocmedPost;

class Agent extends Model
{
    use DataPolicy;

    public $table = 'agents';

    public $fillable = [
        'id',
        'code',
        'user_id',
        'upline_id',
        'tax_enabled',
        'master_prefix',
        'status',
    ];

    public $casts = [
        'id' => 'string',
        'code' => 'string',
        'user_id' => 'string',
        'upline_id' => 'string',
        'master_prefix' => 'string',
        'tax_enabled' => 'boolean',
    ];

    const STATUS_ACTIVE = 'active';
    const STATUS_INACTIVE = 'inactive';
    const STATUS_INVITED = 'invited';
    const STATUS_WAITING_FOR_APPROVAL = 'waiting for approval';
    const STATUS_APPROVED = 'approved';
    const STATUS_REJECTED = 'rejected';

    /**
     * @var array
     */
    public $fieldSearchable = [
        'user_id',
        'upline_id',
        'tax_enabled',
        'status',
        'code',
        'upline.agent.code',
        'user.roles.name'
    ];

    public $appends = ["biolink"];

    public static function rules($scenario = 'default')
    {
        $rules['default'] = [
            'code' => 'required',
            'user_id' => 'required',
            'upline_id' => 'required',
            'tax_enabled' => 'required',
        ];
        $rules['store'] = [
            'code' => 'required',
            'user_id' => 'required',
            'upline_id' => 'required',
            'tax_enabled' => 'required',
        ];
        $rules['update'] = [
            'code' => 'required',
            'user_id' => 'required',
            'upline_id' => 'required',
            'tax_enabled' => 'required',
        ];
        return $rules[$scenario];
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function upline()
    {
        return $this->hasOne(User::class, 'id', 'upline_id');
    }

    public function downline()
    {
        return $this->hasManyThrough(User::class, Agent::class, 'upline_id', 'id', 'user_id', 'user_id')
            ->whereNot('code', config("app.hq.code"));
    }

    public function posts()
    {
        return $this->hasMany(SocmedPost::class, 'created_by', 'user_id');
    }

    public function scopeGreaterRole($query) {
        $roles = array_merge(["hq"], Role::getMobileRoles());
        if(empty(request()->get("greaterRole::rolename"))) return abort(400, "Role Not Provided");
        $index = array_search(request()->get("greaterRole::rolename"), $roles);
        if($index == false) return abort(400, "Role Not Exist");

        $greaterRole = array_slice($roles, 0, $index);
        $query->whereHas(
            "user.roles", function ($query) use ($greaterRole) {
                $query->whereIn("name", $greaterRole);
            }
        );
    }

    public function scopeTotalDownlineByAgent($query, User $user)
    {
        $query->where('upline_id', $user->id)
            ->where('status', Agent::STATUS_ACTIVE)
            ->count();
    }

    public function scopeSocmedPost($query) {
        return $query->select(["id", "code", "user_id"])
        ->selectSub(function ($query) {
            $query->select("display_name")
                ->from('users')
                ->whereColumn('id', 'agents.user_id');
        }, 'display_name')
        ->selectSub(function ($query) {
            $query->select(DB::raw('COALESCE(COUNT(id), 0)'))
                ->from('socmed_posts')
                ->whereColumn('created_by', 'agents.user_id');
        }, 'total_post')
        ->selectSub(function ($query) {
            $query->select(DB::raw("GROUP_CONCAT(DISTINCT social_media SEPARATOR ', ')"))
                ->from('socmed_posts')
                ->whereColumn('created_by', 'agents.user_id');
        }, 'platform')
        ->selectSub(function ($query) {
            $query->select("created_at")
                ->from('socmed_posts')
                ->whereColumn('created_by', 'agents.user_id')
                ->orderBy('created_at', 'desc')
                ->limit(1);
        }, 'last_activity')
        ->whereHas('posts');
    }

    public function scopeAll($query)
    {
        $query->whereNot('code', config("app.hq.code"));
    }

    public function todaySale(User $user)
    {
        $total = 0;
        $agent = $this
            ->with([
                'user.sales' => function ($query) {
                    $query
                        ->whereDate('created_at', '>', Carbon::today()->startOfDay());
                },
                'user.orders' => function ($query) {
                    $query
                        ->whereDate('created_at', '>', Carbon::today()->startOfDay());
                },
                'user.sales.products',
                'user.orders.products'
            ])
            ->where('user_id', $user->id)
            ->first();

        $total += $agent->user->sales->map(function($sale) {
            return $sale->products->sum('price');
        })->sum();


        $total += $agent->user->orders->map(function($sale) {
            return $sale->products->sum('price');
        })->sum();
        return $total;
    }

    public function totalSale(User $user)
    {
        $total = 0;
        $agent = $this
            ->with([
                'user.sales.products',
                'user.sales.tax',
                'user.orders.products',
                'user.orders.tax'
            ])
            ->where('user_id', $user->id)
            ->first();

        $total += $agent->user->sales->sum('total_price');
        $total += $agent->user->orders->sum('total_price');

        return $total;
    }

    public function totalOrder(User $user)
    {
        $total = 0;
        $agent = $this
            ->with([
                'user.orders.products'
            ])
            ->where('user_id', $user->id)
            ->first();

        $total += $agent->user->orders->map(function($sale) {
            return $sale->products->sum('quantity');
        })->sum();
        return $total;
    }

    public function totalSold(User $user)
    {
        $total = 0;
        $agent = $this
            ->with([
                'user.sales.products',
                'user.orders.products'
            ])
            ->where('user_id', $user->id)
            ->first();

        $total += $agent->user->sales->map(function($sale) {
            return $sale->products->sum('quantity');
        })->sum();


        $total += $agent->user->orders->map(function($sale) {
            return $sale->products->sum('quantity');
        })->sum();
        return $total;
    }

    public static function validateAgentRegistration($user, $upline = null) {

        $roles = Role::getMobileRoles();

        if(empty($upline)){
            $user->load("agent.upline.agent");
            $upline = $user->agent->upline;
        }

        if ($upline->agent->code == config("app.hq.code")) return;

        foreach ($roles as $index => $role) {
            if (!$user->hasRole($role)) continue;

            if ($upline->hasRole(collect($roles)->take($index)->toArray())) {
                return;
            }
        }
        abort(400, "Cannot register higher/same hierarchy agent");

    }

    public function getBiolinkAttribute() {
        return config("app.web_url") . "/biolink?id="  . $this->code;
    }
}
