<?php

namespace Modules\Agent\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Auth\Database\Seeders\RolePermissionSeeder;

class AgentRolePermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $access = [
            'superadmin' => $this->admin(),
            'hq' => $this->admin(),
            'admin' => $this->admin(),
            'master_stockist' => $this->upline(),
            'stockist_gold' => $this->upline(),
            'stockist_silver' => $this->upline(),
            'stockist_bronze' => $this->upline(),
            'agent_gold' => $this->upline(),
            'agent_silver' => $this->upline(),
            'agent_bronze' => [
                'agents:create:user_id',
                'agents:view:user_id',
                'agents:update:user_id',
            ],
        ];
        RolePermissionSeeder::registerRP('Agent', $access);
    }

    private function upline() {
        return [
            'agents:create:user_id',
            'agents:view',
            'agents:update:user_id,upline_id',
        ];
    }

    private function admin() {
        return [
            'agents:create',
            'agents:view',
            'agents:update',
            'agents:delete',
        ];
    }
}
