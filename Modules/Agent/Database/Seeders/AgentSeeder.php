<?php

namespace Modules\Agent\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Agent\Models\Agent;
use Modules\Common\Models\CommonRefNo;
use Modules\User\Models\User;
use Modules\Auth\Models\Role;

class AgentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        User::get()->each(function ($user) {

            if (!$user->hasRole($this->getRoles())) return;

            $upline = $this->getUpline($user);
            $prefix = "MS01/";
            $this->registerMasterStockistRefNo($user, $prefix);
            Agent::create([
                'code' => $this->getRunningNo($user, $prefix),
                'master_prefix' => $user->hasRole(['hq']) ? "XYZ" : $prefix,
                'user_id' => $user->id,
                'upline_id' => $upline->id,
                'tax_enabled' => false,
                'status' => 'active',
            ]);
        });
    }

    private function registerMasterStockistRefNo($user, $prefix)
    {
        if (!$user->hasRole(['master_stockist'])) return;

        CommonRefNo::register([
            "agent." . $prefix => $prefix
        ]);
    }

    private function getRunningNo($user, $prefix) {
        if ($user->hasRole(['hq'])) return config("app.hq.code");

        return CommonRefNo::runningNo("agent." . $prefix);
    }

    private function getUpline($user)
    {
        $roles = $this->getRoles();
        foreach ($roles as $index => $role) {
            if ($user->hasRole($role)) {
                if ($index > 1) {
                    return User::where("email", $roles[$index - 1] . "@example.com")->first();
                }

                return User::where("email", config("app.hq.email"))->first();
            }
        }
    }

    private function getRoles()
    {

        return array_merge(["hq"], Role::getMobileRoles());
    }
}
