<?php

namespace Modules\Agent\Bloc;

use Rekamy\LaravelCore\Crudable\Abstract\{CrudBloc, CrudBlocRepo};
use Modules\Agent\Models\Agent;
use Modules\Agent\Http\Requests\AgentRequest;
use Modules\Order\Models\{
    OrdOrder
};
use Carbon\Carbon;
use Modules\Agent\Notifications\UpdateDownlineNotification;
use Modules\SocialMedia\Models\SocmedPost;
use Modules\User\Models\User;

class AgentBloc extends CrudBlocRepo
{
    public function __construct(
        public AgentRequest $request,
    ) {
        $this->model = Agent::class;
        $this->request = $request;
    }

    public static function permission($name)
    {
        //     $permission = [
        //         'index' => 'agents_index',
        //         'create' => 'agents_create',
        //         'show' => 'agents_show',
        //         'update' => 'agents_update',
        //         'destroy' => 'agents_destroy',
        //     ];

        //     return $permission[$name];
    }

    public function update($id, $input)
    {
        $original = $this->getBlocRepo()->find($id);

        $previous = clone ($original);

        $agent = $this->getBlocRepo()->updateAction($id, $input);
        $agent->load("user");

        if ($previous->status == Agent::STATUS_WAITING_FOR_APPROVAL && (in_array($agent->status, [Agent::STATUS_ACTIVE, Agent::STATUS_REJECTED]))) {
            $agent->user->notify(new UpdateDownlineNotification($agent));
        }

        if ($previous->status == Agent::STATUS_WAITING_FOR_APPROVAL && $agent->status == Agent::STATUS_REJECTED) {
            $user = User::where("id", $agent->user->id)->first();
            $user->deleteAll();
        }
        return $agent;
    }

    public function analytic()
    {

        $user = auth()->user();
        if(!$user->isAgent()){
            $user = User::hq();
        }

        return [
            "total_sales" => app(Agent::class)->totalSale($user),
            "total_order" => app(Agent::class)->totalOrder($user),
            "total_sold" => app(Agent::class)->totalSold($user),
            "total_downline" => Agent::where('upline_id', $user->id)
                ->where('status', Agent::STATUS_ACTIVE)
                ->count(),
            "today_order" => OrdOrder::where('seller_id', $user->id)
                ->whereDate('created_at', '>', Carbon::today()->startOfDay())
                ->count(),
            "today_sales" => app(Agent::class)->todaySale($user),
            "total_post" => SocmedPost::where("created_by", $user->id)->count()
        ];
    }

    public function hqAnalytic()
    {
        $user = auth()->user();
        if ($user->hasRole([
            'master_stockist',
            'stockist_gold', 'stockist_silver', 'stockist_bronze',
            'agent_gold', 'agent_silver', 'agent_bronze'
        ])) abort(403, "You don't have permission to access. Please contact HQ");

        $data = [
            "socmedia_summary" => [
                "total_post" => SocmedPost::count(),
                "top_platform" => app(SocmedPost::class)->topPlatform(),
                "total_agent" => SocmedPost::select("created_by")->distinct()->get()->count()
            ]
        ];
        return $data;
    }

    public function biolink($input)
    {
        $agent = Agent::bypassDataPolicy(function () use ($input) {
            return $this->getBlocRepo()->indexAction($input)->first()->setHidden([
                "user_id",
                "upline_id",
                "master_prefix",
                "created_at",
                "updated_at",
                "deleted_at",
                "created_by",
                "updated_by",
                "deleted_by",
                "user.created_at",
            ]);
        });

        return $agent->toArray();
    }
}
