<?php

namespace Modules\Agent\Listeners;

use Modules\Auth\Events\Registered;
use Modules\Agent\Models\Agent;
use Modules\Agent\Notifications\UplineNewAgentNotification;
use Modules\Auth\Models\Role;
use Modules\Common\Models\CommonRefNo;
use Modules\User\Models\User;
use Modules\Auth\Notifications\WelcomeNotification;

class RegisterAgent
{
    private $registered, $request;
    /**
     * Handle the event.
     *
     * @param  \Modules\Auth\Events\Registered  $event
     * @return void
     */
    public function handle(Registered $event)
    {
        $this->registered = $event->user;
        $this->request = $event->request;
        if (!$this->registered->hasRole(Role::getMobileRoles())) return;

        $upline = $this->getUpline();
        Agent::validateAgentRegistration($this->registered, $upline);
        $this->registerMasterStockistRefNo($this->registered);
        $code = $this->getCode($this->registered, $upline);
        $agent = Agent::bypassDataPolicy(function() use ($code, $upline) {
            return Agent::create([
               'code' => $code["ref_no"],
               'master_prefix' =>  $code["prefix"],
               'user_id' => $this->registered->id,
               'upline_id' => $upline->id,
               'status' => !empty($this->request["status"]) ? Agent::STATUS_WAITING_FOR_APPROVAL : Agent::STATUS_INVITED
           ]);
        });
        $this->sendMail($agent);
        $this->notifyUpline($agent, $upline);
    }

    private function getCode($user, $upline)
    {
        $refName = "";
        $prefix = "";
        if ($user->hasRole(['master_stockist'])) {
            $prefix = $this->request["prefix"];
        } else {
            $prefix = $upline->agent->master_prefix;
        }
        $refName = "agent." . $prefix;

        return [
            "ref_no" => CommonRefNo::runningNo($refName),
            "prefix" => $prefix
        ];
    }

    private function registerMasterStockistRefNo($user)
    {
        if (!$user->hasRole(['master_stockist'])) return;
        $prefix = $this->request["prefix"];
        CommonRefNo::register([
            "agent." . $prefix => $prefix
        ]);
    }

    private function sendMail($agent)
    {
        if ($agent->status != Agent::STATUS_INVITED) return;

        $this->registered->notify(new WelcomeNotification);
    }

    private function notifyUpline($agent, $upline)
    {
        if ($agent->status == Agent::STATUS_INVITED) return;

        $upline->notify(new UplineNewAgentNotification($agent));
    }

    private function getUpline()
    {
        $upline = empty($this->request["upline"]) ? config("app.hq.code") :  $this->request["upline"];

        $user = Agent::bypassDataPolicy(function() use ($upline) {
            return User::with('agent')->whereHas("agent", function ($query) use ($upline) {
                $query->where("code", $upline);
            })->first();
        });

        if (empty($user)) abort(404, "Invalid Upline code");

        return $user;
    }
}
