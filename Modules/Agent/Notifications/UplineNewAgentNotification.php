<?php

namespace Modules\Agent\Notifications;

use Illuminate\Notifications\Messages\MailMessage;
use Kutia\Larafirebase\Messages\FirebaseMessage;
use Illuminate\Notifications\Notification;
use Modules\Agent\Models\Agent;

class UplineNewAgentNotification extends Notification
{
    public function __construct(private Agent $agent)
    {
        $this->agent->load("user");
    }

    /**
     * Get the notification's channels.
     *
     * @param  mixed  $notifiable
     * @return array|string
     */
    public function via($notifiable)
    {
        return ['mail', 'firebase'];
    }

    public function toMail(object $notifiable): MailMessage
    {
        $message =  (new MailMessage)
            ->subject('XYZ New Downline')
            ->greeting("Hello $notifiable->display_name,")
            ->line("New agent has registered as your downline")
            ->line("Agent No: " . $this->agent->code)
            ->line("Please accept or reject using XYZ Mobile App");
        return $message;
    }

    /**
     * Get the firebase representation of the notification.
     */
    public function toFirebase($notifiable)
    {
        return (new FirebaseMessage)
            ->withTitle('XYZ New Downline')
            ->withBody("Agent No: " . $this->agent->code . " has registered as your downline")
            ->withPriority('high')
            ->asNotification($notifiable->fcm_token);
    }

    public function shouldSend(object $notifiable, string $channel): bool
    {

        if($channel === 'firebase') {
            $notifiable->append('fcm_token');
            return !empty($notifiable->fcm_token);
        }

        return true;
    }
}
