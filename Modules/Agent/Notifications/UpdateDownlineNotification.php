<?php

namespace Modules\Agent\Notifications;

use Illuminate\Notifications\Messages\MailMessage;
use Kutia\Larafirebase\Messages\FirebaseMessage;
use Illuminate\Notifications\Notification;
use Modules\Agent\Models\Agent;

class UpdateDownlineNotification extends Notification
{
    public function __construct(private Agent $agent)
    {}

    /**
     * Get the notification's channels.
     *
     * @param  mixed  $notifiable
     * @return array|string
     */
    public function via($notifiable)
    {
        return ['mail', 'firebase'];
    }

    public function toMail(object $notifiable): MailMessage
    {
        $message =  (new MailMessage)
            ->subject('XYZ Application Status')
            ->greeting("Hello $notifiable->display_name,")
            ->line("The status of your application to join has been " . $this->status($this->agent->status) . ".");
        return $message;
    }

    /**
     * Get the firebase representation of the notification.
     */
    public function toFirebase($notifiable)
    {
        return (new FirebaseMessage)
            ->withTitle('XYZ Application Status!')
            ->withBody("The status of your application to join has been " . $this->status($this->agent->status) . ".")
            ->withPriority('high')
            ->asNotification($notifiable->fcm_token);
    }

    public function status($status) {
        $statuses = [
            Agent::STATUS_ACTIVE => Agent::STATUS_APPROVED,
            Agent::STATUS_REJECTED => Agent::STATUS_REJECTED,
        ];

        return $statuses[$status];
    }

    public function shouldSend(object $notifiable, string $channel): bool
    {

        if($channel === 'firebase') {
            $notifiable->append('fcm_token');
            return !empty($notifiable->fcm_token);
        }

        return true;
    }
}
