<?php

namespace Modules\Crud\Http\Controllers;

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::apiResources([
    'agent' => AgentController::class,

    'audit' => AuditController::class,

    'common-attachment' => CommonAttachmentController::class,

    'common-reference' => CommonReferenceController::class,

    'common-refno' => CommonRefnoController::class,

    'order-list' => OrderListController::class,

    'permission' => PermissionController::class,

    'role' => RoleController::class,

    'socmed-post' => SocmedPostController::class,

    'user-profile' => UserProfileController::class,

    'user-socialmedia' => UserSocialmediaController::class,

    'user' => UserController::class,

    'war-asset-inbound' => WarAssetInboundController::class,

    'war-asset-outbound' => WarAssetOutboundController::class,

    'war-price' => WarPriceController::class,

    'war-product' => WarProductController::class,

    'war-stock' => WarStockController::class,
]);