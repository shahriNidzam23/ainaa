<?php

namespace Modules\Crud\Repositories;

use Rekamy\LaravelCore\Crudable\Concern\CrudableRepository;
use Rekamy\LaravelCore\Contracts\CrudableRepository as CrudableRepositoryContract;
use Modules\Crud\Models\WarPrice;
use Rekamy\LaravelCore\Override\Repository;

/**
 * Class WarPriceRepository
 * @package Modules\Crud\Repositories
 *
 * @method WarPrice find($id, $columns = ['*'])
 * @method WarPrice find($id, $columns = ['*'])
 * @method WarPrice first($columns = ['*'])
*/
class WarPriceRepository extends Repository implements CrudableRepositoryContract
{
    use CrudableRepository;

    /**
     * @var array
     */
    protected $fieldSearchable = [
		'product_id',
		'name',
		'code',
		'value',
		'active',
	];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return WarPrice::class;
    }
    
}

