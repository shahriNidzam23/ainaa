<?php

namespace Modules\Crud\Repositories;

use Rekamy\LaravelCore\Crudable\Concern\CrudableRepository;
use Rekamy\LaravelCore\Contracts\CrudableRepository as CrudableRepositoryContract;
use Modules\Crud\Models\User;
use Rekamy\LaravelCore\Override\Repository;

/**
 * Class UserRepository
 * @package Modules\Crud\Repositories
 *
 * @method User find($id, $columns = ['*'])
 * @method User find($id, $columns = ['*'])
 * @method User first($columns = ['*'])
*/
class UserRepository extends Repository implements CrudableRepositoryContract
{
    use CrudableRepository;

    /**
     * @var array
     */
    protected $fieldSearchable = [
		'email',
		'display_name',
		'email_verified_at',
		'ott',
	];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return User::class;
    }
    
}

