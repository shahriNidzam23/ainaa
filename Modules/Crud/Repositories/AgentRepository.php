<?php

namespace Modules\Crud\Repositories;

use Rekamy\LaravelCore\Crudable\Concern\CrudableRepository;
use Rekamy\LaravelCore\Contracts\CrudableRepository as CrudableRepositoryContract;
use Modules\Crud\Models\Agent;
use Rekamy\LaravelCore\Override\Repository;

/**
 * Class AgentRepository
 * @package Modules\Crud\Repositories
 *
 * @method Agent find($id, $columns = ['*'])
 * @method Agent find($id, $columns = ['*'])
 * @method Agent first($columns = ['*'])
*/
class AgentRepository extends Repository implements CrudableRepositoryContract
{
    use CrudableRepository;

    /**
     * @var array
     */
    protected $fieldSearchable = [
		'user_id',
		'upline_id',
	];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Agent::class;
    }
    
}

