<?php

namespace Modules\Crud\Repositories;

use Rekamy\LaravelCore\Crudable\Concern\CrudableRepository;
use Rekamy\LaravelCore\Contracts\CrudableRepository as CrudableRepositoryContract;
use Modules\Crud\Models\TestList;
use Rekamy\LaravelCore\Override\Repository;

/**
 * Class TestListRepository
 * @package Modules\Crud\Repositories
 *
 * @method TestList find($id, $columns = ['*'])
 * @method TestList find($id, $columns = ['*'])
 * @method TestList first($columns = ['*'])
*/
class TestListRepository extends Repository implements CrudableRepositoryContract
{
    use CrudableRepository;

    /**
     * @var array
     */
    protected $fieldSearchable = [
		'name',
	];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TestList::class;
    }
    
}

