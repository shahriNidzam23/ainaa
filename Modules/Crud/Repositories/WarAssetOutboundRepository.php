<?php

namespace Modules\Crud\Repositories;

use Rekamy\LaravelCore\Crudable\Concern\CrudableRepository;
use Rekamy\LaravelCore\Contracts\CrudableRepository as CrudableRepositoryContract;
use Modules\Crud\Models\WarAssetOutbound;
use Rekamy\LaravelCore\Override\Repository;

/**
 * Class WarAssetOutboundRepository
 * @package Modules\Crud\Repositories
 *
 * @method WarAssetOutbound find($id, $columns = ['*'])
 * @method WarAssetOutbound find($id, $columns = ['*'])
 * @method WarAssetOutbound first($columns = ['*'])
*/
class WarAssetOutboundRepository extends Repository implements CrudableRepositoryContract
{
    use CrudableRepository;

    /**
     * @var array
     */
    protected $fieldSearchable = [
		'product_id',
		'manufacturer',
		'batch_id',
		'stockist_id',
		'quantity',
	];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return WarAssetOutbound::class;
    }
    
}

