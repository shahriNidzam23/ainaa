<?php

namespace Modules\Crud\Repositories;

use Rekamy\LaravelCore\Crudable\Concern\CrudableRepository;
use Rekamy\LaravelCore\Contracts\CrudableRepository as CrudableRepositoryContract;
use Modules\Crud\Models\OrdSale;
use Rekamy\LaravelCore\Override\Repository;

/**
 * Class OrdSaleRepository
 * @package Modules\Crud\Repositories
 *
 * @method OrdSale find($id, $columns = ['*'])
 * @method OrdSale find($id, $columns = ['*'])
 * @method OrdSale first($columns = ['*'])
*/
class OrdSaleRepository extends Repository implements CrudableRepositoryContract
{
    use CrudableRepository;

    /**
     * @var array
     */
    protected $fieldSearchable = [
		'user_id',
		'ref_no',
		'total_price',
		'status',
		'notes',
	];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return OrdSale::class;
    }
    
}

