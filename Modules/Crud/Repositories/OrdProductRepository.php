<?php

namespace Modules\Crud\Repositories;

use Rekamy\LaravelCore\Crudable\Concern\CrudableRepository;
use Rekamy\LaravelCore\Contracts\CrudableRepository as CrudableRepositoryContract;
use Modules\Crud\Models\OrdProduct;
use Rekamy\LaravelCore\Override\Repository;

/**
 * Class OrdProductRepository
 * @package Modules\Crud\Repositories
 *
 * @method OrdProduct find($id, $columns = ['*'])
 * @method OrdProduct find($id, $columns = ['*'])
 * @method OrdProduct first($columns = ['*'])
*/
class OrdProductRepository extends Repository implements CrudableRepositoryContract
{
    use CrudableRepository;

    /**
     * @var array
     */
    protected $fieldSearchable = [
		'parent_type',
		'parent_id',
		'name',
		'sku',
		'expiry_date',
		'price',
		'price_code',
		'quantity',
	];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return OrdProduct::class;
    }
    
}

