<?php

namespace Modules\Crud\Repositories;

use Rekamy\LaravelCore\Crudable\Concern\CrudableRepository;
use Rekamy\LaravelCore\Contracts\CrudableRepository as CrudableRepositoryContract;
use Modules\Crud\Models\OrdTax;
use Rekamy\LaravelCore\Override\Repository;

/**
 * Class OrdTaxRepository
 * @package Modules\Crud\Repositories
 *
 * @method OrdTax find($id, $columns = ['*'])
 * @method OrdTax find($id, $columns = ['*'])
 * @method OrdTax first($columns = ['*'])
*/
class OrdTaxRepository extends Repository implements CrudableRepositoryContract
{
    use CrudableRepository;

    /**
     * @var array
     */
    protected $fieldSearchable = [
		'parent_type',
		'parent_id',
		'name',
		'value',
	];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return OrdTax::class;
    }
    
}

