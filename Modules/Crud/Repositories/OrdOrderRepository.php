<?php

namespace Modules\Crud\Repositories;

use Rekamy\LaravelCore\Crudable\Concern\CrudableRepository;
use Rekamy\LaravelCore\Contracts\CrudableRepository as CrudableRepositoryContract;
use Modules\Crud\Models\OrdOrder;
use Rekamy\LaravelCore\Override\Repository;

/**
 * Class OrdOrderRepository
 * @package Modules\Crud\Repositories
 *
 * @method OrdOrder find($id, $columns = ['*'])
 * @method OrdOrder find($id, $columns = ['*'])
 * @method OrdOrder first($columns = ['*'])
*/
class OrdOrderRepository extends Repository implements CrudableRepositoryContract
{
    use CrudableRepository;

    /**
     * @var array
     */
    protected $fieldSearchable = [
		'seller_id',
		'buyer_id',
		'payment_type',
		'status',
		'ref_no',
		'total_price',
		'notes',
	];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return OrdOrder::class;
    }
    
}

