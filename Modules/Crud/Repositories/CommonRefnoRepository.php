<?php

namespace Modules\Crud\Repositories;

use Rekamy\LaravelCore\Crudable\Concern\CrudableRepository;
use Rekamy\LaravelCore\Contracts\CrudableRepository as CrudableRepositoryContract;
use Modules\Crud\Models\CommonRefno;
use Rekamy\LaravelCore\Override\Repository;

/**
 * Class CommonRefnoRepository
 * @package Modules\Crud\Repositories
 *
 * @method CommonRefno find($id, $columns = ['*'])
 * @method CommonRefno find($id, $columns = ['*'])
 * @method CommonRefno first($columns = ['*'])
*/
class CommonRefnoRepository extends Repository implements CrudableRepositoryContract
{
    use CrudableRepository;

    /**
     * @var array
     */
    protected $fieldSearchable = [
		'name',
		'prefix',
		'running_no',
	];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CommonRefno::class;
    }
    
}

