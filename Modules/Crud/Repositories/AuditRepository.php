<?php

namespace Modules\Crud\Repositories;

use Rekamy\LaravelCore\Crudable\Concern\CrudableRepository;
use Rekamy\LaravelCore\Contracts\CrudableRepository as CrudableRepositoryContract;
use Modules\Crud\Models\Audit;
use Rekamy\LaravelCore\Override\Repository;

/**
 * Class AuditRepository
 * @package Modules\Crud\Repositories
 *
 * @method Audit find($id, $columns = ['*'])
 * @method Audit find($id, $columns = ['*'])
 * @method Audit first($columns = ['*'])
*/
class AuditRepository extends Repository implements CrudableRepositoryContract
{
    use CrudableRepository;

    /**
     * @var array
     */
    protected $fieldSearchable = [
		'user_type',
		'user_id',
		'event',
		'auditable_type',
		'auditable_id',
		'old_values',
		'new_values',
		'url',
		'ip_address',
		'user_agent',
		'tags',
	];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Audit::class;
    }
    
}

