<?php

namespace Modules\Crud\Repositories;

use Rekamy\LaravelCore\Crudable\Concern\CrudableRepository;
use Rekamy\LaravelCore\Contracts\CrudableRepository as CrudableRepositoryContract;
use Modules\Crud\Models\Permission;
use Rekamy\LaravelCore\Override\Repository;

/**
 * Class PermissionRepository
 * @package Modules\Crud\Repositories
 *
 * @method Permission find($id, $columns = ['*'])
 * @method Permission find($id, $columns = ['*'])
 * @method Permission first($columns = ['*'])
*/
class PermissionRepository extends Repository implements CrudableRepositoryContract
{
    use CrudableRepository;

    /**
     * @var array
     */
    protected $fieldSearchable = [
		'name',
		'module',
		'description',
		'guard_name',
	];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Permission::class;
    }
    
}

