<?php

namespace Modules\Crud\Repositories;

use Rekamy\LaravelCore\Crudable\Concern\CrudableRepository;
use Rekamy\LaravelCore\Contracts\CrudableRepository as CrudableRepositoryContract;
use Modules\Crud\Models\UserProfile;
use Rekamy\LaravelCore\Override\Repository;

/**
 * Class UserProfileRepository
 * @package Modules\Crud\Repositories
 *
 * @method UserProfile find($id, $columns = ['*'])
 * @method UserProfile find($id, $columns = ['*'])
 * @method UserProfile first($columns = ['*'])
*/
class UserProfileRepository extends Repository implements CrudableRepositoryContract
{
    use CrudableRepository;

    /**
     * @var array
     */
    protected $fieldSearchable = [
		'user_id',
		'fullname',
		'phone_number',
		'photo_url',
	];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return UserProfile::class;
    }
    
}

