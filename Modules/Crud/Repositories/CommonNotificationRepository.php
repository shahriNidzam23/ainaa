<?php

namespace Modules\Crud\Repositories;

use Rekamy\LaravelCore\Crudable\Concern\CrudableRepository;
use Rekamy\LaravelCore\Contracts\CrudableRepository as CrudableRepositoryContract;
use Modules\Crud\Models\CommonNotification;
use Rekamy\LaravelCore\Override\Repository;

/**
 * Class CommonNotificationRepository
 * @package Modules\Crud\Repositories
 *
 * @method CommonNotification find($id, $columns = ['*'])
 * @method CommonNotification find($id, $columns = ['*'])
 * @method CommonNotification first($columns = ['*'])
*/
class CommonNotificationRepository extends Repository implements CrudableRepositoryContract
{
    use CrudableRepository;

    /**
     * @var array
     */
    protected $fieldSearchable = [
		'user_id',
		'token',
	];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CommonNotification::class;
    }
    
}

