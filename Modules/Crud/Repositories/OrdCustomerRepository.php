<?php

namespace Modules\Crud\Repositories;

use Rekamy\LaravelCore\Crudable\Concern\CrudableRepository;
use Rekamy\LaravelCore\Contracts\CrudableRepository as CrudableRepositoryContract;
use Modules\Crud\Models\OrdCustomer;
use Rekamy\LaravelCore\Override\Repository;

/**
 * Class OrdCustomerRepository
 * @package Modules\Crud\Repositories
 *
 * @method OrdCustomer find($id, $columns = ['*'])
 * @method OrdCustomer find($id, $columns = ['*'])
 * @method OrdCustomer first($columns = ['*'])
*/
class OrdCustomerRepository extends Repository implements CrudableRepositoryContract
{
    use CrudableRepository;

    /**
     * @var array
     */
    protected $fieldSearchable = [
		'sale_id',
		'name',
		'phone_number',
		'email',
	];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return OrdCustomer::class;
    }
    
}

