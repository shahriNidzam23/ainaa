<?php

namespace Modules\Crud\Repositories;

use Rekamy\LaravelCore\Crudable\Concern\CrudableRepository;
use Rekamy\LaravelCore\Contracts\CrudableRepository as CrudableRepositoryContract;
use Modules\Crud\Models\WarStock;
use Rekamy\LaravelCore\Override\Repository;

/**
 * Class WarStockRepository
 * @package Modules\Crud\Repositories
 *
 * @method WarStock find($id, $columns = ['*'])
 * @method WarStock find($id, $columns = ['*'])
 * @method WarStock first($columns = ['*'])
*/
class WarStockRepository extends Repository implements CrudableRepositoryContract
{
    use CrudableRepository;

    /**
     * @var array
     */
    protected $fieldSearchable = [
		'product_id',
		'user_id',
		'quantity',
	];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return WarStock::class;
    }
    
}

