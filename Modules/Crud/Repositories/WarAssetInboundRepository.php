<?php

namespace Modules\Crud\Repositories;

use Rekamy\LaravelCore\Crudable\Concern\CrudableRepository;
use Rekamy\LaravelCore\Contracts\CrudableRepository as CrudableRepositoryContract;
use Modules\Crud\Models\WarAssetInbound;
use Rekamy\LaravelCore\Override\Repository;

/**
 * Class WarAssetInboundRepository
 * @package Modules\Crud\Repositories
 *
 * @method WarAssetInbound find($id, $columns = ['*'])
 * @method WarAssetInbound find($id, $columns = ['*'])
 * @method WarAssetInbound first($columns = ['*'])
*/
class WarAssetInboundRepository extends Repository implements CrudableRepositoryContract
{
    use CrudableRepository;

    /**
     * @var array
     */
    protected $fieldSearchable = [
		'product_id',
		'manufacturer',
		'batch_id',
		'receive_quantity',
		'rejected_quantity',
		'accepted_quantity',
	];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return WarAssetInbound::class;
    }
    
}

