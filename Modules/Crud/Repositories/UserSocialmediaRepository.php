<?php

namespace Modules\Crud\Repositories;

use Rekamy\LaravelCore\Crudable\Concern\CrudableRepository;
use Rekamy\LaravelCore\Contracts\CrudableRepository as CrudableRepositoryContract;
use Modules\Crud\Models\UserSocialmedia;
use Rekamy\LaravelCore\Override\Repository;

/**
 * Class UserSocialmediaRepository
 * @package Modules\Crud\Repositories
 *
 * @method UserSocialmedia find($id, $columns = ['*'])
 * @method UserSocialmedia find($id, $columns = ['*'])
 * @method UserSocialmedia first($columns = ['*'])
*/
class UserSocialmediaRepository extends Repository implements CrudableRepositoryContract
{
    use CrudableRepository;

    /**
     * @var array
     */
    protected $fieldSearchable = [
		'user_id',
		'platform',
		'username',
	];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return UserSocialmedia::class;
    }
    
}

