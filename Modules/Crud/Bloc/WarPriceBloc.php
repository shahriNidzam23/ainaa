<?php

namespace Modules\Crud\Bloc;

use Rekamy\LaravelCore\Crudable\Abstract\CrudBloc;
use Modules\Crud\Repositories\WarPriceRepository;
use Modules\Crud\Http\Requests\WarPriceRequest;

class WarPriceBloc extends CrudBloc
{
    public function __construct(
        public WarPriceRepository $repo, 
        public WarPriceRequest $request,
    ) {
        $this->repo = $repo;
        $this->request = $request;
    }

    public static function permission($name) {
    //     $permission = [
    //         'index' => 'war_prices_index',
    //         'create' => 'war_prices_create',
    //         'show' => 'war_prices_show',
    //         'update' => 'war_prices_update',
    //         'destroy' => 'war_prices_destroy',
    //     ];
        
    //     return $permission[$name];
    }
}
