<?php

namespace Modules\Crud\Bloc;

use Rekamy\LaravelCore\Crudable\Abstract\CrudBloc;
use Modules\Crud\Repositories\OrdTaxRepository;
use Modules\Crud\Http\Requests\OrdTaxRequest;

class OrdTaxBloc extends CrudBloc
{
    public function __construct(
        public OrdTaxRepository $repo, 
        public OrdTaxRequest $request,
    ) {
        $this->repo = $repo;
        $this->request = $request;
    }

    public static function permission($name) {
    //     $permission = [
    //         'index' => 'ord_taxes_index',
    //         'create' => 'ord_taxes_create',
    //         'show' => 'ord_taxes_show',
    //         'update' => 'ord_taxes_update',
    //         'destroy' => 'ord_taxes_destroy',
    //     ];
        
    //     return $permission[$name];
    }
}
