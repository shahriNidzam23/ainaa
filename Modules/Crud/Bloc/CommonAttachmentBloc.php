<?php

namespace Modules\Crud\Bloc;

use Rekamy\LaravelCore\Crudable\Abstract\CrudBloc;
use Modules\Crud\Repositories\CommonAttachmentRepository;
use Modules\Crud\Http\Requests\CommonAttachmentRequest;

class CommonAttachmentBloc extends CrudBloc
{
    public function __construct(
        public CommonAttachmentRepository $repo, 
        public CommonAttachmentRequest $request,
    ) {
        $this->repo = $repo;
        $this->request = $request;
    }

    public static function permission($name) {
    //     $permission = [
    //         'index' => 'common_attachments_index',
    //         'create' => 'common_attachments_create',
    //         'show' => 'common_attachments_show',
    //         'update' => 'common_attachments_update',
    //         'destroy' => 'common_attachments_destroy',
    //     ];
        
    //     return $permission[$name];
    }
}
