<?php

namespace Modules\Crud\Bloc;

use Rekamy\LaravelCore\Crudable\Abstract\CrudBloc;
use Modules\Crud\Repositories\OrdSaleRepository;
use Modules\Crud\Http\Requests\OrdSaleRequest;

class OrdSaleBloc extends CrudBloc
{
    public function __construct(
        public OrdSaleRepository $repo, 
        public OrdSaleRequest $request,
    ) {
        $this->repo = $repo;
        $this->request = $request;
    }

    public static function permission($name) {
    //     $permission = [
    //         'index' => 'ord_sales_index',
    //         'create' => 'ord_sales_create',
    //         'show' => 'ord_sales_show',
    //         'update' => 'ord_sales_update',
    //         'destroy' => 'ord_sales_destroy',
    //     ];
        
    //     return $permission[$name];
    }
}
