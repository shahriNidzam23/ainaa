<?php

namespace Modules\Crud\Bloc;

use Rekamy\LaravelCore\Crudable\Abstract\CrudBloc;
use Modules\Crud\Repositories\CommonNotificationRepository;
use Modules\Crud\Http\Requests\CommonNotificationRequest;

class CommonNotificationBloc extends CrudBloc
{
    public function __construct(
        public CommonNotificationRepository $repo, 
        public CommonNotificationRequest $request,
    ) {
        $this->repo = $repo;
        $this->request = $request;
    }

    public static function permission($name) {
    //     $permission = [
    //         'index' => 'common_notifications_index',
    //         'create' => 'common_notifications_create',
    //         'show' => 'common_notifications_show',
    //         'update' => 'common_notifications_update',
    //         'destroy' => 'common_notifications_destroy',
    //     ];
        
    //     return $permission[$name];
    }
}
