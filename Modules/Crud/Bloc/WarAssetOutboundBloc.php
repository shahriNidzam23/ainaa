<?php

namespace Modules\Crud\Bloc;

use Rekamy\LaravelCore\Crudable\Abstract\CrudBloc;
use Modules\Crud\Repositories\WarAssetOutboundRepository;
use Modules\Crud\Http\Requests\WarAssetOutboundRequest;

class WarAssetOutboundBloc extends CrudBloc
{
    public function __construct(
        public WarAssetOutboundRepository $repo, 
        public WarAssetOutboundRequest $request,
    ) {
        $this->repo = $repo;
        $this->request = $request;
    }

    public static function permission($name) {
    //     $permission = [
    //         'index' => 'war_asset_outbound_index',
    //         'create' => 'war_asset_outbound_create',
    //         'show' => 'war_asset_outbound_show',
    //         'update' => 'war_asset_outbound_update',
    //         'destroy' => 'war_asset_outbound_destroy',
    //     ];
        
    //     return $permission[$name];
    }
}
