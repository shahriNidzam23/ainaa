<?php

namespace Modules\Crud\Bloc;

use Rekamy\LaravelCore\Crudable\Abstract\CrudBloc;
use Modules\Crud\Repositories\WarStockRepository;
use Modules\Crud\Http\Requests\WarStockRequest;

class WarStockBloc extends CrudBloc
{
    public function __construct(
        public WarStockRepository $repo, 
        public WarStockRequest $request,
    ) {
        $this->repo = $repo;
        $this->request = $request;
    }

    public static function permission($name) {
    //     $permission = [
    //         'index' => 'war_stocks_index',
    //         'create' => 'war_stocks_create',
    //         'show' => 'war_stocks_show',
    //         'update' => 'war_stocks_update',
    //         'destroy' => 'war_stocks_destroy',
    //     ];
        
    //     return $permission[$name];
    }
}
