<?php

namespace Modules\Crud\Bloc;

use Rekamy\LaravelCore\Crudable\Abstract\CrudBloc;
use Modules\Crud\Repositories\OrdProductRepository;
use Modules\Crud\Http\Requests\OrdProductRequest;

class OrdProductBloc extends CrudBloc
{
    public function __construct(
        public OrdProductRepository $repo, 
        public OrdProductRequest $request,
    ) {
        $this->repo = $repo;
        $this->request = $request;
    }

    public static function permission($name) {
    //     $permission = [
    //         'index' => 'ord_products_index',
    //         'create' => 'ord_products_create',
    //         'show' => 'ord_products_show',
    //         'update' => 'ord_products_update',
    //         'destroy' => 'ord_products_destroy',
    //     ];
        
    //     return $permission[$name];
    }
}
