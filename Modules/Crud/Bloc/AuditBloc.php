<?php

namespace Modules\Crud\Bloc;

use Rekamy\LaravelCore\Crudable\Abstract\CrudBloc;
use Modules\Crud\Repositories\AuditRepository;
use Modules\Crud\Http\Requests\AuditRequest;

class AuditBloc extends CrudBloc
{
    public function __construct(
        public AuditRepository $repo, 
        public AuditRequest $request,
    ) {
        $this->repo = $repo;
        $this->request = $request;
    }

    public static function permission($name) {
    //     $permission = [
    //         'index' => 'audits_index',
    //         'create' => 'audits_create',
    //         'show' => 'audits_show',
    //         'update' => 'audits_update',
    //         'destroy' => 'audits_destroy',
    //     ];
        
    //     return $permission[$name];
    }
}
