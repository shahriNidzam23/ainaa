<?php

namespace Modules\Crud\Bloc;

use Rekamy\LaravelCore\Crudable\Abstract\CrudBloc;
use Modules\Crud\Repositories\OrdOrderRepository;
use Modules\Crud\Http\Requests\OrdOrderRequest;

class OrdOrderBloc extends CrudBloc
{
    public function __construct(
        public OrdOrderRepository $repo, 
        public OrdOrderRequest $request,
    ) {
        $this->repo = $repo;
        $this->request = $request;
    }

    public static function permission($name) {
    //     $permission = [
    //         'index' => 'ord_orders_index',
    //         'create' => 'ord_orders_create',
    //         'show' => 'ord_orders_show',
    //         'update' => 'ord_orders_update',
    //         'destroy' => 'ord_orders_destroy',
    //     ];
        
    //     return $permission[$name];
    }
}
