<?php

namespace Modules\Crud\Bloc;

use Rekamy\LaravelCore\Crudable\Abstract\CrudBloc;
use Modules\Crud\Repositories\WarAssetInboundRepository;
use Modules\Crud\Http\Requests\WarAssetInboundRequest;

class WarAssetInboundBloc extends CrudBloc
{
    public function __construct(
        public WarAssetInboundRepository $repo, 
        public WarAssetInboundRequest $request,
    ) {
        $this->repo = $repo;
        $this->request = $request;
    }

    public static function permission($name) {
    //     $permission = [
    //         'index' => 'war_asset_inbound_index',
    //         'create' => 'war_asset_inbound_create',
    //         'show' => 'war_asset_inbound_show',
    //         'update' => 'war_asset_inbound_update',
    //         'destroy' => 'war_asset_inbound_destroy',
    //     ];
        
    //     return $permission[$name];
    }
}
