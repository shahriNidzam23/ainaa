<?php

namespace Modules\Crud\Bloc;

use Rekamy\LaravelCore\Crudable\Abstract\CrudBloc;
use Modules\Crud\Repositories\WarProductRepository;
use Modules\Crud\Http\Requests\WarProductRequest;

class WarProductBloc extends CrudBloc
{
    public function __construct(
        public WarProductRepository $repo, 
        public WarProductRequest $request,
    ) {
        $this->repo = $repo;
        $this->request = $request;
    }

    public static function permission($name) {
    //     $permission = [
    //         'index' => 'war_products_index',
    //         'create' => 'war_products_create',
    //         'show' => 'war_products_show',
    //         'update' => 'war_products_update',
    //         'destroy' => 'war_products_destroy',
    //     ];
        
    //     return $permission[$name];
    }
}
