<?php

namespace Modules\Crud\Bloc;

use Rekamy\LaravelCore\Crudable\Abstract\CrudBloc;
use Modules\Crud\Repositories\UserRepository;
use Modules\Crud\Http\Requests\UserRequest;

class UserBloc extends CrudBloc
{
    public function __construct(
        public UserRepository $repo, 
        public UserRequest $request,
    ) {
        $this->repo = $repo;
        $this->request = $request;
    }

    public static function permission($name) {
    //     $permission = [
    //         'index' => 'users_index',
    //         'create' => 'users_create',
    //         'show' => 'users_show',
    //         'update' => 'users_update',
    //         'destroy' => 'users_destroy',
    //     ];
        
    //     return $permission[$name];
    }
}
