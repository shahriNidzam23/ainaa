<?php

namespace Modules\Crud\Bloc;

use Rekamy\LaravelCore\Crudable\Abstract\CrudBloc;
use Modules\Crud\Repositories\AgentRepository;
use Modules\Crud\Http\Requests\AgentRequest;

class AgentBloc extends CrudBloc
{
    public function __construct(
        public AgentRepository $repo, 
        public AgentRequest $request,
    ) {
        $this->repo = $repo;
        $this->request = $request;
    }

    public static function permission($name) {
    //     $permission = [
    //         'index' => 'agents_index',
    //         'create' => 'agents_create',
    //         'show' => 'agents_show',
    //         'update' => 'agents_update',
    //         'destroy' => 'agents_destroy',
    //     ];
        
    //     return $permission[$name];
    }
}
