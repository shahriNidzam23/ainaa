<?php

namespace Modules\Crud\Bloc;

use Rekamy\LaravelCore\Crudable\Abstract\CrudBloc;
use Modules\Crud\Repositories\TestListRepository;
use Modules\Crud\Http\Requests\TestListRequest;

class TestListBloc extends CrudBloc
{
    public function __construct(
        public TestListRepository $repo, 
        public TestListRequest $request,
    ) {
        $this->repo = $repo;
        $this->request = $request;
    }

    public static function permission($name) {
    //     $permission = [
    //         'index' => 'test_lists_index',
    //         'create' => 'test_lists_create',
    //         'show' => 'test_lists_show',
    //         'update' => 'test_lists_update',
    //         'destroy' => 'test_lists_destroy',
    //     ];
        
    //     return $permission[$name];
    }
}
