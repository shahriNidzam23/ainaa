<?php

namespace Modules\Crud\Bloc;

use Rekamy\LaravelCore\Crudable\Abstract\CrudBloc;
use Modules\Crud\Repositories\OrdCustomerRepository;
use Modules\Crud\Http\Requests\OrdCustomerRequest;

class OrdCustomerBloc extends CrudBloc
{
    public function __construct(
        public OrdCustomerRepository $repo, 
        public OrdCustomerRequest $request,
    ) {
        $this->repo = $repo;
        $this->request = $request;
    }

    public static function permission($name) {
    //     $permission = [
    //         'index' => 'ord_customers_index',
    //         'create' => 'ord_customers_create',
    //         'show' => 'ord_customers_show',
    //         'update' => 'ord_customers_update',
    //         'destroy' => 'ord_customers_destroy',
    //     ];
        
    //     return $permission[$name];
    }
}
