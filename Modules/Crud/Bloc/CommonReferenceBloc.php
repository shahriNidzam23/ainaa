<?php

namespace Modules\Crud\Bloc;

use Rekamy\LaravelCore\Crudable\Abstract\CrudBloc;
use Modules\Crud\Repositories\CommonReferenceRepository;
use Modules\Crud\Http\Requests\CommonReferenceRequest;

class CommonReferenceBloc extends CrudBloc
{
    public function __construct(
        public CommonReferenceRepository $repo, 
        public CommonReferenceRequest $request,
    ) {
        $this->repo = $repo;
        $this->request = $request;
    }

    public static function permission($name) {
    //     $permission = [
    //         'index' => 'common_references_index',
    //         'create' => 'common_references_create',
    //         'show' => 'common_references_show',
    //         'update' => 'common_references_update',
    //         'destroy' => 'common_references_destroy',
    //     ];
        
    //     return $permission[$name];
    }
}
