<?php

namespace Modules\Crud\Bloc;

use Rekamy\LaravelCore\Crudable\Abstract\CrudBloc;
use Modules\Crud\Repositories\SocmedPostRepository;
use Modules\Crud\Http\Requests\SocmedPostRequest;

class SocmedPostBloc extends CrudBloc
{
    public function __construct(
        public SocmedPostRepository $repo, 
        public SocmedPostRequest $request,
    ) {
        $this->repo = $repo;
        $this->request = $request;
    }

    public static function permission($name) {
    //     $permission = [
    //         'index' => 'socmed_posts_index',
    //         'create' => 'socmed_posts_create',
    //         'show' => 'socmed_posts_show',
    //         'update' => 'socmed_posts_update',
    //         'destroy' => 'socmed_posts_destroy',
    //     ];
        
    //     return $permission[$name];
    }
}
