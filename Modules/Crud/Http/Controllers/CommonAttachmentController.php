<?php

namespace Modules\Crud\Http\Controllers;

use Rekamy\LaravelCore\Crudable\Abstract\CrudController;
use Modules\Crud\Bloc\CommonAttachmentBloc;

class CommonAttachmentController extends CrudController
{
    protected $baseBloc;
    protected $moduleName = '';
    private $result;

    public function __construct(CommonAttachmentBloc $bloc) 
    {
        // $this->registerPassportScopes();
        $this->baseBloc = $bloc;
        $this->moduleName = 'common_attachments';
    }

    public function registerPassportScopes()
    {
        $this->middleware('scope:common_attachments_index')->only('index');
        $this->middleware('scope:common_attachments_create')->only('store');
        $this->middleware('scope:common_attachments_show')->only('show');
        $this->middleware('scope:common_attachments_update')->only('update');
        $this->middleware('scope:common_attachments_destroy')->only('destroy');
    }
}
