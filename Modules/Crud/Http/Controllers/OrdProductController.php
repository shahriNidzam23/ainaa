<?php

namespace Modules\Crud\Http\Controllers;

use Rekamy\LaravelCore\Crudable\Abstract\CrudController;
use Modules\Crud\Bloc\OrdProductBloc;

class OrdProductController extends CrudController
{
    protected $baseBloc;
    protected $moduleName = '';
    private $result;

    public function __construct(OrdProductBloc $bloc) 
    {
        // $this->registerPassportScopes();
        $this->baseBloc = $bloc;
        $this->moduleName = 'ord_products';
    }

    public function registerPassportScopes()
    {
        $this->middleware('scope:ord_products_index')->only('index');
        $this->middleware('scope:ord_products_create')->only('store');
        $this->middleware('scope:ord_products_show')->only('show');
        $this->middleware('scope:ord_products_update')->only('update');
        $this->middleware('scope:ord_products_destroy')->only('destroy');
    }
}
