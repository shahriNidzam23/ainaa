<?php

namespace Modules\Crud\Http\Controllers;

use Rekamy\LaravelCore\Crudable\Abstract\CrudController;
use Modules\Crud\Bloc\UserBloc;

class UserController extends CrudController
{
    protected $baseBloc;
    protected $moduleName = '';
    private $result;

    public function __construct(UserBloc $bloc) 
    {
        // $this->registerPassportScopes();
        $this->baseBloc = $bloc;
        $this->moduleName = 'users';
    }

    public function registerPassportScopes()
    {
        $this->middleware('scope:users_index')->only('index');
        $this->middleware('scope:users_create')->only('store');
        $this->middleware('scope:users_show')->only('show');
        $this->middleware('scope:users_update')->only('update');
        $this->middleware('scope:users_destroy')->only('destroy');
    }
}
