<?php

namespace Modules\Crud\Http\Controllers;

use Rekamy\LaravelCore\Crudable\Abstract\CrudController;
use Modules\Crud\Bloc\AgentBloc;

class AgentController extends CrudController
{
    protected $baseBloc;
    protected $moduleName = '';
    private $result;

    public function __construct(AgentBloc $bloc) 
    {
        // $this->registerPassportScopes();
        $this->baseBloc = $bloc;
        $this->moduleName = 'agents';
    }

    public function registerPassportScopes()
    {
        $this->middleware('scope:agents_index')->only('index');
        $this->middleware('scope:agents_create')->only('store');
        $this->middleware('scope:agents_show')->only('show');
        $this->middleware('scope:agents_update')->only('update');
        $this->middleware('scope:agents_destroy')->only('destroy');
    }
}
