<?php

namespace Modules\Crud\Http\Controllers;

use Rekamy\LaravelCore\Crudable\Abstract\CrudController;
use Modules\Crud\Bloc\PermissionBloc;

class PermissionController extends CrudController
{
    protected $baseBloc;
    protected $moduleName = '';
    private $result;

    public function __construct(PermissionBloc $bloc) 
    {
        // $this->registerPassportScopes();
        $this->baseBloc = $bloc;
        $this->moduleName = 'permissions';
    }

    public function registerPassportScopes()
    {
        $this->middleware('scope:permissions_index')->only('index');
        $this->middleware('scope:permissions_create')->only('store');
        $this->middleware('scope:permissions_show')->only('show');
        $this->middleware('scope:permissions_update')->only('update');
        $this->middleware('scope:permissions_destroy')->only('destroy');
    }
}
