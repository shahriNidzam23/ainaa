<?php

namespace Modules\Crud\Http\Controllers;

use Rekamy\LaravelCore\Crudable\Abstract\CrudController;
use Modules\Crud\Bloc\AuditBloc;

class AuditController extends CrudController
{
    protected $baseBloc;
    protected $moduleName = '';
    private $result;

    public function __construct(AuditBloc $bloc) 
    {
        // $this->registerPassportScopes();
        $this->baseBloc = $bloc;
        $this->moduleName = 'audits';
    }

    public function registerPassportScopes()
    {
        $this->middleware('scope:audits_index')->only('index');
        $this->middleware('scope:audits_create')->only('store');
        $this->middleware('scope:audits_show')->only('show');
        $this->middleware('scope:audits_update')->only('update');
        $this->middleware('scope:audits_destroy')->only('destroy');
    }
}
