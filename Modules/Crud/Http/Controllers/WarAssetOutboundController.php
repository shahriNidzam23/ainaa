<?php

namespace Modules\Crud\Http\Controllers;

use Rekamy\LaravelCore\Crudable\Abstract\CrudController;
use Modules\Crud\Bloc\WarAssetOutboundBloc;

class WarAssetOutboundController extends CrudController
{
    protected $baseBloc;
    protected $moduleName = '';
    private $result;

    public function __construct(WarAssetOutboundBloc $bloc) 
    {
        // $this->registerPassportScopes();
        $this->baseBloc = $bloc;
        $this->moduleName = 'war_asset_outbound';
    }

    public function registerPassportScopes()
    {
        $this->middleware('scope:war_asset_outbound_index')->only('index');
        $this->middleware('scope:war_asset_outbound_create')->only('store');
        $this->middleware('scope:war_asset_outbound_show')->only('show');
        $this->middleware('scope:war_asset_outbound_update')->only('update');
        $this->middleware('scope:war_asset_outbound_destroy')->only('destroy');
    }
}
