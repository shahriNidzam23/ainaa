<?php

namespace Modules\Crud\Http\Controllers;

use Rekamy\LaravelCore\Crudable\Abstract\CrudController;
use Modules\Crud\Bloc\OrdSaleBloc;

class OrdSaleController extends CrudController
{
    protected $baseBloc;
    protected $moduleName = '';
    private $result;

    public function __construct(OrdSaleBloc $bloc) 
    {
        // $this->registerPassportScopes();
        $this->baseBloc = $bloc;
        $this->moduleName = 'ord_sales';
    }

    public function registerPassportScopes()
    {
        $this->middleware('scope:ord_sales_index')->only('index');
        $this->middleware('scope:ord_sales_create')->only('store');
        $this->middleware('scope:ord_sales_show')->only('show');
        $this->middleware('scope:ord_sales_update')->only('update');
        $this->middleware('scope:ord_sales_destroy')->only('destroy');
    }
}
