<?php

namespace Modules\Crud\Http\Controllers;

use Rekamy\LaravelCore\Crudable\Abstract\CrudController;
use Modules\Crud\Bloc\WarAssetInboundBloc;

class WarAssetInboundController extends CrudController
{
    protected $baseBloc;
    protected $moduleName = '';
    private $result;

    public function __construct(WarAssetInboundBloc $bloc) 
    {
        // $this->registerPassportScopes();
        $this->baseBloc = $bloc;
        $this->moduleName = 'war_asset_inbound';
    }

    public function registerPassportScopes()
    {
        $this->middleware('scope:war_asset_inbound_index')->only('index');
        $this->middleware('scope:war_asset_inbound_create')->only('store');
        $this->middleware('scope:war_asset_inbound_show')->only('show');
        $this->middleware('scope:war_asset_inbound_update')->only('update');
        $this->middleware('scope:war_asset_inbound_destroy')->only('destroy');
    }
}
