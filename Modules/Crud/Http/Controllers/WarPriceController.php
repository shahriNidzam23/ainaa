<?php

namespace Modules\Crud\Http\Controllers;

use Rekamy\LaravelCore\Crudable\Abstract\CrudController;
use Modules\Crud\Bloc\WarPriceBloc;

class WarPriceController extends CrudController
{
    protected $baseBloc;
    protected $moduleName = '';
    private $result;

    public function __construct(WarPriceBloc $bloc) 
    {
        // $this->registerPassportScopes();
        $this->baseBloc = $bloc;
        $this->moduleName = 'war_prices';
    }

    public function registerPassportScopes()
    {
        $this->middleware('scope:war_prices_index')->only('index');
        $this->middleware('scope:war_prices_create')->only('store');
        $this->middleware('scope:war_prices_show')->only('show');
        $this->middleware('scope:war_prices_update')->only('update');
        $this->middleware('scope:war_prices_destroy')->only('destroy');
    }
}
