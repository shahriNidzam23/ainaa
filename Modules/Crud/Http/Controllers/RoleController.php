<?php

namespace Modules\Crud\Http\Controllers;

use Rekamy\LaravelCore\Crudable\Abstract\CrudController;
use Modules\Crud\Bloc\RoleBloc;

class RoleController extends CrudController
{
    protected $baseBloc;
    protected $moduleName = '';
    private $result;

    public function __construct(RoleBloc $bloc) 
    {
        // $this->registerPassportScopes();
        $this->baseBloc = $bloc;
        $this->moduleName = 'roles';
    }

    public function registerPassportScopes()
    {
        $this->middleware('scope:roles_index')->only('index');
        $this->middleware('scope:roles_create')->only('store');
        $this->middleware('scope:roles_show')->only('show');
        $this->middleware('scope:roles_update')->only('update');
        $this->middleware('scope:roles_destroy')->only('destroy');
    }
}
