<?php

namespace Modules\Crud\Http\Controllers;

use Rekamy\LaravelCore\Crudable\Abstract\CrudController;
use Modules\Crud\Bloc\OrdOrderBloc;

class OrdOrderController extends CrudController
{
    protected $baseBloc;
    protected $moduleName = '';
    private $result;

    public function __construct(OrdOrderBloc $bloc) 
    {
        // $this->registerPassportScopes();
        $this->baseBloc = $bloc;
        $this->moduleName = 'ord_orders';
    }

    public function registerPassportScopes()
    {
        $this->middleware('scope:ord_orders_index')->only('index');
        $this->middleware('scope:ord_orders_create')->only('store');
        $this->middleware('scope:ord_orders_show')->only('show');
        $this->middleware('scope:ord_orders_update')->only('update');
        $this->middleware('scope:ord_orders_destroy')->only('destroy');
    }
}
