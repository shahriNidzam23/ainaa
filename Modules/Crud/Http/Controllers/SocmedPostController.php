<?php

namespace Modules\Crud\Http\Controllers;

use Rekamy\LaravelCore\Crudable\Abstract\CrudController;
use Modules\Crud\Bloc\SocmedPostBloc;

class SocmedPostController extends CrudController
{
    protected $baseBloc;
    protected $moduleName = '';
    private $result;

    public function __construct(SocmedPostBloc $bloc) 
    {
        // $this->registerPassportScopes();
        $this->baseBloc = $bloc;
        $this->moduleName = 'socmed_posts';
    }

    public function registerPassportScopes()
    {
        $this->middleware('scope:socmed_posts_index')->only('index');
        $this->middleware('scope:socmed_posts_create')->only('store');
        $this->middleware('scope:socmed_posts_show')->only('show');
        $this->middleware('scope:socmed_posts_update')->only('update');
        $this->middleware('scope:socmed_posts_destroy')->only('destroy');
    }
}
