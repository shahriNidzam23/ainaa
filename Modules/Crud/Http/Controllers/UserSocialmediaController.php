<?php

namespace Modules\Crud\Http\Controllers;

use Rekamy\LaravelCore\Crudable\Abstract\CrudController;
use Modules\Crud\Bloc\UserSocialmediaBloc;

class UserSocialmediaController extends CrudController
{
    protected $baseBloc;
    protected $moduleName = '';
    private $result;

    public function __construct(UserSocialmediaBloc $bloc) 
    {
        // $this->registerPassportScopes();
        $this->baseBloc = $bloc;
        $this->moduleName = 'user_socialmedias';
    }

    public function registerPassportScopes()
    {
        $this->middleware('scope:user_socialmedias_index')->only('index');
        $this->middleware('scope:user_socialmedias_create')->only('store');
        $this->middleware('scope:user_socialmedias_show')->only('show');
        $this->middleware('scope:user_socialmedias_update')->only('update');
        $this->middleware('scope:user_socialmedias_destroy')->only('destroy');
    }
}
