<?php

namespace Modules\Crud\Http\Controllers;

use Rekamy\LaravelCore\Crudable\Abstract\CrudController;
use Modules\Crud\Bloc\CommonRefnoBloc;

class CommonRefnoController extends CrudController
{
    protected $baseBloc;
    protected $moduleName = '';
    private $result;

    public function __construct(CommonRefnoBloc $bloc) 
    {
        // $this->registerPassportScopes();
        $this->baseBloc = $bloc;
        $this->moduleName = 'common_refnos';
    }

    public function registerPassportScopes()
    {
        $this->middleware('scope:common_refnos_index')->only('index');
        $this->middleware('scope:common_refnos_create')->only('store');
        $this->middleware('scope:common_refnos_show')->only('show');
        $this->middleware('scope:common_refnos_update')->only('update');
        $this->middleware('scope:common_refnos_destroy')->only('destroy');
    }
}
