<?php

namespace Modules\Crud\Http\Controllers;

use Rekamy\LaravelCore\Crudable\Abstract\CrudController;
use Modules\Crud\Bloc\TestListBloc;

class TestListController extends CrudController
{
    protected $baseBloc;
    protected $moduleName = '';
    private $result;

    public function __construct(TestListBloc $bloc) 
    {
        // $this->registerPassportScopes();
        $this->baseBloc = $bloc;
        $this->moduleName = 'test_lists';
    }

    public function registerPassportScopes()
    {
        $this->middleware('scope:test_lists_index')->only('index');
        $this->middleware('scope:test_lists_create')->only('store');
        $this->middleware('scope:test_lists_show')->only('show');
        $this->middleware('scope:test_lists_update')->only('update');
        $this->middleware('scope:test_lists_destroy')->only('destroy');
    }
}
