<?php

namespace Modules\Crud\Http\Requests;

use Modules\Crud\Models\User;
use Modules\Crud\Bloc\UserBloc;
use Illuminate\Foundation\Http\FormRequest;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Rekamy\LaravelCore\Contracts\CrudableRequest;

class UserRequest extends FormRequest implements CrudableRequest
{
    public function model() {
        return User::class;
    }

    public function validateIndex()
    {
        /*
        if (!auth()->check()) throw new \Exception("Unauthorized Access", 401);
        $haveAccess = auth()->user()->can(UserBloc::permission('index'));
        if (!$haveAccess) throw new \Exception("Unauthorized Processing Request", 403);
        */
    }

    public function validateStore()
    {
        /*
        if (!auth()->check()) throw new \Exception("Unauthorized Access", 401);
        $haveAccess = auth()->user()->can(UserBloc::permission('create'));
        if (!$haveAccess) throw new \Exception("Unauthorized Processing Request", 403);
        */

        validator(request()->all(), User::rules('store'))->validate();
    }

    public function validateShow()
    {
        /*
        $haveAccess = auth()->user()->can(UserBloc::permission('show'));
        if (!$haveAccess) throw new \Exception("Unauthorized Processing Request", 403);
        */
    }

    public function validateUpdate()
    {
        /*
        $haveAccess = auth()->user()->can(UserBloc::permission('update'));
        if (!$haveAccess) throw new \Exception("Unauthorized Processing Request", 403);
        */
    }

    public function validateDestroy()
    {
        /*
        $haveAccess = auth()->user()->can(UserBloc::permission('destroy'));
        if (!$haveAccess) throw new \Exception("Unauthorized Processing Request", 403);
        */
    }

    public function rules()
    {
        return [];
        // return User::rules('default');
    }
}
