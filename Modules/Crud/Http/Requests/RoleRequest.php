<?php

namespace Modules\Crud\Http\Requests;

use Modules\Crud\Models\Role;
use Modules\Crud\Bloc\RoleBloc;
use Illuminate\Foundation\Http\FormRequest;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Rekamy\LaravelCore\Contracts\CrudableRequest;

class RoleRequest extends FormRequest implements CrudableRequest
{
    public function model() {
        return Role::class;
    }

    public function validateIndex()
    {
        /*
        if (!auth()->check()) throw new \Exception("Unauthorized Access", 401);
        $haveAccess = auth()->user()->can(RoleBloc::permission('index'));
        if (!$haveAccess) throw new \Exception("Unauthorized Processing Request", 403);
        */
    }

    public function validateStore()
    {
        /*
        if (!auth()->check()) throw new \Exception("Unauthorized Access", 401);
        $haveAccess = auth()->user()->can(RoleBloc::permission('create'));
        if (!$haveAccess) throw new \Exception("Unauthorized Processing Request", 403);
        */

        validator(request()->all(), Role::rules('store'))->validate();
    }

    public function validateShow()
    {
        /*
        $haveAccess = auth()->user()->can(RoleBloc::permission('show'));
        if (!$haveAccess) throw new \Exception("Unauthorized Processing Request", 403);
        */
    }

    public function validateUpdate()
    {
        /*
        $haveAccess = auth()->user()->can(RoleBloc::permission('update'));
        if (!$haveAccess) throw new \Exception("Unauthorized Processing Request", 403);
        */
    }

    public function validateDestroy()
    {
        /*
        $haveAccess = auth()->user()->can(RoleBloc::permission('destroy'));
        if (!$haveAccess) throw new \Exception("Unauthorized Processing Request", 403);
        */
    }

    public function rules()
    {
        return [];
        // return Role::rules('default');
    }
}
