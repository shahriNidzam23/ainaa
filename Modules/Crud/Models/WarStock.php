<?php
namespace Modules\Crud\Models;

use Illuminate\Database\Eloquent\{SoftDeletes};
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\{BelongsTo, HasMany};
use Illuminate\Support\Str;
use App\Models\BaseModel as Model;


class WarStock extends Model
{
	public $table = 'war_stocks';

	public $fillable = [
		'id',
		'product_id',
		'user_id',
		'quantity',
	];

	public $casts = [
		'id' => 'string',
		'product_id' => 'string',
		'user_id' => 'string',
		'quantity' => 'integer',
	];

	public static function rules($scenario = 'default') {
		$rules['default'] = [
			'product_id' => 'required',
			'user_id' => 'required',
			'quantity' => 'required',
		];
		$rules['store'] = [
			'product_id' => 'required',
			'user_id' => 'required',
			'quantity' => 'required',
		];
		$rules['update'] = [
			'product_id' => 'required',
			'user_id' => 'required',
			'quantity' => 'required',
		];
		return $rules[$scenario];
	}

	/**
	 * Get war_stocks product
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function product() : BelongsTo
	{
	    return $this->belongsTo(WarProduct::class, 'product_id');
	}

	/**
	 * Get war_stocks user
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function user() : BelongsTo
	{
	    return $this->belongsTo(User::class, 'user_id');
	}
}
