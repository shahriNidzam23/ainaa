<?php
namespace Modules\Crud\Models;

use Illuminate\Database\Eloquent\{SoftDeletes};
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\{BelongsTo, HasMany};
use Illuminate\Support\Str;
use App\Models\BaseModel as Model;


class Agent extends Model
{
	public $table = 'agents';

	public $fillable = [
		'id',
		'user_id',
		'upline_id',
	];

	public $casts = [
		'id' => 'string',
		'user_id' => 'string',
		'upline_id' => 'string',
	];

	public static function rules($scenario = 'default') {
		$rules['default'] = [
			'user_id' => 'required',
			'upline_id' => 'required',
		];
		$rules['store'] = [
			'user_id' => 'required',
			'upline_id' => 'required',
		];
		$rules['update'] = [
			'user_id' => 'required',
			'upline_id' => 'required',
		];
		return $rules[$scenario];
	}

	/**
	 * Get agents upline
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function upline() : BelongsTo
	{
	    return $this->belongsTo(User::class, 'upline_id');
	}

	/**
	 * Get agents user
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function user() : BelongsTo
	{
	    return $this->belongsTo(User::class, 'user_id');
	}
}
