<?php

namespace Modules\Crud\Models;

use Illuminate\Database\Eloquent\{SoftDeletes};
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\{BelongsTo, HasMany};
use Illuminate\Support\Str;
use App\Models\BaseModel as Model;


class OrdOrder extends Model
{
	public $table = 'ord_orders';

	public $fillable = [
		'id',
		'seller_id',
		'buyer_id',
		'payment_type',
		'status',
		'ref_no',
		'total_price',
		'notes',
	];

	public $casts = [
		'id' => 'string',
		'seller_id' => 'string',
		'buyer_id' => 'string',
		'payment_type' => 'string',
		'status' => 'string',
		'ref_no' => 'string',
		'total_price' => 'decimal:2',
		'notes' => 'string',
	];

	public static function rules($scenario = 'default') {
		$rules['default'] = [
			'seller_id' => 'required',
			'buyer_id' => 'required',
			'payment_type' => 'required',
			'status' => 'required',
			'ref_no' => 'required',
			'total_price' => 'required',
		];
		$rules['store'] = [
			'seller_id' => 'required',
			'buyer_id' => 'required',
			'payment_type' => 'required',
			'status' => 'required',
			'ref_no' => 'required',
			'total_price' => 'required',
		];
		$rules['update'] = [
			'seller_id' => 'required',
			'buyer_id' => 'required',
			'payment_type' => 'required',
			'status' => 'required',
			'ref_no' => 'required',
			'total_price' => 'required',
		];
		return $rules[$scenario];
	}

	/**
	 * Get ord_orders buyer
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function buyer() : BelongsTo
	{
	    return $this->belongsTo(User::class, 'buyer_id');
	}

	/**
	 * Get ord_orders seller
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function seller() : BelongsTo
	{
	    return $this->belongsTo(User::class, 'seller_id');
	}
}
