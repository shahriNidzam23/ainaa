<?php

namespace Modules\Crud\Models;

use Illuminate\Database\Eloquent\{SoftDeletes};
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\{BelongsTo, HasMany};
use Illuminate\Support\Str;
use App\Models\BaseModel as Model;


class OrdProduct extends Model
{
	public $table = 'ord_products';

	public $fillable = [
		'id',
		'parent_type',
		'parent_id',
		'name',
		'sku',
		'expiry_date',
		'price',
		'price_code',
		'quantity',
	];

	public $casts = [
		'id' => 'string',
		'parent_type' => 'string',
		'parent_id' => 'string',
		'name' => 'string',
		'sku' => 'string',
		'expiry_date' => 'date:Y-m-d H:i',
		'price' => 'decimal:2',
		'price_code' => 'string',
		'quantity' => 'integer',
	];

	public static function rules($scenario = 'default') {
		$rules['default'] = [
			'parent_type' => 'required',
			'parent_id' => 'required',
			'name' => 'required',
			'sku' => 'required',
			'price' => 'required',
			'price_code' => 'required',
			'quantity' => 'required',
		];
		$rules['store'] = [
			'parent_type' => 'required',
			'parent_id' => 'required',
			'name' => 'required',
			'sku' => 'required',
			'price' => 'required',
			'price_code' => 'required',
			'quantity' => 'required',
		];
		$rules['update'] = [
			'parent_type' => 'required',
			'parent_id' => 'required',
			'name' => 'required',
			'sku' => 'required',
			'price' => 'required',
			'price_code' => 'required',
			'quantity' => 'required',
		];
		return $rules[$scenario];
	}
}
