<?php
namespace Modules\Crud\Models;

use Illuminate\Database\Eloquent\{SoftDeletes};
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\{BelongsTo, HasMany};
use Illuminate\Support\Str;
use App\Models\BaseModel as Model;


class WarAssetInbound extends Model
{
	public $table = 'war_asset_inbound';

	public $fillable = [
		'id',
		'product_id',
		'manufacturer',
		'batch_id',
		'receive_quantity',
		'rejected_quantity',
		'accepted_quantity',
	];

	public $casts = [
		'id' => 'string',
		'product_id' => 'string',
		'manufacturer' => 'string',
		'batch_id' => 'integer',
		'receive_quantity' => 'integer',
		'rejected_quantity' => 'integer',
		'accepted_quantity' => 'integer',
	];

	public static function rules($scenario = 'default') {
		$rules['default'] = [
			'product_id' => 'required',
			'manufacturer' => 'required',
			'batch_id' => 'required',
			'receive_quantity' => 'required',
			'rejected_quantity' => 'required',
			'accepted_quantity' => 'required',
		];
		$rules['store'] = [
			'product_id' => 'required',
			'manufacturer' => 'required',
			'batch_id' => 'required',
			'receive_quantity' => 'required',
			'rejected_quantity' => 'required',
			'accepted_quantity' => 'required',
		];
		$rules['update'] = [
			'product_id' => 'required',
			'manufacturer' => 'required',
			'batch_id' => 'required',
			'receive_quantity' => 'required',
			'rejected_quantity' => 'required',
			'accepted_quantity' => 'required',
		];
		return $rules[$scenario];
	}

	/**
	 * Get war_asset_inbound product
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function product() : BelongsTo
	{
	    return $this->belongsTo(WarProduct::class, 'product_id');
	}
}
