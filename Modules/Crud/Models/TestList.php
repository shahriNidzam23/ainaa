<?php
namespace Modules\Crud\Models;

use Illuminate\Database\Eloquent\{SoftDeletes};
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\{BelongsTo, HasMany};
use Illuminate\Support\Str;
use App\Models\BaseModel as Model;


class TestList extends Model
{
	public $table = 'test_lists';

	public $fillable = [
		'id',
		'name',
	];

	public $casts = [
		'id' => 'string',
		'name' => 'string',
	];

	public static function rules($scenario = 'default') {
		$rules['default'] = [
		];
		$rules['store'] = [
		];
		$rules['update'] = [
		];
		return $rules[$scenario];
	}
}
