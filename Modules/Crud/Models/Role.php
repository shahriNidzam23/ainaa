<?php
namespace Modules\Crud\Models;

use Illuminate\Database\Eloquent\{SoftDeletes};
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\{BelongsTo, HasMany};
use Illuminate\Support\Str;
use App\Models\BaseModel as Model;


class Role extends Model
{
	public $table = 'roles';

	public $fillable = [
		'id',
		'name',
		'description',
		'guard_name',
	];

	public $casts = [
		'id' => 'string',
		'name' => 'string',
		'description' => 'string',
		'guard_name' => 'string',
	];

	public static function rules($scenario = 'default') {
		$rules['default'] = [
			'name' => 'required',
			'description' => 'required',
			'guard_name' => 'required',
		];
		$rules['store'] = [
			'name' => 'required',
			'description' => 'required',
			'guard_name' => 'required',
		];
		$rules['update'] = [
			'name' => 'required',
			'description' => 'required',
			'guard_name' => 'required',
		];
		return $rules[$scenario];
	}

	/**
	 * Get roles roleHasPermissions
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function roleHasPermissions() : HasMany
	{
	    return $this->hasMany(RoleHasPermission::class, 'role_id');
	}
}
