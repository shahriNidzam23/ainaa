<?php
namespace Modules\Crud\Models;

use Illuminate\Database\Eloquent\{SoftDeletes};
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\{BelongsTo, HasMany};
use Illuminate\Support\Str;
use App\Models\BaseModel as Model;


class UserProfile extends Model
{
	public $table = 'user_profiles';

	public $fillable = [
		'id',
		'user_id',
		'fullname',
		'phone_number',
		'photo_url',
	];

	public $casts = [
		'id' => 'string',
		'user_id' => 'string',
		'fullname' => 'string',
		'phone_number' => 'string',
		'photo_url' => 'string',
	];

	public static function rules($scenario = 'default') {
		$rules['default'] = [
			'user_id' => 'required',
			'fullname' => 'required',
		];
		$rules['store'] = [
			'user_id' => 'required',
			'fullname' => 'required',
		];
		$rules['update'] = [
			'user_id' => 'required',
			'fullname' => 'required',
		];
		return $rules[$scenario];
	}

	/**
	 * Get user_profiles user
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function user() : BelongsTo
	{
	    return $this->belongsTo(User::class, 'user_id');
	}
}
