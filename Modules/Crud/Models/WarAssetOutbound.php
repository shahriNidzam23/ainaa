<?php
namespace Modules\Crud\Models;

use Illuminate\Database\Eloquent\{SoftDeletes};
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\{BelongsTo, HasMany};
use Illuminate\Support\Str;
use App\Models\BaseModel as Model;


class WarAssetOutbound extends Model
{
	public $table = 'war_asset_outbound';

	public $fillable = [
		'id',
		'product_id',
		'manufacturer',
		'batch_id',
		'stockist_id',
		'quantity',
	];

	public $casts = [
		'id' => 'string',
		'product_id' => 'string',
		'manufacturer' => 'string',
		'batch_id' => 'integer',
		'stockist_id' => 'integer',
		'quantity' => 'integer',
	];

	public static function rules($scenario = 'default') {
		$rules['default'] = [
			'product_id' => 'required',
			'manufacturer' => 'required',
			'batch_id' => 'required',
			'stockist_id' => 'required',
			'quantity' => 'required',
		];
		$rules['store'] = [
			'product_id' => 'required',
			'manufacturer' => 'required',
			'batch_id' => 'required',
			'stockist_id' => 'required',
			'quantity' => 'required',
		];
		$rules['update'] = [
			'product_id' => 'required',
			'manufacturer' => 'required',
			'batch_id' => 'required',
			'stockist_id' => 'required',
			'quantity' => 'required',
		];
		return $rules[$scenario];
	}

	/**
	 * Get war_asset_outbound product
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function product() : BelongsTo
	{
	    return $this->belongsTo(WarProduct::class, 'product_id');
	}
}
