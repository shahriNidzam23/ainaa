<?php
namespace Modules\Crud\Models;

use Illuminate\Database\Eloquent\{SoftDeletes};
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\{BelongsTo, HasMany};
use Illuminate\Support\Str;
use App\Models\BaseModel as Model;


class WarProduct extends Model
{
	public $table = 'war_products';

	public $fillable = [
		'id',
		'name',
		'sku',
		'quantity',
	];

	public $casts = [
		'id' => 'string',
		'name' => 'string',
		'sku' => 'string',
		'quantity' => 'integer',
	];

	public static function rules($scenario = 'default') {
		$rules['default'] = [
			'name' => 'required',
			'sku' => 'required',
			'quantity' => 'required',
		];
		$rules['store'] = [
			'name' => 'required',
			'sku' => 'required',
			'quantity' => 'required',
		];
		$rules['update'] = [
			'name' => 'required',
			'sku' => 'required',
			'quantity' => 'required',
		];
		return $rules[$scenario];
	}

	/**
	 * Get war_products warAssetInbounds
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function warAssetInbounds() : HasMany
	{
	    return $this->hasMany(WarAssetInbound::class, 'product_id');
	}

	/**
	 * Get war_products warAssetOutbounds
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function warAssetOutbounds() : HasMany
	{
	    return $this->hasMany(WarAssetOutbound::class, 'product_id');
	}

	/**
	 * Get war_products warPrices
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function warPrices() : HasMany
	{
	    return $this->hasMany(WarPrice::class, 'product_id');
	}

	/**
	 * Get war_products warStocks
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function warStocks() : HasMany
	{
	    return $this->hasMany(WarStock::class, 'product_id');
	}
}
