<?php
namespace Modules\Crud\Models;

use Illuminate\Database\Eloquent\{SoftDeletes};
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\{BelongsTo, HasMany};
use Illuminate\Support\Str;
use App\Models\BaseModel as Model;


class User extends Model
{
	public $table = 'users';

	public $fillable = [
		'id',
		'email',
		'display_name',
		'email_verified_at',
		'ott',
	];

	public $casts = [
		'id' => 'string',
		'email' => 'string',
		'display_name' => 'string',
		'email_verified_at' => 'date:Y-m-d H:i',
		'ott' => 'string',
	];

	public static function rules($scenario = 'default') {
		$rules['default'] = [
			'email' => 'required',
			'display_name' => 'required',
		];
		$rules['store'] = [
			'email' => 'required',
			'display_name' => 'required',
		];
		$rules['update'] = [
			'email' => 'required',
			'display_name' => 'required',
		];
		return $rules[$scenario];
	}

	/**
	 * Get users agents
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function upline() : HasMany
	{
	    return $this->hasOne(Agent::class, 'upline_id');
	}

	/**
	 * Get users agents
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function user() : HasMany
	{
	    return $this->hasOne(Agent::class, 'user_id');
	}

	/**
	 * Get users userProfiles
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function userProfiles() : HasMany
	{
	    return $this->hasMany(UserProfile::class, 'user_id');
	}

	/**
	 * Get users userSocialmedias
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function userSocialmedias() : HasMany
	{
	    return $this->hasMany(UserSocialmedia::class, 'user_id');
	}

	/**
	 * Get users warStocks
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function warStocks() : HasMany
	{
	    return $this->hasMany(WarStock::class, 'user_id');
	}
}
