<?php
namespace Modules\Crud\Models;

use Illuminate\Database\Eloquent\{SoftDeletes};
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\{BelongsTo, HasMany};
use Illuminate\Support\Str;
use App\Models\BaseModel as Model;


class SocmedPost extends Model
{
	public $table = 'socmed_posts';

	public $fillable = [
		'id',
		'url_posting',
		'social_media',
		'username',
	];

	public $casts = [
		'id' => 'string',
		'url_posting' => 'string',
		'social_media' => 'string',
		'username' => 'string',
	];

	public static function rules($scenario = 'default') {
		$rules['default'] = [
			'url_posting' => 'required',
			'social_media' => 'required',
			'username' => 'required',
		];
		$rules['store'] = [
			'url_posting' => 'required',
			'social_media' => 'required',
			'username' => 'required',
		];
		$rules['update'] = [
			'url_posting' => 'required',
			'social_media' => 'required',
			'username' => 'required',
		];
		return $rules[$scenario];
	}
}
