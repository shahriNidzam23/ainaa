<?php

namespace Modules\Crud\Models;

use Illuminate\Database\Eloquent\{SoftDeletes};
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\{BelongsTo, HasMany};
use Illuminate\Support\Str;
use App\Models\BaseModel as Model;


class CommonNotification extends Model
{
	public $table = 'common_notifications';

	public $fillable = [
		'id',
		'user_id',
		'token',
	];

	public $casts = [
		'id' => 'string',
		'user_id' => 'string',
		'token' => 'string',
	];

	public static function rules($scenario = 'default') {
		$rules['default'] = [
			'user_id' => 'required',
			'token' => 'required',
		];
		$rules['store'] = [
			'user_id' => 'required',
			'token' => 'required',
		];
		$rules['update'] = [
			'user_id' => 'required',
			'token' => 'required',
		];
		return $rules[$scenario];
	}
}
