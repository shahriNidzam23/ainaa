<?php
namespace Modules\Crud\Models;

use Illuminate\Database\Eloquent\{SoftDeletes};
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\{BelongsTo, HasMany};
use Illuminate\Support\Str;
use App\Models\BaseModel as Model;


class CommonAttachment extends Model
{
	public $table = 'common_attachments';

	public $fillable = [
		'id',
		'parent_type',
		'parent_id',
		'name',
		'path',
	];

	public $casts = [
		'id' => 'string',
		'parent_type' => 'string',
		'parent_id' => 'string',
		'name' => 'string',
		'path' => 'string',
	];

	public static function rules($scenario = 'default') {
		$rules['default'] = [
			'parent_type' => 'required',
			'parent_id' => 'required',
			'name' => 'required',
			'path' => 'required',
		];
		$rules['store'] = [
			'parent_type' => 'required',
			'parent_id' => 'required',
			'name' => 'required',
			'path' => 'required',
		];
		$rules['update'] = [
			'parent_type' => 'required',
			'parent_id' => 'required',
			'name' => 'required',
			'path' => 'required',
		];
		return $rules[$scenario];
	}
}
