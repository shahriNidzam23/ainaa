<?php
namespace Modules\Crud\Models;

use Illuminate\Database\Eloquent\{SoftDeletes};
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\{BelongsTo, HasMany};
use Illuminate\Support\Str;
use App\Models\BaseModel as Model;


class Audit extends Model
{
	public $table = 'audits';

	public $fillable = [
		'id',
		'user_type',
		'user_id',
		'event',
		'auditable_type',
		'auditable_id',
		'old_values',
		'new_values',
		'url',
		'ip_address',
		'user_agent',
		'tags',
	];

	public $casts = [
		'user_type' => 'string',
		'user_id' => 'string',
		'event' => 'string',
		'auditable_type' => 'string',
		'auditable_id' => 'string',
		'old_values' => 'string',
		'new_values' => 'string',
		'url' => 'string',
		'ip_address' => 'string',
		'user_agent' => 'string',
		'tags' => 'string',
	];

	public static function rules($scenario = 'default') {
		$rules['default'] = [
			'event' => 'required',
			'auditable_type' => 'required',
			'auditable_id' => 'required',
		];
		$rules['store'] = [
			'event' => 'required',
			'auditable_type' => 'required',
			'auditable_id' => 'required',
		];
		$rules['update'] = [
			'event' => 'required',
			'auditable_type' => 'required',
			'auditable_id' => 'required',
		];
		return $rules[$scenario];
	}
}
