<?php

namespace Modules\Crud\Models;

use Illuminate\Database\Eloquent\{SoftDeletes};
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\{BelongsTo, HasMany};
use Illuminate\Support\Str;
use App\Models\BaseModel as Model;


class OrdSale extends Model
{
	public $table = 'ord_sales';

	public $fillable = [
		'id',
		'user_id',
		'ref_no',
		'total_price',
		'status',
		'notes',
	];

	public $casts = [
		'id' => 'string',
		'user_id' => 'string',
		'ref_no' => 'string',
		'total_price' => 'decimal:2',
		'status' => 'string',
		'notes' => 'string',
	];

	public static function rules($scenario = 'default') {
		$rules['default'] = [
			'user_id' => 'required',
			'ref_no' => 'required',
			'total_price' => 'required',
			'status' => 'required',
		];
		$rules['store'] = [
			'user_id' => 'required',
			'ref_no' => 'required',
			'total_price' => 'required',
			'status' => 'required',
		];
		$rules['update'] = [
			'user_id' => 'required',
			'ref_no' => 'required',
			'total_price' => 'required',
			'status' => 'required',
		];
		return $rules[$scenario];
	}

	/**
	 * Get ord_sales ordCustomers
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function ordCustomers() : HasMany
	{
	    return $this->hasMany(OrdCustomer::class, 'sale_id');
	}

	/**
	 * Get ord_sales user
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function user() : BelongsTo
	{
	    return $this->belongsTo(User::class, 'user_id');
	}
}
