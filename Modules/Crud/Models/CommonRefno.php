<?php
namespace Modules\Crud\Models;

use Illuminate\Database\Eloquent\{SoftDeletes};
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\{BelongsTo, HasMany};
use Illuminate\Support\Str;
use App\Models\BaseModel as Model;


class CommonRefno extends Model
{
	public $table = 'common_refnos';

	public $fillable = [
		'id',
		'name',
		'prefix',
		'running_no',
	];

	public $casts = [
		'id' => 'string',
		'name' => 'string',
		'prefix' => 'string',
		'running_no' => 'integer',
	];

	public static function rules($scenario = 'default') {
		$rules['default'] = [
			'name' => 'required',
			'prefix' => 'required',
			'running_no' => 'required',
		];
		$rules['store'] = [
			'name' => 'required',
			'prefix' => 'required',
			'running_no' => 'required',
		];
		$rules['update'] = [
			'name' => 'required',
			'prefix' => 'required',
			'running_no' => 'required',
		];
		return $rules[$scenario];
	}
}
