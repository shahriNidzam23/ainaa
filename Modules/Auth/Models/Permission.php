<?php
namespace Modules\Auth\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\{BelongsTo, HasMany};
use Spatie\Permission\Models\Permission as BasePermissions;
use Rekamy\LaravelCore\Traits\GroupedRecord;
use Kirschbaum\PowerJoins\PowerJoins;
use Illuminate\Database\Eloquent\Concerns\HasUuids;

class Permission extends BasePermissions
{
    use GroupedRecord, HasFactory, HasUuids, PowerJoins;
	// use DataPolicy;
	public $table = 'permissions';

	public $fillable = [
		'id',
		'name',
		'module',
		'description',
		'guard_name',
	];

	public $casts = [
		'id' => 'string',
		'name' => 'string',
		'module' => 'string',
		'description' => 'string',
		'guard_name' => 'string',
	];

	public static function rules($scenario = 'default') {
		$rules['default'] = [
			'name' => 'required',
			'description' => 'required',
			'guard_name' => 'required',
		];
		$rules['store'] = [
			'name' => 'required',
			'description' => 'required',
			'guard_name' => 'required',
		];
		$rules['update'] = [
			'name' => 'required',
			'description' => 'required',
			'guard_name' => 'required',
		];
		return $rules[$scenario];
	}

	/**
	 * Get permissions roleHasPermissions
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function roleHasPermissions() : HasMany
	{
	    return $this->hasMany(RoleHasPermission::class, 'permission_id');
	}

    public function scopeByRole($query) {
        if(empty(request()->get("byRole::rolename"))) return abort(400, "Module Not Provided");


        $role = Role::where("name", request()->get("byRole::rolename"))->firstOrFail();
        return $query->whereHas('roleHasPermissions')->whereHas('roleHasPermissions', function($query) use ($role){
            $query->where("role_id", $role->id);
        });
    }
}
