<?php
namespace Modules\Auth\Models;

use Illuminate\Database\Eloquent\{Model};
use Illuminate\Database\Eloquent\Relations\{BelongsTo, HasMany};

class RoleHasPermission extends Model
{
	public $table = 'role_has_permissions';

    public $timestamps = false;

	public $fillable = [
		'permission_id',
		'role_id',
	];
}
