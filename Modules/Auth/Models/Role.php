<?php

namespace Modules\Auth\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\{BelongsTo, HasMany};
use Rekamy\LaravelCore\Traits\GroupedRecord;
use Kirschbaum\PowerJoins\PowerJoins;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Spatie\Permission\Models\Role as BaseRoles;

class Role extends BaseRoles
{
    use GroupedRecord, HasFactory, HasUuids, PowerJoins;
    public $table = 'roles';

    public $fillable = [
        'id',
        'name',
        'description',
        'guard_name',
    ];

    public $casts = [
        'id' => 'string',
        'name' => 'string',
        'description' => 'string',
        'guard_name' => 'string',
    ];

    protected static function boot()
    {

        static::deleting(function ($model) {
            $roles = array_merge(["hq", "superadmin", "admin"], Role::getMobileRoles());
            abort_if(in_array($model->name, $roles), 400, "$model->name cannot be deleted");

        });
        // Boot parent last to ensure traits run after BaseModel boot
        parent::boot();

    }

    public static function rules($scenario = 'default')
    {
        $rules['default'] = [
            'name' => 'required',
            'description' => 'required',
            'guard_name' => 'required',
        ];
        $rules['store'] = [
            'name' => 'required',
            'description' => 'required',
            'guard_name' => 'required',
        ];
        $rules['update'] = [
            'name' => 'required',
            'description' => 'required',
            'guard_name' => 'required',
        ];
        return $rules[$scenario];
    }

    /**
     * Get roles roleHasPermissions
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function roleHasPermissions(): HasMany
    {
        return $this->hasMany(RoleHasPermission::class, 'role_id');
    }

    public static function getMobileRoles()
    {
        return [
            'master_stockist',
            'stockist_gold', 'stockist_silver', 'stockist_bronze',
            'agent_gold', 'agent_silver', 'agent_bronze'
        ];
    }

    public function scopeList($query)
    {

        return $query->select('roles.*')
            ->selectSub(function ($query) {
                $query
                    ->join('role_has_permissions', 'permissions.id', '=', 'role_has_permissions.permission_id')
                    ->select(\DB::raw("COUNT(DISTINCT permissions.module)"))
                    ->from('permissions')
                    ->whereColumn('role_has_permissions.role_id', 'roles.id');
            }, 'permissions')
            ->selectSub(function ($query) {
                $query
                    ->select(\DB::raw("COUNT(model_id)"))
                    ->from('model_has_roles')
                    ->whereColumn('model_has_roles.role_id', 'roles.id');
            }, 'users');
    }

    public function scopeManagement($query) {
        $roles = array_merge(["hq", "superadmin"], Role::getMobileRoles());
        $query->whereNotIn("name", $roles);
    }

    public function scopeAgent($query) {
        $query->whereIn("name", Role::getMobileRoles());
    }
}
