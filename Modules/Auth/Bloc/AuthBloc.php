<?php

namespace Modules\Auth\Bloc;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Modules\User\Bloc\UserBloc;
use Modules\User\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Events\{
    Login,
    Logout,
};
use Modules\Agent\Models\Agent;
use Modules\Auth\Events\{
    Registered,
};

use Modules\Auth\Http\Requests\{
    VerifyEmailRequest,
    LoginRequest,
    ForgotPasswordRequest,
    ResetPasswordRequest,
    RegisterRequest,
    VerifyTokenRequest
};


use Modules\Auth\Events\RequestToken;
use Modules\Auth\Models\Role;

class AuthBloc
{
    public function __construct(
        public UserBloc $userBloc,
    ) {
    }

    public function get(Request $request)
    {
        $user = auth()->user();

        return $user;
    }

    public function requestToken($request)
    {
        $user = Agent::bypassDataPolicy(function() use ($request) {

            return User::with("agent")->where([
                "email" => $request->email,
            ])->first();
        });

        if(!empty($user->agent)){
            if($user->agent->status == Agent::STATUS_INACTIVE){
                abort(403, 'Your account has been locked please contact HQ.');
            }
        }

        if (empty($user)) {
            abort(404, 'Email not found');
        }
        $roles = Role::getMobileRoles();

        if(empty($request->platform)) return abort(403, "You do not have permission to access");
        if(!($request->platform == 'dashboard' || $request->platform == "mobile")) return abort(403, "You do not have permission to access");

        if($request->platform == "dashboard" && $user->hasRole($roles)) {
            return abort(403, "You do not have permission to access dashboard");
        }
        if($request->platform == "mobile" && !$user->hasRole($roles)) {
            return abort(403, "You do not have permission to access");
        }

        $ott = $user->email == 'demo@xyz.com' ? 888888 : rand(100000, 999999);
        $user->password = bcrypt($ott);
        $user->ott = $ott;
        $user->save();

        event(new RequestToken($user));
    }

    public function login(LoginRequest $request)
    {
        $request->authenticate();

        if (!str($request->route()->uri)->contains('api/')) {
            $request->session()->regenerate();
        }

        $user = $request->user();

        event(new Login('auth', $user, false));

        return $user;
    }

    public function register(RegisterRequest $request)
    {
        DB::beginTransaction();
        try {
            $user = $this->userBloc->create($request->all());
            event(new Registered($user, $request->all()));
            DB::commit();
            return $user;
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }

    public function registerBulk($request)
    {
        DB::beginTransaction();
        try {
            if (!$request->hasFile('attachment')) abort(400, "CSV file not present");
            $attachment = request()->file('attachment');
            collect($this->extractCsv($attachment))->each(function ($row) {
                $request = [
                    "upline" => $row["upline"],
                    "prefix" => $row["prefix"],
                ];
                $user = $this->userBloc->create($row);

                event(new Registered($user, $request));
            });

            DB::commit();

            // return $user;
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }

    private function extractCsv($attachment)
    {
        try {
            if (($handle = fopen($attachment->path(), "r")) !== FALSE) {
                $headers = [];
                $rows = [];
                $index = 0;
                while (($row = fgetcsv($handle, 1000, ",")) !== FALSE) {
                    if ($index < 1) $headers = $row;
                    if ($index > 0) {
                        $rows[] = [];
                        foreach ($headers as $key => $header) {
                            $rows[count($rows) - 1][$header] = $row[$key];
                        }
                    }
                    $index++;
                }

                fclose($handle);

                return $rows;
            }
        } catch (\Throwable $th) {
        }

        abort(500, "Failed to read CSV file");
    }

    public function logout(Request $request)
    {
        if (str($request->route()->uri)->contains('api/')) {
            auth()->user()->notification()->delete();
            auth()->user()->currentAccessToken()->delete();
        }

        if (auth('web')->check()) {
            auth('web')->logout();

            $request->session()->invalidate();
            $request->session()->regenerateToken();
        }

        event(new Logout('auth', $request->user()));
    }
}
