<?php

namespace Modules\Auth\Bloc;

use Rekamy\LaravelCore\Crudable\Abstract\CrudBloc;
use Modules\Auth\Repositories\RoleRepository;
use Modules\Auth\Http\Requests\RoleRequest;

class RoleBloc extends CrudBloc
{
    public function __construct(
        public RoleRepository $repo,
        public RoleRequest $request,
    ) {
        $this->repo = $repo;
        $this->request = $request;
    }

    public static function permission($name) {
    //     $permission = [
    //         'index' => 'roles_index',
    //         'create' => 'roles_create',
    //         'show' => 'roles_show',
    //         'update' => 'roles_update',
    //         'destroy' => 'roles_destroy',
    //     ];

    //     return $permission[$name];
    }
}
