<?php

namespace Modules\Auth\Bloc;

use Rekamy\LaravelCore\Crudable\Abstract\CrudBloc;
use Modules\Auth\Repositories\PermissionRepository;
use Modules\Auth\Http\Requests\PermissionRequest;

class PermissionBloc extends CrudBloc
{
    public function __construct(
        public PermissionRepository $repo,
        public PermissionRequest $request,
    ) {
        $this->repo = $repo;
        $this->request = $request;
    }

    public static function permission($name) {
    //     $permission = [
    //         'index' => 'permissions_index',
    //         'create' => 'permissions_create',
    //         'show' => 'permissions_show',
    //         'update' => 'permissions_update',
    //         'destroy' => 'permissions_destroy',
    //     ];

    //     return $permission[$name];
    }
}
