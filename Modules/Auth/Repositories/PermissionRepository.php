<?php

namespace Modules\Auth\Repositories;

use Modules\Auth\Models\Permission;
use Rekamy\LaravelCore\Crudable\Concern\CrudableRepository;
use Rekamy\LaravelCore\Contracts\CrudableRepository as CrudableRepositoryContract;
use Rekamy\LaravelCore\Override\Repository;

/**
 * Class PermissionRepository
 * @package App\Repositories
 *
 * @method Permission find($id, $columns = ['*'])
 * @method Permission find($id, $columns = ['*'])
 * @method Permission first($columns = ['*'])
*/
class PermissionRepository extends Repository implements CrudableRepositoryContract
{
    use CrudableRepository;

    /**
     * @var array
     */
    protected $fieldSearchable = [
		'name',
		'module',
		'description',
		'guard_name',
        'roleHasPermissions.role_id'
	];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Permission::class;
    }

}

