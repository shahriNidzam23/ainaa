<?php

namespace Modules\Auth\Repositories;

use Rekamy\LaravelCore\Crudable\Concern\CrudableRepository;
use Rekamy\LaravelCore\Contracts\CrudableRepository as CrudableRepositoryContract;
use Modules\Auth\Models\Role;
use Rekamy\LaravelCore\Override\Repository;

/**
 * Class RoleRepository
 * @package App\Repositories
 *
 * @method Role find($id, $columns = ['*'])
 * @method Role find($id, $columns = ['*'])
 * @method Role first($columns = ['*'])
*/
class RoleRepository extends Repository implements CrudableRepositoryContract
{
    use CrudableRepository;

    /**
     * @var array
     */
    protected $fieldSearchable = [
		'name',
		'description',
		'guard_name',
	];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Role::class;
    }

}

