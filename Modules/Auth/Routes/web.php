<?php

namespace Modules\Auth\Http\Controllers;

use Illuminate\Support\Facades\Route;


Route::get('verify-email/{id}/{hash}', [AuthController::class, 'verifyEmail'])
    ->middleware(['signed', 'throttle:6,1'])
    ->name('verification.verify');

Route::get('/login', function () {
    return view('welcome');
})->name('login');
