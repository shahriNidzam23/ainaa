<?php

namespace Modules\Auth\Http\Controllers;

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::as('auth.')->group(function () {
    Route::post('token', [AuthController::class, 'requestToken'])->name('verify-code');
    Route::post('login', [AuthController::class, 'login'])->name('login');
    Route::post('register', [AuthController::class, 'register'])->name('register');
});

Route::group(['as' => 'auth.', 'middleware' => ['auth:sanctum']], function () {
    Route::post('register/bulk', [AuthController::class, 'registerBulk'])->name('register.bulk');
    Route::get('/', [AuthController::class, 'get'])->name('current');
    Route::post('logout', [AuthController::class, 'logout'])->name('logout');

    Route::apiResources([
        'permission' => PermissionController::class,
        'role' => RoleController::class
    ]);
});
