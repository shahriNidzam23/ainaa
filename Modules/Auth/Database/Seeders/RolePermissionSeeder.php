<?php

namespace Modules\Auth\Database\Seeders;

use Modules\Auth\Models\Permission;
use Modules\Auth\Models\Role;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolePermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('model_has_roles')->delete();
        DB::table('model_has_permissions')->delete();
        DB::table('role_has_permissions')->delete();
        DB::table('roles')->delete();
        DB::table('permissions')->delete();
        $roles = $this->roles();
        $this->registerRole($roles);


        $access = [
            'superadmin' => $this->dashboard(),
            'hq' => $this->dashboard(),
            'admin' => $this->dashboard(),
            'store_keeper' => $this->dashboard(),
        ];


        $roleAccess = [
            'superadmin' => $this->role(),
            'hq' => $this->role(),
        ];


        $userAccess = [
            'superadmin' => $this->user(),
            'hq' => $this->user(),
        ];


        $auditAccess = [
            'superadmin' => $this->audit(),
            'hq' => $this->audit(),
        ];

        RolePermissionSeeder::registerRP('Dashboard', $access);
        RolePermissionSeeder::registerRP('Role', $roleAccess);
        RolePermissionSeeder::registerRP('User', $userAccess);
        RolePermissionSeeder::registerRP('Audit', $auditAccess);

    }

    private function audit() {
        return [
            "audits:view",
        ];
    }

    private function user() {
        return [
            "users:view",
        ];
    }

    private function role() {
        return [
            "roles:view",
        ];
    }

    private function dashboard() {
        return [
            "dashboard:view",
        ];
    }

    public function roles()
    {
        return [
            'superadmin' => 'Superadmin',
            'hq' => 'HQ',
            'admin' => 'Admin',

            'hr' => 'HR',
            'finance' => 'Finance',
            'hr' => 'HR',
            'store_keeper' => 'Store Keeper',

            'master_stockist' => 'Master Stockist',
            'stockist_gold' => 'Stockist (Gold)',
            'stockist_silver' => 'Stockist (Silver)',
            'stockist_bronze' => 'Stockist (Bronze)',
            'agent_gold' => 'Agent (Gold)',
            'agent_silver' => 'Agent (Silver)',
            'agent_bronze' => 'Agent (Bronze)',
        ];
    }

    public function registerRole($access)
    {
        foreach ($access as $key => $value) {

            Role::firstOrCreate(
                ['name' => $key],
                ['name' => $key, 'description' => $value],
            );
        }

    }

    public static function registerRP($module, $access)
    {
        foreach ($access as $role => $permissions) {

            $roleModel = Role::firstOrCreate(
                ['name' => $role],
                ['name' => $role, 'description' => str($role)->headline()],
            );
            collect($permissions)->each(function ($permission) use ($roleModel, $module) {
                $breakdown = explode(':', $permission);
                $by = "";
                if(count($breakdown) > 2) {
                    $column = str(explode("=", $breakdown[2])[0])->headline();
                    $by = " By " . $column;
                }
                [$name, $permissionName] = explode(':', $permission);
                $permissionModel = Permission::firstOrCreate(
                    [
                        'name' => $permission,
                        'module' => str($module)->headline(),
                        'description' => str($permissionName.' '.$name . $by)->headline(),
                    ]
                );
                $roleModel->givePermissionTo($permissionModel);
            });
        }
    }
}
