<?php

namespace Modules\Auth\Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class AuthReferenceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $references = [
            [
                'id' => \Str::orderedUuid(),
                'name' => 'Agent Role',
                'value' => 'HQ',
                'code' => 'hq',
                'order' => 1,
            ],
            [
                'id' => \Str::orderedUuid(),
                'name' => 'Agent Role',
                'value' => 'Master Stockist',
                'code' => 'master_stockist',
                'order' => 2,
            ],
            [
                'id' => \Str::orderedUuid(),
                'name' => 'Agent Role',
                'value' => 'Stockist (Gold)',
                'code' => 'stockist_gold',
                'order' => 3,
            ],
            [
                'id' => \Str::orderedUuid(),
                'name' => 'Agent Role',
                'value' => 'Stockist (Silver)',
                'code' => 'stockist_silver',
                'order' => 4,
            ],
            [
                'id' => \Str::orderedUuid(),
                'name' => 'Agent Role',
                'value' => 'Stockist (Bronze)',
                'code' => 'stockist_bronze',
                'order' => 5,
            ],
            [
                'id' => \Str::orderedUuid(),
                'name' => 'Agent Role',
                'value' => 'Agent (Gold)',
                'code' => 'agent_gold',
                'order' => 6,
            ],
            [
                'id' => \Str::orderedUuid(),
                'name' => 'Agent Role',
                'value' => 'Agent (Silver)',
                'code' => 'agent_silver',
                'order' => 7,
            ],
            [
                'id' => \Str::orderedUuid(),
                'name' => 'Agent Role',
                'value' => 'Agent (Bronze)',
                'code' => 'agent_bronze',
                'order' => 8,
            ],
            [
                'id' => \Str::orderedUuid(),
                'name' => 'Office Role',
                'value' => 'HQ',
                'code' => 'hq',
                'order' => 1,
            ],
            [
                'id' => \Str::orderedUuid(),
                'name' => 'Office Role',
                'value' => 'Finance',
                'code' => 'finance',
                'order' => 2,
            ],
            [
                'id' => \Str::orderedUuid(),
                'name' => 'Office Role',
                'value' => 'HR',
                'code' => 'hr',
                'order' => 3,
            ],
            [
                'id' => \Str::orderedUuid(),
                'name' => 'Office Role',
                'value' => 'Store Keeper',
                'code' => 'store_keeper',
                'order' => 4,
            ],
            [
                'id' => \Str::orderedUuid(),
                'name' => 'Office Role',
                'value' => 'Admin',
                'code' => 'admin',
                'order' => 4,
            ],
        ];

        DB::table('common_references')->insert($references);
    }
}
