<?php

namespace Modules\Auth\Notifications;

use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\URL;

class WelcomeNotification extends Notification
{

    /**
     * Get the notification's channels.
     *
     * @param  mixed  $notifiable
     * @return array|string
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Build the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {

        return (new MailMessage)
            ->subject(Lang::get('Welcome to XYZ!'))
            ->greeting("Hello $notifiable->display_name,")
            ->line(Lang::get('Welcome to XYZ'))
            ->line(Lang::get('We are thrilled to have you join our community and embark on this journey with us.'))
            ->line(Lang::get('Please download XYZ Mobile App and login using your email.'));
    }
}
