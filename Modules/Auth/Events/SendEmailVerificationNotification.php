<?php

namespace Modules\Auth\Events;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Modules\Auth\Events\{
    RequestToken
};

class SendEmailVerificationNotification
{

    public function handle(RequestToken $event)
    {
        if($event->user->email != 'demo@xyz.com') {
            $event->user->sendEmailVerificationNotification();
        }
    }
}
