<?php

namespace Modules\Auth\Http\Requests;

use Modules\User\Models\User;
use Illuminate\Auth\Events\Verified;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Validator;

class VerifyEmailRequest extends FormRequest
{
    public function getCurrentUser()
    {
        return User::withoutGlobalScope('verified')->find($this->route('id'));
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (! $this->getCurrentUser()) {
            return false;
        }

        if (! hash_equals(sha1($this->getCurrentUser()->getEmailForVerification()), (string) $this->route('hash'))) {
            return false;
        }

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    /**
     * Fulfill the email verification request.
     *
     * @return void
     */
    public function fulfill()
    {
        if (! $this->getCurrentUser()->hasVerifiedEmail()) {
            $this->getCurrentUser()->markEmailAsVerified();

            event(new Verified($this->getCurrentUser()));
        }
    }

    /**
     * Configure the validator instance.
     *
     * @return void
     */
    public function withValidator(Validator $validator)
    {
        return $validator;
    }
}
