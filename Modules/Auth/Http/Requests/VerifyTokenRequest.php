<?php

namespace Modules\Auth\Http\Requests;

use Modules\User\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules;

class VerifyTokenRequest extends FormRequest
{

    public function __construct()
    {
        $validator = validator(request()->all(), $this->rules());
        $this->setValidator($validator);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'token' => ['required', 'string', 'max:6'],
        ];
    }
}
