<?php

namespace Modules\Auth\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class AuthResponse extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // note: temporary not using
        // if (!$this->resource->hasVerifiedEmail()) abort(503, "Sila sahkan emel anda.");

        return $this->resource->withoutRelations()->toArray();
    }

    public function with(Request $request)
    {
        return [
            'message' => 'Successful Retrieved Data',
            'status'  => 200,
        ];
    }
}
