<?php

namespace Modules\Auth\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class RegisterResponse extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->resource->withoutRelations()->toArray();
    }

    public function with(Request $request)
    {
        return [
            'message' => 'Successful Register',
            'status'  => 201,
        ];
    }
}
