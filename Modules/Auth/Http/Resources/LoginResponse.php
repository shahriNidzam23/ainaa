<?php

namespace Modules\Auth\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Modules\Auth\Models\Role;

class LoginResponse extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // note: temporary not using
        // if (!$this->resource->hasVerifiedEmail()) abort(503, "Sila sahkan emel anda.");
        $this->resource->load("agent");

        return [
            'user' => $this->resource->withoutRelations()->toArray(),
            'profile' => $this->resource?->profile ?? [],
            'permissions' => $this->permissions($this->resource),
            'token' => $this->resource->createToken(config('auth.token_name'), ['*'])->plainTextToken,
            'role' => $this->resource->roles->pluck('name')->first(),
            'agent' => $this->resource->agent,
        ];
    }

    private function permissions($user) {


        $permissions = $user->getAllPermissions();
        $roles = Role::with(['permissions',])->whereIn('name', $user->getRoleNames())->get();

        foreach ($roles as $key => $role) {
            foreach ($role->permissions as $key => $permission) {
                $permissions[] = $permission;
            }
        }

        return $permissions->map(function ($permission) {
            return $permission->name;
        });
    }

    public function with(Request $request)
    {
        return [
            'message' => 'Successful Logged In',
            'status'  => 200,
        ];
    }
}
