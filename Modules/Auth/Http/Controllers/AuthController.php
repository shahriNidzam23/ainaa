<?php

namespace Modules\Auth\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\Auth\Bloc\AuthBloc;
use Modules\Auth\Http\Requests\{
    VerifyEmailRequest,
    LoginRequest,
    ForgotPasswordRequest,
    ResetPasswordRequest,
    RegisterRequest,
    VerifyTokenRequest
};
use Modules\Auth\Http\Resources\{
    AuthResponse,
    LoginResponse,
    RegisterResponse,
};


class AuthController extends Controller
{

    public function __construct(
        private AuthBloc $authBloc
    ) {
    }

    public function get(Request $request)
    {
        $result = $this->authBloc->get($request);

        return new AuthResponse($result);
    }

    public function requestToken(Request $request)
    {
        $this->authBloc->requestToken($request);

        return response()->json([
            'message' => 'Please refer your email for one time password ',
            'status'  => 200,
        ]);
    }

    public function login(LoginRequest $request)
    {
        $result = $this->authBloc->login($request);

        return new LoginResponse($result);
    }

    public function register(RegisterRequest $request)
    {
        $result = $this->authBloc->register($request);

        return new RegisterResponse($result);
    }

    public function registerBulk(Request $request)
    {
        $this->authBloc->registerBulk($request);

        return response()->json([
            'message' => 'Successfully Register',
            'status'  => 200,
        ]);
    }

    public function logout(Request $request)
    {
        $this->authBloc->logout($request);

        return response()->json([
            'message' => 'Successful Logged Out',
            'status'  => 200,
        ]);
    }
}
