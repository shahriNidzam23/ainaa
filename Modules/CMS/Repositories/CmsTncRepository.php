<?php

namespace Modules\CMS\Repositories;

use Rekamy\LaravelCore\Crudable\Concern\CrudableRepository;
use Rekamy\LaravelCore\Contracts\CrudableRepository as CrudableRepositoryContract;
use Modules\CMS\Models\CmsTnc;
use Rekamy\LaravelCore\Override\Repository;

/**
 * Class CmsTncRepository
 * @package Modules\CMS\Repositories
 *
 * @method CmsTnc find($id, $columns = ['*'])
 * @method CmsTnc find($id, $columns = ['*'])
 * @method CmsTnc first($columns = ['*'])
*/
class CmsTncRepository extends Repository implements CrudableRepositoryContract
{
    use CrudableRepository;

    /**
     * @var array
     */
    protected $fieldSearchable = [
		'content',
	];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CmsTnc::class;
    }
    
}

