<?php

namespace Modules\CMS\Repositories;

use Rekamy\LaravelCore\Crudable\Concern\CrudableRepository;
use Rekamy\LaravelCore\Contracts\CrudableRepository as CrudableRepositoryContract;
use Modules\CMS\Models\CmsPrivacyPolicy;
use Rekamy\LaravelCore\Override\Repository;

/**
 * Class CmsPrivacyPolicyRepository
 * @package Modules\CMS\Repositories
 *
 * @method CmsPrivacyPolicy find($id, $columns = ['*'])
 * @method CmsPrivacyPolicy find($id, $columns = ['*'])
 * @method CmsPrivacyPolicy first($columns = ['*'])
*/
class CmsPrivacyPolicyRepository extends Repository implements CrudableRepositoryContract
{
    use CrudableRepository;

    /**
     * @var array
     */
    protected $fieldSearchable = [
		'content',
	];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CmsPrivacyPolicy::class;
    }
    
}

