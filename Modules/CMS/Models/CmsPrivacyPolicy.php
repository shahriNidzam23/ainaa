<?php

namespace Modules\CMS\Models;

use Illuminate\Database\Eloquent\{SoftDeletes};
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\{BelongsTo, HasMany};
use Illuminate\Support\Str;
use App\Models\BaseModel as Model;
use App\Traits\DataPolicy;

class CmsPrivacyPolicy extends Model
{
    use DataPolicy;

	public $table = 'cms_privacy_policy';

	public $fillable = [
		'id',
		'content',
	];

	public $casts = [
		'content' => 'string',
	];

	public static function rules($scenario = 'default') {
		$rules['default'] = [
			'content' => 'required',
		];
		$rules['store'] = [
			'content' => 'required',
		];
		$rules['update'] = [
			'content' => 'required',
		];
		return $rules[$scenario];
	}
}
