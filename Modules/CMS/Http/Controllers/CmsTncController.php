<?php

namespace Modules\CMS\Http\Controllers;

use Rekamy\LaravelCore\Crudable\Abstract\CrudController;
use Modules\CMS\Bloc\CmsTncBloc;

class CmsTncController extends CrudController
{
    protected $baseBloc;
    protected $moduleName = '';
    private $result;

    public function __construct(CmsTncBloc $bloc) 
    {
        // $this->registerPassportScopes();
        $this->baseBloc = $bloc;
        $this->moduleName = 'cms_tnc';
    }

    public function registerPassportScopes()
    {
        $this->middleware('scope:cms_tnc_index')->only('index');
        $this->middleware('scope:cms_tnc_create')->only('store');
        $this->middleware('scope:cms_tnc_show')->only('show');
        $this->middleware('scope:cms_tnc_update')->only('update');
        $this->middleware('scope:cms_tnc_destroy')->only('destroy');
    }
}
