<?php

namespace Modules\CMS\Http\Controllers;

use Rekamy\LaravelCore\Crudable\Abstract\CrudController;
use Modules\CMS\Bloc\CmsPrivacyPolicyBloc;

class CmsPrivacyPolicyController extends CrudController
{
    protected $baseBloc;
    protected $moduleName = '';
    private $result;

    public function __construct(CmsPrivacyPolicyBloc $bloc) 
    {
        // $this->registerPassportScopes();
        $this->baseBloc = $bloc;
        $this->moduleName = 'cms_privacy_policy';
    }

    public function registerPassportScopes()
    {
        $this->middleware('scope:cms_privacy_policy_index')->only('index');
        $this->middleware('scope:cms_privacy_policy_create')->only('store');
        $this->middleware('scope:cms_privacy_policy_show')->only('show');
        $this->middleware('scope:cms_privacy_policy_update')->only('update');
        $this->middleware('scope:cms_privacy_policy_destroy')->only('destroy');
    }
}
