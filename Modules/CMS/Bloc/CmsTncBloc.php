<?php

namespace Modules\CMS\Bloc;

use Rekamy\LaravelCore\Crudable\Abstract\CrudBloc;
use Modules\CMS\Repositories\CmsTncRepository;
use Modules\CMS\Http\Requests\CmsTncRequest;
use Modules\CMS\Models\CmsTnc;

class CmsTncBloc extends CrudBloc
{
    public function __construct(
        public CmsTncRepository $repo,
        public CmsTncRequest $request,
    ) {
        $this->repo = $repo;
        $this->request = $request;
    }

    public static function permission($name) {
    //     $permission = [
    //         'index' => 'cms_tnc_index',
    //         'create' => 'cms_tnc_create',
    //         'show' => 'cms_tnc_show',
    //         'update' => 'cms_tnc_update',
    //         'destroy' => 'cms_tnc_destroy',
    //     ];

    //     return $permission[$name];
    }

    public function index($input)
    {
        return CmsTnc::bypassDataPolicy(function () use ($input){
            return $this->repo->indexAction($input)->toArray();
        });
    }


    public function show($id)
    {
        return CmsTnc::bypassDataPolicy(function () use ($id){
            return $this->repo->showAction($id);
        });
    }
}
