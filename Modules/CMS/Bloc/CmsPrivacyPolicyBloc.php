<?php

namespace Modules\CMS\Bloc;

use Rekamy\LaravelCore\Crudable\Abstract\CrudBloc;
use Modules\CMS\Repositories\CmsPrivacyPolicyRepository;
use Modules\CMS\Http\Requests\CmsPrivacyPolicyRequest;
use Modules\CMS\Models\CmsPrivacyPolicy;

class CmsPrivacyPolicyBloc extends CrudBloc
{
    public function __construct(
        public CmsPrivacyPolicyRepository $repo,
        public CmsPrivacyPolicyRequest $request,
    ) {
        $this->repo = $repo;
        $this->request = $request;
    }

    public static function permission($name) {
    //     $permission = [
    //         'index' => 'cms_privacy_policy_index',
    //         'create' => 'cms_privacy_policy_create',
    //         'show' => 'cms_privacy_policy_show',
    //         'update' => 'cms_privacy_policy_update',
    //         'destroy' => 'cms_privacy_policy_destroy',
    //     ];

    //     return $permission[$name];
    }

    public function index($input)
    {
        return CmsPrivacyPolicy::bypassDataPolicy(function () use ($input){
            return $this->repo->indexAction($input)->toArray();
        });
    }


    public function show($id)
    {
        return CmsPrivacyPolicy::bypassDataPolicy(function () use ($id){
            return $this->repo->showAction($id);
        });
    }
}
