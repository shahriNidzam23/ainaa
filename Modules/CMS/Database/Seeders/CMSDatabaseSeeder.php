<?php

namespace Modules\CMS\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class CMSDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table("cms_privacy_policy")->insert([
            "content" => file_get_contents(__DIR__ . "/../data/privacypolicy.html")
        ]);


        \DB::table("cms_tnc")->insert([
            "content" => file_get_contents(__DIR__ . "/../data/tnc.html")
        ]);

        $this->call([
            CMSRolePermissionSeeder::class
        ]);
    }
}
