<?php

namespace Modules\CMS\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Auth\Database\Seeders\RolePermissionSeeder;

class CMSRolePermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $access = [
            'superadmin' => $this->admin(),
            'hq' => $this->admin(),
            'admin' => $this->admin(),
        ];
        RolePermissionSeeder::registerRP('CMS', $access);
    }

    private function admin() {
        return [
            'cms_tnc:create',
            'cms_tnc:view',
            'cms_tnc:update',
            'cms_tnc:delete',

            'cms_privacy_policy:create',
            'cms_privacy_policy:view',
            'cms_privacy_policy:update',
            'cms_privacy_policy:delete',
        ];
    }
}
