<?php

namespace Modules\CMS\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::group(['as' => 'cms.', 'middleware' => ['auth:sanctum']], function () {
    Route::put('/privacy-policy/{id}', [CmsPrivacyPolicyController::class, 'update']);
    Route::put('/tnc/{id}', [CmsTncController::class, 'update']);
});

Route::group(['as' => 'cms.public.'], function () {
    Route::get('/privacy-policy', [CmsPrivacyPolicyController::class, 'index']);
    Route::get('/tnc', [CmsTncController::class, 'index']);
    Route::get('/privacy-policy/{id}', [CmsPrivacyPolicyController::class, 'show']);
    Route::get('/tnc/{id}', [CmsTncController::class, 'show']);
});
