<?php

use Illuminate\Foundation\Inspiring;
use Illuminate\Support\Facades\Artisan;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->purpose('Display an inspiring quote');


Artisan::command('storage:clear', function () {
    $filesToPreserve = \DB::table("common_attachments")->get()->pluck("path")->toArray();
    $foldersToPreserve = [".", "credentials", "modules", "default", "public", "system"];

    // Get all files in the storage directory
    $filesInStorage = Storage::allFiles();
    foreach ($filesInStorage as $filename) {



        // Check if the file should be deleted or preserved
        if (in_array(dirname($filename), $foldersToPreserve)) continue;
        if (in_array($filename, $filesToPreserve)) continue;

        // Delete the file
        Storage::delete($filename);

        // Optionally, you can output a message indicating the deleted file
        $this->info('Deleted: ' . $filename);
    }

    $this->info('Files have been deleted except those listed.');
})->purpose('Clear Storage');
