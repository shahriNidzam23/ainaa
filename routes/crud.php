<?php

namespace Modules\Crud\Http\Controllers;

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::apiResources([
    'bookmark' => BookmarkController::class,

    'checkin' => CheckinController::class,

    'permission' => PermissionController::class,

    'role' => RoleController::class,

    'solatspot' => SolatspotController::class,

    'user-profile' => UserProfileController::class,

    'user' => UserController::class,
]);
