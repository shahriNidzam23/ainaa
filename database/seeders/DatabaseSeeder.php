<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Modules\Agent\Database\Seeders\AgentDatabaseSeeder;
use Modules\Auth\Database\Seeders\AuthDatabaseSeeder;
use Modules\User\Database\Seeders\UserDatabaseSeeder;
use Modules\Common\Database\Seeders\CommonDatabaseSeeder;
use Modules\Order\Database\Seeders\OrderDatabaseSeeder;
use Modules\Warehouse\Database\Seeders\WarehouseDatabaseSeeder;
use Modules\SocialMedia\Database\Seeders\SocialMediaDatabaseSeeder;
use Modules\CMS\Database\Seeders\CMSDatabaseSeeder;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->call([
            AuthDatabaseSeeder::class,
            UserDatabaseSeeder::class,
            AgentDatabaseSeeder::class,
            CommonDatabaseSeeder::class,
            WarehouseDatabaseSeeder::class,
            OrderDatabaseSeeder::class,
            SocialMediaDatabaseSeeder::class,
            CMSDatabaseSeeder::class
        ]);
    }
}
