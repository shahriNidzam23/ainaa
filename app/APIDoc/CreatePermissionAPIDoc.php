<?php

namespace App\APIDoc;

/**
 * @OA\Post(
 *     path="/api/crud/permission",
 *     tags={"Permissions"},
 *     summary="Store Permissions",
 *     description="Store a Permissions into database",
 *     @OA\RequestBody(
 *          required=true,
 *          @OA\JsonContent(ref="#/components/schemas/Permissions"),
 *     ),
 *     @OA\Response(response=200, description="Permissions Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class CreatePermissionAPIDoc {
}
