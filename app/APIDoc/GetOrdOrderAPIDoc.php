<?php

namespace App\APIDoc;

/**
 * @OA\Get(
 *     path="/api/crud/ord-order",
 *     tags={"OrdOrders"},
 *     summary="Get Ord Orders",
 *     description="Get list of Ord Orders",
 *     @OA\Response(response=200, description="Ord Orders Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class GetOrdOrderAPIDoc {
} 
