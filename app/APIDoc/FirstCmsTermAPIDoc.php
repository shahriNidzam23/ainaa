<?php

namespace App\APIDoc;

/**
 * @OA\Get(
 *     path="/api/crud/cms-term/{id}",
 *     tags={"CmsTerms"},
 *     summary="Get a Cms Terms by ID",
 *     description="Get Cms Terms by id",
 *     @OA\Response(response=200, description="Cms Terms Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 *     @OA\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          @OA\Schema(
 *              type="string"
 *          )
 *     ),
 * )
 */
class FirstCmsTermAPIDoc {
} 
