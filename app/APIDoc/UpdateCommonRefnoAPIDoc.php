<?php

namespace App\APIDoc;

/**
 * @OA\Patch(
 *     path="/api/crud/common-refno/{id}",
 *     tags={"CommonRefnos"},
 *     summary="Update a Common Refnos by ID",
 *     description="Update Common Refnos",
 *     @OA\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          @OA\Schema(type="string")
 *     ),
 *     @OA\RequestBody(
 *          required=true,
 *          @OA\JsonContent(ref="#/components/schemas/Common Refnos"),
 *     ),
 *     @OA\Response(response=200, description="Common Refnos Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class UpdateCommonRefnoAPIDoc {
}
