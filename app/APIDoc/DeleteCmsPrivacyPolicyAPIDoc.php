<?php

namespace App\APIDoc;

/**
 * @OA\Delete(
 *     path="/api/crud/cms-privacy-policy/{id}",
 *     tags={"CmsPrivacyPolicy"},
 *     summary="Delete a Cms Privacy Policy by ID",
 *     description="Delete Cms Privacy Policy",
 *     @OA\Response(response=200, description="Cms Privacy Policy Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 *     @OA\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *     ),
 * )
 */
class DeleteCmsPrivacyPolicyAPIDoc {
} 
