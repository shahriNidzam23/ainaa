<?php

namespace App\APIDoc;

/**
 * @OA\Get(
 *     path="/api/crud/war-transfer",
 *     tags={"WarTransfers"},
 *     summary="Get War Transfers",
 *     description="Get list of War Transfers",
 *     @OA\Response(response=200, description="War Transfers Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class GetWarTransferAPIDoc {
} 
