<?php

namespace App\APIDoc;

/**
 * @OA\Patch(
 *     path="/api/crud/ord-customer/{id}",
 *     tags={"OrdCustomers"},
 *     summary="Update a Ord Customers by ID",
 *     description="Update Ord Customers",
 *     @OA\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          @OA\Schema(type="string")
 *     ),
 *     @OA\RequestBody(
 *          required=true,
 *          @OA\JsonContent(ref="#/components/schemas/Ord Customers"),
 *     ),
 *     @OA\Response(response=200, description="Ord Customers Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class UpdateOrdCustomerAPIDoc {
}
