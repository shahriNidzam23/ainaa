<?php

namespace App\APIDoc;

/**
 * @OA\Delete(
 *     path="/api/crud/ord-sale/{id}",
 *     tags={"OrdSales"},
 *     summary="Delete a Ord Sales by ID",
 *     description="Delete Ord Sales",
 *     @OA\Response(response=200, description="Ord Sales Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 *     @OA\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *     ),
 * )
 */
class DeleteOrdSaleAPIDoc {
} 
