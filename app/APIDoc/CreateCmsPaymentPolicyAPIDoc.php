<?php

namespace App\APIDoc;

/**
 * @OA\Post(
 *     path="/api/crud/cms-payment-policy",
 *     tags={"CmsPaymentPolicy"},
 *     summary="Store Cms Payment Policy",
 *     description="Store a Cms Payment Policy into database",
 *     @OA\RequestBody(
 *          required=true,
 *          @OA\JsonContent(ref="#/components/schemas/Cms Payment Policy"),
 *     ),
 *     @OA\Response(response=200, description="Cms Payment Policy Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class CreateCmsPaymentPolicyAPIDoc {
}
