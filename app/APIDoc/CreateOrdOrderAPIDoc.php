<?php

namespace App\APIDoc;

/**
 * @OA\Post(
 *     path="/api/crud/ord-order",
 *     tags={"OrdOrders"},
 *     summary="Store Ord Orders",
 *     description="Store a Ord Orders into database",
 *     @OA\RequestBody(
 *          required=true,
 *          @OA\JsonContent(ref="#/components/schemas/Ord Orders"),
 *     ),
 *     @OA\Response(response=200, description="Ord Orders Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class CreateOrdOrderAPIDoc {
}
