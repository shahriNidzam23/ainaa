<?php

namespace App\APIDoc;

/**
 * @OA\Get(
 *     path="/api/crud/cms-term",
 *     tags={"CmsTerms"},
 *     summary="Get Cms Terms",
 *     description="Get list of Cms Terms",
 *     @OA\Response(response=200, description="Cms Terms Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class GetCmsTermAPIDoc {
} 
