<?php

namespace App\APIDoc;

/**
 * @OA\Post(
 *     path="/api/crud/ord-tax",
 *     tags={"OrdTaxes"},
 *     summary="Store Ord Taxes",
 *     description="Store a Ord Taxes into database",
 *     @OA\RequestBody(
 *          required=true,
 *          @OA\JsonContent(ref="#/components/schemas/Ord Taxes"),
 *     ),
 *     @OA\Response(response=200, description="Ord Taxes Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class CreateOrdTaxAPIDoc {
}
