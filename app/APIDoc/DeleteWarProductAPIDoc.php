<?php

namespace App\APIDoc;

/**
 * @OA\Delete(
 *     path="/api/crud/war-product/{id}",
 *     tags={"WarProducts"},
 *     summary="Delete a War Products by ID",
 *     description="Delete War Products",
 *     @OA\Response(response=200, description="War Products Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 *     @OA\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *     ),
 * )
 */
class DeleteWarProductAPIDoc {
} 
