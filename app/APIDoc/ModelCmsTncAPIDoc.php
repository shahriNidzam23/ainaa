<?php

namespace App\APIDoc;

/**
 * @OA\Schema(
 *     schema="Cms Tnc",
 *     description="Model Cms Tnc schema",
  *    required={ "content" },
 *     @OA\Property(
 *          property="content",
 *          description="content",
 *          type="string",
 *          example="text",
 *     ),
 * )
 */
class ModelCmsTncAPIDoc {
}
