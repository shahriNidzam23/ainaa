<?php

namespace App\APIDoc;

/**
 * @OA\Patch(
 *     path="/api/crud/cms-tnc/{id}",
 *     tags={"CmsTnc"},
 *     summary="Update a Cms Tnc by ID",
 *     description="Update Cms Tnc",
 *     @OA\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          @OA\Schema(type="string")
 *     ),
 *     @OA\RequestBody(
 *          required=true,
 *          @OA\JsonContent(ref="#/components/schemas/Cms Tnc"),
 *     ),
 *     @OA\Response(response=200, description="Cms Tnc Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class UpdateCmsTncAPIDoc {
}
