<?php

namespace App\APIDoc;

/**
 * @OA\Schema(
 *     schema="Users",
 *     description="Model Users schema",
  *    required={ "email", "display_name" },
 *     @OA\Property(
 *          property="email",
 *          description="email",
 *          type="string",
 *          example="string",
 *     ),
 *     @OA\Property(
 *          property="display_name",
 *          description="display_name",
 *          type="string",
 *          example="string",
 *     ),
 *     @OA\Property(
 *          property="email_verified_at",
 *          description="email_verified_at",
 *          type="datetime",
 *          example="2020-01-01 13:00",
 *     ),
 *     @OA\Property(
 *          property="ott",
 *          description="ott",
 *          type="string",
 *          example="string",
 *     ),
 * )
 */
class ModelUserAPIDoc {
}
