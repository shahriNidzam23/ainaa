<?php

namespace App\APIDoc;

/**
 * @OA\Patch(
 *     path="/api/crud/user/{id}",
 *     tags={"Users"},
 *     summary="Update a Users by ID",
 *     description="Update Users",
 *     @OA\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          @OA\Schema(type="string")
 *     ),
 *     @OA\RequestBody(
 *          required=true,
 *          @OA\JsonContent(ref="#/components/schemas/Users"),
 *     ),
 *     @OA\Response(response=200, description="Users Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class UpdateUserAPIDoc {
}
