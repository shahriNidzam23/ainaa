<?php

namespace App\APIDoc;

/**
 * @OA\Get(
 *     path="/api/crud/common-refno",
 *     tags={"CommonRefnos"},
 *     summary="Get Common Refnos",
 *     description="Get list of Common Refnos",
 *     @OA\Response(response=200, description="Common Refnos Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class GetCommonRefnoAPIDoc {
} 
