<?php

namespace App\APIDoc;

/**
 * @OA\Patch(
 *     path="/api/crud/common-reference/{id}",
 *     tags={"CommonReferences"},
 *     summary="Update a Common References by ID",
 *     description="Update Common References",
 *     @OA\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          @OA\Schema(type="string")
 *     ),
 *     @OA\RequestBody(
 *          required=true,
 *          @OA\JsonContent(ref="#/components/schemas/Common References"),
 *     ),
 *     @OA\Response(response=200, description="Common References Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class UpdateCommonReferenceAPIDoc {
}
