<?php

namespace App\APIDoc;

/**
 * @OA\Get(
 *     path="/api/crud/war-product",
 *     tags={"WarProducts"},
 *     summary="Get War Products",
 *     description="Get list of War Products",
 *     @OA\Response(response=200, description="War Products Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class GetWarProductAPIDoc {
} 
