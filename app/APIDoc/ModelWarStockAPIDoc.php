<?php

namespace App\APIDoc;

/**
 * @OA\Schema(
 *     schema="War Stocks",
 *     description="Model War Stocks schema",
  *    required={ "product_id", "user_id", "quantity" },
 *     @OA\Property(
 *          property="product_id",
 *          description="product_id",
 *          type="string",
 *          example="string",
 *     ),
 *     @OA\Property(
 *          property="user_id",
 *          description="user_id",
 *          type="string",
 *          example="string",
 *     ),
 *     @OA\Property(
 *          property="quantity",
 *          description="quantity",
 *          type="int64",
 *          example="1",
 *     ),
 * )
 */
class ModelWarStockAPIDoc {
}
