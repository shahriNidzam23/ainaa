<?php

namespace App\APIDoc;

/**
 * @OA\Patch(
 *     path="/api/crud/cms-term/{id}",
 *     tags={"CmsTerms"},
 *     summary="Update a Cms Terms by ID",
 *     description="Update Cms Terms",
 *     @OA\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          @OA\Schema(type="string")
 *     ),
 *     @OA\RequestBody(
 *          required=true,
 *          @OA\JsonContent(ref="#/components/schemas/Cms Terms"),
 *     ),
 *     @OA\Response(response=200, description="Cms Terms Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class UpdateCmsTermAPIDoc {
}
