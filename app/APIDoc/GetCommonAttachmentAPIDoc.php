<?php

namespace App\APIDoc;

/**
 * @OA\Get(
 *     path="/api/crud/common-attachment",
 *     tags={"CommonAttachments"},
 *     summary="Get Common Attachments",
 *     description="Get list of Common Attachments",
 *     @OA\Response(response=200, description="Common Attachments Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class GetCommonAttachmentAPIDoc {
} 
