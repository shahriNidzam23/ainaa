<?php

namespace App\APIDoc;

/**
 * @OA\Patch(
 *     path="/api/crud/cms-privacy-policy/{id}",
 *     tags={"CmsPrivacyPolicy"},
 *     summary="Update a Cms Privacy Policy by ID",
 *     description="Update Cms Privacy Policy",
 *     @OA\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          @OA\Schema(type="string")
 *     ),
 *     @OA\RequestBody(
 *          required=true,
 *          @OA\JsonContent(ref="#/components/schemas/Cms Privacy Policy"),
 *     ),
 *     @OA\Response(response=200, description="Cms Privacy Policy Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class UpdateCmsPrivacyPolicyAPIDoc {
}
