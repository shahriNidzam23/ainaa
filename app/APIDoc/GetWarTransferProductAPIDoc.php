<?php

namespace App\APIDoc;

/**
 * @OA\Get(
 *     path="/api/crud/war-transfer-product",
 *     tags={"WarTransferProducts"},
 *     summary="Get War Transfer Products",
 *     description="Get list of War Transfer Products",
 *     @OA\Response(response=200, description="War Transfer Products Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class GetWarTransferProductAPIDoc {
} 
