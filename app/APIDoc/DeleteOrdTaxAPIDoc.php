<?php

namespace App\APIDoc;

/**
 * @OA\Delete(
 *     path="/api/crud/ord-tax/{id}",
 *     tags={"OrdTaxes"},
 *     summary="Delete a Ord Taxes by ID",
 *     description="Delete Ord Taxes",
 *     @OA\Response(response=200, description="Ord Taxes Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 *     @OA\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *     ),
 * )
 */
class DeleteOrdTaxAPIDoc {
} 
