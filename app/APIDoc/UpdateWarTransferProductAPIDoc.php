<?php

namespace App\APIDoc;

/**
 * @OA\Patch(
 *     path="/api/crud/war-transfer-product/{id}",
 *     tags={"WarTransferProducts"},
 *     summary="Update a War Transfer Products by ID",
 *     description="Update War Transfer Products",
 *     @OA\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          @OA\Schema(type="string")
 *     ),
 *     @OA\RequestBody(
 *          required=true,
 *          @OA\JsonContent(ref="#/components/schemas/War Transfer Products"),
 *     ),
 *     @OA\Response(response=200, description="War Transfer Products Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class UpdateWarTransferProductAPIDoc {
}
