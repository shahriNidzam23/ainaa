<?php

namespace App\APIDoc;

/**
 * @OA\Patch(
 *     path="/api/crud/order-list/{id}",
 *     tags={"OrderLists"},
 *     summary="Update a Order Lists by ID",
 *     description="Update Order Lists",
 *     @OA\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          @OA\Schema(type="string")
 *     ),
 *     @OA\RequestBody(
 *          required=true,
 *          @OA\JsonContent(ref="#/components/schemas/Order Lists"),
 *     ),
 *     @OA\Response(response=200, description="Order Lists Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class UpdateOrderListAPIDoc {
}
