<?php

namespace App\APIDoc;

/**
 * @OA\Patch(
 *     path="/api/crud/user-socialmedia/{id}",
 *     tags={"UserSocialmedias"},
 *     summary="Update a User Socialmedias by ID",
 *     description="Update User Socialmedias",
 *     @OA\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          @OA\Schema(type="string")
 *     ),
 *     @OA\RequestBody(
 *          required=true,
 *          @OA\JsonContent(ref="#/components/schemas/User Socialmedias"),
 *     ),
 *     @OA\Response(response=200, description="User Socialmedias Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class UpdateUserSocialmediaAPIDoc {
}
