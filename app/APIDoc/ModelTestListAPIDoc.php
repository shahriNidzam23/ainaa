<?php

namespace App\APIDoc;

/**
 * @OA\Schema(
 *     schema="Test Lists",
 *     description="Model Test Lists schema",
 *     @OA\Property(
 *          property="name",
 *          description="name",
 *          type="string",
 *          example="string",
 *     ),
 * )
 */
class ModelTestListAPIDoc {
}
