<?php

namespace App\APIDoc;

/**
 * @OA\Post(
 *     path="/api/crud/common-notification",
 *     tags={"CommonNotifications"},
 *     summary="Store Common Notifications",
 *     description="Store a Common Notifications into database",
 *     @OA\RequestBody(
 *          required=true,
 *          @OA\JsonContent(ref="#/components/schemas/Common Notifications"),
 *     ),
 *     @OA\Response(response=200, description="Common Notifications Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class CreateCommonNotificationAPIDoc {
}
