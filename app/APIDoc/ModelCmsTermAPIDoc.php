<?php

namespace App\APIDoc;

/**
 * @OA\Schema(
 *     schema="Cms Terms",
 *     description="Model Cms Terms schema",
  *    required={ "template" },
 *     @OA\Property(
 *          property="template",
 *          description="template",
 *          type="string",
 *          example="text",
 *     ),
 * )
 */
class ModelCmsTermAPIDoc {
}
