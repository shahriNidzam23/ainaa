<?php

namespace App\APIDoc;

/**
 * @OA\Post(
 *     path="/api/crud/user",
 *     tags={"Users"},
 *     summary="Store Users",
 *     description="Store a Users into database",
 *     @OA\RequestBody(
 *          required=true,
 *          @OA\JsonContent(ref="#/components/schemas/Users"),
 *     ),
 *     @OA\Response(response=200, description="Users Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class CreateUserAPIDoc {
}
