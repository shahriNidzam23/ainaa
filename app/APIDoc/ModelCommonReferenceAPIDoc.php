<?php

namespace App\APIDoc;

/**
 * @OA\Schema(
 *     schema="Common References",
 *     description="Model Common References schema",
  *    required={ "name", "value", "code", "order" },
 *     @OA\Property(
 *          property="name",
 *          description="name",
 *          type="string",
 *          example="string",
 *     ),
 *     @OA\Property(
 *          property="value",
 *          description="value",
 *          type="string",
 *          example="string",
 *     ),
 *     @OA\Property(
 *          property="code",
 *          description="code",
 *          type="string",
 *          example="string",
 *     ),
 *     @OA\Property(
 *          property="order",
 *          description="order",
 *          type="int64",
 *          example="1",
 *     ),
 * )
 */
class ModelCommonReferenceAPIDoc {
}
