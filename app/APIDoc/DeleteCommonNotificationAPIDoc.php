<?php

namespace App\APIDoc;

/**
 * @OA\Delete(
 *     path="/api/crud/common-notification/{id}",
 *     tags={"CommonNotifications"},
 *     summary="Delete a Common Notifications by ID",
 *     description="Delete Common Notifications",
 *     @OA\Response(response=200, description="Common Notifications Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 *     @OA\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *     ),
 * )
 */
class DeleteCommonNotificationAPIDoc {
} 
