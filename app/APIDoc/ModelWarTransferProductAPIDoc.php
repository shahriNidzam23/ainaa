<?php

namespace App\APIDoc;

/**
 * @OA\Schema(
 *     schema="War Transfer Products",
 *     description="Model War Transfer Products schema",
  *    required={ "transfer_id", "product_id", "receive_quantity", "rejected_quantity", "accepted_quantity" },
 *     @OA\Property(
 *          property="transfer_id",
 *          description="transfer_id",
 *          type="string",
 *          example="string",
 *     ),
 *     @OA\Property(
 *          property="product_id",
 *          description="product_id",
 *          type="string",
 *          example="string",
 *     ),
 *     @OA\Property(
 *          property="receive_quantity",
 *          description="receive_quantity",
 *          type="int64",
 *          example="1",
 *     ),
 *     @OA\Property(
 *          property="rejected_quantity",
 *          description="rejected_quantity",
 *          type="int64",
 *          example="1",
 *     ),
 *     @OA\Property(
 *          property="accepted_quantity",
 *          description="accepted_quantity",
 *          type="int64",
 *          example="1",
 *     ),
 * )
 */
class ModelWarTransferProductAPIDoc {
}
