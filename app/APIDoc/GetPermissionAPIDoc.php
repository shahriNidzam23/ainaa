<?php

namespace App\APIDoc;

/**
 * @OA\Get(
 *     path="/api/crud/permission",
 *     tags={"Permissions"},
 *     summary="Get Permissions",
 *     description="Get list of Permissions",
 *     @OA\Response(response=200, description="Permissions Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class GetPermissionAPIDoc {
} 
