<?php

namespace App\APIDoc;

/**
 * @OA\Post(
 *     path="/api/crud/war-asset-inbound",
 *     tags={"WarAssetInbound"},
 *     summary="Store War Asset Inbound",
 *     description="Store a War Asset Inbound into database",
 *     @OA\RequestBody(
 *          required=true,
 *          @OA\JsonContent(ref="#/components/schemas/War Asset Inbound"),
 *     ),
 *     @OA\Response(response=200, description="War Asset Inbound Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class CreateWarAssetInboundAPIDoc {
}
