<?php

namespace App\APIDoc;

/**
 * @OA\Post(
 *     path="/api/crud/ord-sale",
 *     tags={"OrdSales"},
 *     summary="Store Ord Sales",
 *     description="Store a Ord Sales into database",
 *     @OA\RequestBody(
 *          required=true,
 *          @OA\JsonContent(ref="#/components/schemas/Ord Sales"),
 *     ),
 *     @OA\Response(response=200, description="Ord Sales Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class CreateOrdSaleAPIDoc {
}
