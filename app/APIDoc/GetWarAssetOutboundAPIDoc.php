<?php

namespace App\APIDoc;

/**
 * @OA\Get(
 *     path="/api/crud/war-asset-outbound",
 *     tags={"WarAssetOutbound"},
 *     summary="Get War Asset Outbound",
 *     description="Get list of War Asset Outbound",
 *     @OA\Response(response=200, description="War Asset Outbound Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class GetWarAssetOutboundAPIDoc {
} 
