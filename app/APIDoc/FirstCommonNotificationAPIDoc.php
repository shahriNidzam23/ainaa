<?php

namespace App\APIDoc;

/**
 * @OA\Get(
 *     path="/api/crud/common-notification/{id}",
 *     tags={"CommonNotifications"},
 *     summary="Get a Common Notifications by ID",
 *     description="Get Common Notifications by id",
 *     @OA\Response(response=200, description="Common Notifications Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 *     @OA\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          @OA\Schema(
 *              type="string"
 *          )
 *     ),
 * )
 */
class FirstCommonNotificationAPIDoc {
} 
