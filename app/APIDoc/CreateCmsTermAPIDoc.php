<?php

namespace App\APIDoc;

/**
 * @OA\Post(
 *     path="/api/crud/cms-term",
 *     tags={"CmsTerms"},
 *     summary="Store Cms Terms",
 *     description="Store a Cms Terms into database",
 *     @OA\RequestBody(
 *          required=true,
 *          @OA\JsonContent(ref="#/components/schemas/Cms Terms"),
 *     ),
 *     @OA\Response(response=200, description="Cms Terms Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class CreateCmsTermAPIDoc {
}
