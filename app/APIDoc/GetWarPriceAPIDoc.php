<?php

namespace App\APIDoc;

/**
 * @OA\Get(
 *     path="/api/crud/war-price",
 *     tags={"WarPrices"},
 *     summary="Get War Prices",
 *     description="Get list of War Prices",
 *     @OA\Response(response=200, description="War Prices Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class GetWarPriceAPIDoc {
} 
