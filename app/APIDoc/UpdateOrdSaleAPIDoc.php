<?php

namespace App\APIDoc;

/**
 * @OA\Patch(
 *     path="/api/crud/ord-sale/{id}",
 *     tags={"OrdSales"},
 *     summary="Update a Ord Sales by ID",
 *     description="Update Ord Sales",
 *     @OA\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          @OA\Schema(type="string")
 *     ),
 *     @OA\RequestBody(
 *          required=true,
 *          @OA\JsonContent(ref="#/components/schemas/Ord Sales"),
 *     ),
 *     @OA\Response(response=200, description="Ord Sales Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class UpdateOrdSaleAPIDoc {
}
