<?php

namespace App\APIDoc;

/**
 * @OA\Post(
 *     path="/api/crud/war-transfer-product",
 *     tags={"WarTransferProducts"},
 *     summary="Store War Transfer Products",
 *     description="Store a War Transfer Products into database",
 *     @OA\RequestBody(
 *          required=true,
 *          @OA\JsonContent(ref="#/components/schemas/War Transfer Products"),
 *     ),
 *     @OA\Response(response=200, description="War Transfer Products Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class CreateWarTransferProductAPIDoc {
}
