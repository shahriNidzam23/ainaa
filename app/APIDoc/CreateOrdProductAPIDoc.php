<?php

namespace App\APIDoc;

/**
 * @OA\Post(
 *     path="/api/crud/ord-product",
 *     tags={"OrdProducts"},
 *     summary="Store Ord Products",
 *     description="Store a Ord Products into database",
 *     @OA\RequestBody(
 *          required=true,
 *          @OA\JsonContent(ref="#/components/schemas/Ord Products"),
 *     ),
 *     @OA\Response(response=200, description="Ord Products Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class CreateOrdProductAPIDoc {
}
