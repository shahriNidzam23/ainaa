<?php

namespace App\APIDoc;

/**
 * @OA\Post(
 *     path="/api/crud/ord-customer",
 *     tags={"OrdCustomers"},
 *     summary="Store Ord Customers",
 *     description="Store a Ord Customers into database",
 *     @OA\RequestBody(
 *          required=true,
 *          @OA\JsonContent(ref="#/components/schemas/Ord Customers"),
 *     ),
 *     @OA\Response(response=200, description="Ord Customers Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class CreateOrdCustomerAPIDoc {
}
