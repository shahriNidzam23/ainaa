<?php

namespace App\APIDoc;

/**
 * @OA\Patch(
 *     path="/api/crud/ord-package/{id}",
 *     tags={"OrdPackages"},
 *     summary="Update a Ord Packages by ID",
 *     description="Update Ord Packages",
 *     @OA\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          @OA\Schema(type="string")
 *     ),
 *     @OA\RequestBody(
 *          required=true,
 *          @OA\JsonContent(ref="#/components/schemas/Ord Packages"),
 *     ),
 *     @OA\Response(response=200, description="Ord Packages Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class UpdateOrdPackageAPIDoc {
}
