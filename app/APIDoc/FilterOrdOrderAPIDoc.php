<?php

namespace App\APIDoc;

use OpenApi\Attributes as OA;
use Symfony\Component\HttpFoundation\Response;

/**
 * @OA\Get(
 *     path="/api/crud/ord-order/filter",
 *     tags={"OrdOrders", "CRUD"},
 *     summary="General filter to get Ord Orders. Just name the column name",
 *     description="Filter Ord Orders Data",
 *     @OA\Response(response=200, description="Ord Orders Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=401, description="Unauthorized", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=403, description="Forbidden", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=422, description="Unprocessable Content", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=500, description="Internal Server Error", @OA\MediaType(mediaType="application/json")),*     @OA\Parameter(
 *          name="seller_id",
 *          in="query",
 *          required=true, 
 *          @OA\Schema(
 *              type=""
 *          ),
 *     ),
 *     @OA\Parameter(
 *          name="buyer_id",
 *          in="query",
 *          required=true, 
 *          @OA\Schema(
 *              type=""
 *          ),
 *     ),
 *     @OA\Parameter(
 *          name="payment_type",
 *          in="query",
 *          required=true, 
 *          @OA\Schema(
 *              type="string"
 *          ),
 *     ),
 *     @OA\Parameter(
 *          name="status",
 *          in="query",
 *          required=true, 
 *          @OA\Schema(
 *              type="string"
 *          ),
 *     ),
 *     @OA\Parameter(
 *          name="ref_no",
 *          in="query",
 *          required=true, 
 *          @OA\Schema(
 *              type="string"
 *          ),
 *     ),
 *     @OA\Parameter(
 *          name="notes",
 *          in="query",
 *          required=false, 
 *          @OA\Schema(
 *              type="string"
 *          ),
 *     ),
 * )
 */
class FilterOrdOrderAPIDoc {
} 
