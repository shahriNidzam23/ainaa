<?php

namespace App\APIDoc;

/**
 * @OA\Post(
 *     path="/api/crud/common-reference",
 *     tags={"CommonReferences"},
 *     summary="Store Common References",
 *     description="Store a Common References into database",
 *     @OA\RequestBody(
 *          required=true,
 *          @OA\JsonContent(ref="#/components/schemas/Common References"),
 *     ),
 *     @OA\Response(response=200, description="Common References Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class CreateCommonReferenceAPIDoc {
}
