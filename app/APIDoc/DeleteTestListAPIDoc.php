<?php

namespace App\APIDoc;

/**
 * @OA\Delete(
 *     path="/api/crud/test-list/{id}",
 *     tags={"TestLists"},
 *     summary="Delete a Test Lists by ID",
 *     description="Delete Test Lists",
 *     @OA\Response(response=200, description="Test Lists Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 *     @OA\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *     ),
 * )
 */
class DeleteTestListAPIDoc {
} 
