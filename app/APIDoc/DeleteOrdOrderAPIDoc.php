<?php

namespace App\APIDoc;

/**
 * @OA\Delete(
 *     path="/api/crud/ord-order/{id}",
 *     tags={"OrdOrders"},
 *     summary="Delete a Ord Orders by ID",
 *     description="Delete Ord Orders",
 *     @OA\Response(response=200, description="Ord Orders Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 *     @OA\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *     ),
 * )
 */
class DeleteOrdOrderAPIDoc {
} 
