<?php

namespace App\APIDoc;

use OpenApi\Attributes as OA;
use Symfony\Component\HttpFoundation\Response;

/**
 * @OA\Get(
 *     path="/api/crud/audit/filter",
 *     tags={"Audits", "CRUD"},
 *     summary="General filter to get Audits. Just name the column name",
 *     description="Filter Audits Data",
 *     @OA\Response(response=200, description="Audits Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=401, description="Unauthorized", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=403, description="Forbidden", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=422, description="Unprocessable Content", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=500, description="Internal Server Error", @OA\MediaType(mediaType="application/json")),*     @OA\Parameter(
 *          name="user_type",
 *          in="query",
 *          required=false, 
 *          @OA\Schema(
 *              type="string"
 *          ),
 *     ),
 *     @OA\Parameter(
 *          name="user_id",
 *          in="query",
 *          required=false, 
 *          @OA\Schema(
 *              type=""
 *          ),
 *     ),
 *     @OA\Parameter(
 *          name="event",
 *          in="query",
 *          required=true, 
 *          @OA\Schema(
 *              type="string"
 *          ),
 *     ),
 *     @OA\Parameter(
 *          name="auditable_type",
 *          in="query",
 *          required=true, 
 *          @OA\Schema(
 *              type="string"
 *          ),
 *     ),
 *     @OA\Parameter(
 *          name="auditable_id",
 *          in="query",
 *          required=true, 
 *          @OA\Schema(
 *              type=""
 *          ),
 *     ),
 *     @OA\Parameter(
 *          name="old_values",
 *          in="query",
 *          required=false, 
 *          @OA\Schema(
 *              type="string"
 *          ),
 *     ),
 *     @OA\Parameter(
 *          name="new_values",
 *          in="query",
 *          required=false, 
 *          @OA\Schema(
 *              type="string"
 *          ),
 *     ),
 *     @OA\Parameter(
 *          name="url",
 *          in="query",
 *          required=false, 
 *          @OA\Schema(
 *              type="string"
 *          ),
 *     ),
 *     @OA\Parameter(
 *          name="ip_address",
 *          in="query",
 *          required=false, 
 *          @OA\Schema(
 *              type="string"
 *          ),
 *     ),
 *     @OA\Parameter(
 *          name="user_agent",
 *          in="query",
 *          required=false, 
 *          @OA\Schema(
 *              type="string"
 *          ),
 *     ),
 *     @OA\Parameter(
 *          name="tags",
 *          in="query",
 *          required=false, 
 *          @OA\Schema(
 *              type="string"
 *          ),
 *     ),
 * )
 */
class FilterAuditAPIDoc {
} 
