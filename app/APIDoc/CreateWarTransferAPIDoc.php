<?php

namespace App\APIDoc;

/**
 * @OA\Post(
 *     path="/api/crud/war-transfer",
 *     tags={"WarTransfers"},
 *     summary="Store War Transfers",
 *     description="Store a War Transfers into database",
 *     @OA\RequestBody(
 *          required=true,
 *          @OA\JsonContent(ref="#/components/schemas/War Transfers"),
 *     ),
 *     @OA\Response(response=200, description="War Transfers Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class CreateWarTransferAPIDoc {
}
