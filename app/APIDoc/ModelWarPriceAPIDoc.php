<?php

namespace App\APIDoc;

/**
 * @OA\Schema(
 *     schema="War Prices",
 *     description="Model War Prices schema",
  *    required={ "product_id", "name", "code", "value", "active" },
 *     @OA\Property(
 *          property="product_id",
 *          description="product_id",
 *          type="string",
 *          example="string",
 *     ),
 *     @OA\Property(
 *          property="name",
 *          description="name",
 *          type="string",
 *          example="string",
 *     ),
 *     @OA\Property(
 *          property="code",
 *          description="code",
 *          type="string",
 *          example="string",
 *     ),
 *     @OA\Property(
 *          property="value",
 *          description="value",
 *          type="decimal",
 *          example="decimal",
 *     ),
 *     @OA\Property(
 *          property="active",
 *          description="active",
 *          type="boolean",
 *          example="1",
 *     ),
 * )
 */
class ModelWarPriceAPIDoc {
}
