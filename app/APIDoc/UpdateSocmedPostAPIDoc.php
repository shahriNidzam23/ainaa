<?php

namespace App\APIDoc;

/**
 * @OA\Patch(
 *     path="/api/crud/socmed-post/{id}",
 *     tags={"SocmedPosts"},
 *     summary="Update a Socmed Posts by ID",
 *     description="Update Socmed Posts",
 *     @OA\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          @OA\Schema(type="string")
 *     ),
 *     @OA\RequestBody(
 *          required=true,
 *          @OA\JsonContent(ref="#/components/schemas/Socmed Posts"),
 *     ),
 *     @OA\Response(response=200, description="Socmed Posts Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class UpdateSocmedPostAPIDoc {
}
