<?php

namespace App\APIDoc;

/**
 * @OA\Get(
 *     path="/api/crud/socmed-post",
 *     tags={"SocmedPosts"},
 *     summary="Get Socmed Posts",
 *     description="Get list of Socmed Posts",
 *     @OA\Response(response=200, description="Socmed Posts Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class GetSocmedPostAPIDoc {
} 
