<?php

namespace App\APIDoc;

/**
 * @OA\Delete(
 *     path="/api/crud/cms-term/{id}",
 *     tags={"CmsTerms"},
 *     summary="Delete a Cms Terms by ID",
 *     description="Delete Cms Terms",
 *     @OA\Response(response=200, description="Cms Terms Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 *     @OA\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *     ),
 * )
 */
class DeleteCmsTermAPIDoc {
} 
