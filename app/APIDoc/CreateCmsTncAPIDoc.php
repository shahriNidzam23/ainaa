<?php

namespace App\APIDoc;

/**
 * @OA\Post(
 *     path="/api/crud/cms-tnc",
 *     tags={"CmsTnc"},
 *     summary="Store Cms Tnc",
 *     description="Store a Cms Tnc into database",
 *     @OA\RequestBody(
 *          required=true,
 *          @OA\JsonContent(ref="#/components/schemas/Cms Tnc"),
 *     ),
 *     @OA\Response(response=200, description="Cms Tnc Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class CreateCmsTncAPIDoc {
}
