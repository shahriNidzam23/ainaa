<?php

namespace App\APIDoc;

/**
 * @OA\Patch(
 *     path="/api/crud/permission/{id}",
 *     tags={"Permissions"},
 *     summary="Update a Permissions by ID",
 *     description="Update Permissions",
 *     @OA\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          @OA\Schema(type="string")
 *     ),
 *     @OA\RequestBody(
 *          required=true,
 *          @OA\JsonContent(ref="#/components/schemas/Permissions"),
 *     ),
 *     @OA\Response(response=200, description="Permissions Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class UpdatePermissionAPIDoc {
}
