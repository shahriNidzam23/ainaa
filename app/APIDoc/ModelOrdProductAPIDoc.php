<?php

namespace App\APIDoc;

/**
 * @OA\Schema(
 *     schema="Ord Products",
 *     description="Model Ord Products schema",
  *    required={ "parent_type", "parent_id", "product_id", "price", "quantity" },
 *     @OA\Property(
 *          property="parent_type",
 *          description="parent_type",
 *          type="string",
 *          example="string",
 *     ),
 *     @OA\Property(
 *          property="parent_id",
 *          description="parent_id",
 *          type="string",
 *          example="string",
 *     ),
 *     @OA\Property(
 *          property="product_id",
 *          description="product_id",
 *          type="string",
 *          example="string",
 *     ),
 *     @OA\Property(
 *          property="price",
 *          description="price",
 *          type="float",
 *          example="float",
 *     ),
 *     @OA\Property(
 *          property="quantity",
 *          description="quantity",
 *          type="int64",
 *          example="1",
 *     ),
 * )
 */
class ModelOrdProductAPIDoc {
}
