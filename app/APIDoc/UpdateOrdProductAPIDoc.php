<?php

namespace App\APIDoc;

/**
 * @OA\Patch(
 *     path="/api/crud/ord-product/{id}",
 *     tags={"OrdProducts"},
 *     summary="Update a Ord Products by ID",
 *     description="Update Ord Products",
 *     @OA\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          @OA\Schema(type="string")
 *     ),
 *     @OA\RequestBody(
 *          required=true,
 *          @OA\JsonContent(ref="#/components/schemas/Ord Products"),
 *     ),
 *     @OA\Response(response=200, description="Ord Products Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class UpdateOrdProductAPIDoc {
}
