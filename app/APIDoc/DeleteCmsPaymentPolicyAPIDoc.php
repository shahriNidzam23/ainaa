<?php

namespace App\APIDoc;

/**
 * @OA\Delete(
 *     path="/api/crud/cms-payment-policy/{id}",
 *     tags={"CmsPaymentPolicy"},
 *     summary="Delete a Cms Payment Policy by ID",
 *     description="Delete Cms Payment Policy",
 *     @OA\Response(response=200, description="Cms Payment Policy Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 *     @OA\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *     ),
 * )
 */
class DeleteCmsPaymentPolicyAPIDoc {
} 
