<?php

namespace App\APIDoc;

/**
 * @OA\Schema(
 *     schema="Common Notifications",
 *     description="Model Common Notifications schema",
  *    required={ "user_id", "token" },
 *     @OA\Property(
 *          property="user_id",
 *          description="user_id",
 *          type="string",
 *          example="string",
 *     ),
 *     @OA\Property(
 *          property="token",
 *          description="token",
 *          type="string",
 *          example="string",
 *     ),
 * )
 */
class ModelCommonNotificationAPIDoc {
}
