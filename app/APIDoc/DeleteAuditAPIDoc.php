<?php

namespace App\APIDoc;

/**
 * @OA\Delete(
 *     path="/api/crud/audit/{id}",
 *     tags={"Audits"},
 *     summary="Delete a Audits by ID",
 *     description="Delete Audits",
 *     @OA\Response(response=200, description="Audits Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 *     @OA\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *     ),
 * )
 */
class DeleteAuditAPIDoc {
} 
