<?php

namespace App\APIDoc;

/**
 * @OA\Delete(
 *     path="/api/crud/war-price/{id}",
 *     tags={"WarPrices"},
 *     summary="Delete a War Prices by ID",
 *     description="Delete War Prices",
 *     @OA\Response(response=200, description="War Prices Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 *     @OA\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *     ),
 * )
 */
class DeleteWarPriceAPIDoc {
} 
