<?php

namespace App\APIDoc;

use OpenApi\Attributes as OA;
use Symfony\Component\HttpFoundation\Response;

/**
 * @OA\Get(
 *     path="/api/crud/cms-tnc/filter",
 *     tags={"CmsTnc", "CRUD"},
 *     summary="General filter to get Cms Tnc. Just name the column name",
 *     description="Filter Cms Tnc Data",
 *     @OA\Response(response=200, description="Cms Tnc Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=401, description="Unauthorized", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=403, description="Forbidden", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=422, description="Unprocessable Content", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=500, description="Internal Server Error", @OA\MediaType(mediaType="application/json")),*     @OA\Parameter(
 *          name="content",
 *          in="query",
 *          required=true, 
 *          @OA\Schema(
 *              type="string"
 *          ),
 *     ),
 * )
 */
class FilterCmsTncAPIDoc {
} 
