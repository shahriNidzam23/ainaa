<?php

namespace App\APIDoc;

/**
 * @OA\Get(
 *     path="/api/crud/permission/{id}",
 *     tags={"Permissions"},
 *     summary="Get a Permissions by ID",
 *     description="Get Permissions by id",
 *     @OA\Response(response=200, description="Permissions Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 *     @OA\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          @OA\Schema(
 *              type="string"
 *          )
 *     ),
 * )
 */
class FirstPermissionAPIDoc {
} 
