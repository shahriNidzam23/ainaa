<?php

namespace App\APIDoc;

/**
 * @OA\Post(
 *     path="/api/crud/war-product",
 *     tags={"WarProducts"},
 *     summary="Store War Products",
 *     description="Store a War Products into database",
 *     @OA\RequestBody(
 *          required=true,
 *          @OA\JsonContent(ref="#/components/schemas/War Products"),
 *     ),
 *     @OA\Response(response=200, description="War Products Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class CreateWarProductAPIDoc {
}
