<?php

namespace App\APIDoc;

/**
 * @OA\Delete(
 *     path="/api/crud/role/{id}",
 *     tags={"Roles"},
 *     summary="Delete a Roles by ID",
 *     description="Delete Roles",
 *     @OA\Response(response=200, description="Roles Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 *     @OA\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *     ),
 * )
 */
class DeleteRoleAPIDoc {
} 
