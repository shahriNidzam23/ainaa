<?php

namespace App\APIDoc;

/**
 * @OA\Delete(
 *     path="/api/crud/permission/{id}",
 *     tags={"Permissions"},
 *     summary="Delete a Permissions by ID",
 *     description="Delete Permissions",
 *     @OA\Response(response=200, description="Permissions Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 *     @OA\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *     ),
 * )
 */
class DeletePermissionAPIDoc {
} 
