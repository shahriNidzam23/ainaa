<?php

namespace App\APIDoc;

/**
 * @OA\Post(
 *     path="/api/crud/order-list",
 *     tags={"OrderLists"},
 *     summary="Store Order Lists",
 *     description="Store a Order Lists into database",
 *     @OA\RequestBody(
 *          required=true,
 *          @OA\JsonContent(ref="#/components/schemas/Order Lists"),
 *     ),
 *     @OA\Response(response=200, description="Order Lists Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class CreateOrderListAPIDoc {
}
