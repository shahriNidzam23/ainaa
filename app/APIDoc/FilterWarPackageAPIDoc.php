<?php

namespace App\APIDoc;

use OpenApi\Attributes as OA;
use Symfony\Component\HttpFoundation\Response;

/**
 * @OA\Get(
 *     path="/api/crud/war-package/filter",
 *     tags={"WarPackages", "CRUD"},
 *     summary="General filter to get War Packages. Just name the column name",
 *     description="Filter War Packages Data",
 *     @OA\Response(response=200, description="War Packages Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=401, description="Unauthorized", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=403, description="Forbidden", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=422, description="Unprocessable Content", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=500, description="Internal Server Error", @OA\MediaType(mediaType="application/json")),*     @OA\Parameter(
 *          name="product_id",
 *          in="query",
 *          required=true, 
 *          @OA\Schema(
 *              type=""
 *          ),
 *     ),
 *     @OA\Parameter(
 *          name="quantity",
 *          in="query",
 *          required=true, 
 *          @OA\Schema(
 *              type="integer"
 *          ),
 *     ),
 *     @OA\Parameter(
 *          name="name",
 *          in="query",
 *          required=true, 
 *          @OA\Schema(
 *              type="string"
 *          ),
 *     ),
 *     @OA\Parameter(
 *          name="buying_price",
 *          in="query",
 *          required=true, 
 *          @OA\Schema(
 *              type="decimal"
 *          ),
 *     ),
 * )
 */
class FilterWarPackageAPIDoc {
} 
