<?php

namespace App\APIDoc;

/**
 * @OA\Post(
 *     path="/api/crud/war-asset-outbound",
 *     tags={"WarAssetOutbound"},
 *     summary="Store War Asset Outbound",
 *     description="Store a War Asset Outbound into database",
 *     @OA\RequestBody(
 *          required=true,
 *          @OA\JsonContent(ref="#/components/schemas/War Asset Outbound"),
 *     ),
 *     @OA\Response(response=200, description="War Asset Outbound Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class CreateWarAssetOutboundAPIDoc {
}
