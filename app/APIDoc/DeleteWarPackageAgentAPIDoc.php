<?php

namespace App\APIDoc;

/**
 * @OA\Delete(
 *     path="/api/crud/war-package-agent/{id}",
 *     tags={"WarPackageAgents"},
 *     summary="Delete a War Package Agents by ID",
 *     description="Delete War Package Agents",
 *     @OA\Response(response=200, description="War Package Agents Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 *     @OA\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *     ),
 * )
 */
class DeleteWarPackageAgentAPIDoc {
} 
