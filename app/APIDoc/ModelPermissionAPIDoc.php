<?php

namespace App\APIDoc;

/**
 * @OA\Schema(
 *     schema="Permissions",
 *     description="Model Permissions schema",
  *    required={ "name", "description", "guard_name" },
 *     @OA\Property(
 *          property="name",
 *          description="name",
 *          type="string",
 *          example="string",
 *     ),
 *     @OA\Property(
 *          property="module",
 *          description="module",
 *          type="string",
 *          example="string",
 *     ),
 *     @OA\Property(
 *          property="description",
 *          description="description",
 *          type="string",
 *          example="string",
 *     ),
 *     @OA\Property(
 *          property="guard_name",
 *          description="guard_name",
 *          type="string",
 *          example="string",
 *     ),
 * )
 */
class ModelPermissionAPIDoc {
}
