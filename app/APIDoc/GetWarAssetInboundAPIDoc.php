<?php

namespace App\APIDoc;

/**
 * @OA\Get(
 *     path="/api/crud/war-asset-inbound",
 *     tags={"WarAssetInbound"},
 *     summary="Get War Asset Inbound",
 *     description="Get list of War Asset Inbound",
 *     @OA\Response(response=200, description="War Asset Inbound Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class GetWarAssetInboundAPIDoc {
} 
