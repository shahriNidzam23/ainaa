<?php

namespace App\APIDoc;

/**
 * @OA\Post(
 *     path="/api/crud/war-package",
 *     tags={"WarPackages"},
 *     summary="Store War Packages",
 *     description="Store a War Packages into database",
 *     @OA\RequestBody(
 *          required=true,
 *          @OA\JsonContent(ref="#/components/schemas/War Packages"),
 *     ),
 *     @OA\Response(response=200, description="War Packages Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class CreateWarPackageAPIDoc {
}
