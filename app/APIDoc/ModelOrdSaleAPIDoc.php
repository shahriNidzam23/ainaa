<?php

namespace App\APIDoc;

/**
 * @OA\Schema(
 *     schema="Ord Sales",
 *     description="Model Ord Sales schema",
  *    required={ "user_id", "ref_no" },
 *     @OA\Property(
 *          property="user_id",
 *          description="user_id",
 *          type="string",
 *          example="string",
 *     ),
 *     @OA\Property(
 *          property="ref_no",
 *          description="ref_no",
 *          type="string",
 *          example="string",
 *     ),
 * )
 */
class ModelOrdSaleAPIDoc {
}
