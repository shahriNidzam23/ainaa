<?php

namespace App\APIDoc;

/**
 * @OA\Get(
 *     path="/api/crud/war-transfer-product/{id}",
 *     tags={"WarTransferProducts"},
 *     summary="Get a War Transfer Products by ID",
 *     description="Get War Transfer Products by id",
 *     @OA\Response(response=200, description="War Transfer Products Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 *     @OA\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          @OA\Schema(
 *              type="string"
 *          )
 *     ),
 * )
 */
class FirstWarTransferProductAPIDoc {
} 
