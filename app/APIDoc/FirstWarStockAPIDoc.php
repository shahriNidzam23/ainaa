<?php

namespace App\APIDoc;

/**
 * @OA\Get(
 *     path="/api/crud/war-stock/{id}",
 *     tags={"WarStocks"},
 *     summary="Get a War Stocks by ID",
 *     description="Get War Stocks by id",
 *     @OA\Response(response=200, description="War Stocks Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 *     @OA\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          @OA\Schema(
 *              type="string"
 *          )
 *     ),
 * )
 */
class FirstWarStockAPIDoc {
} 
