<?php

namespace App\APIDoc;

/**
 * @OA\Get(
 *     path="/api/crud/audit",
 *     tags={"Audits"},
 *     summary="Get Audits",
 *     description="Get list of Audits",
 *     @OA\Response(response=200, description="Audits Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class GetAuditAPIDoc {
} 
