<?php

namespace App\APIDoc;

/**
 * @OA\Get(
 *     path="/api/crud/common-notification",
 *     tags={"CommonNotifications"},
 *     summary="Get Common Notifications",
 *     description="Get list of Common Notifications",
 *     @OA\Response(response=200, description="Common Notifications Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class GetCommonNotificationAPIDoc {
} 
