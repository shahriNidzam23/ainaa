<?php

namespace App\APIDoc;

/**
 * @OA\Get(
 *     path="/api/crud/war-price/{id}",
 *     tags={"WarPrices"},
 *     summary="Get a War Prices by ID",
 *     description="Get War Prices by id",
 *     @OA\Response(response=200, description="War Prices Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 *     @OA\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          @OA\Schema(
 *              type="string"
 *          )
 *     ),
 * )
 */
class FirstWarPriceAPIDoc {
} 
