<?php

namespace App\APIDoc;

/**
 * @OA\Patch(
 *     path="/api/crud/war-price/{id}",
 *     tags={"WarPrices"},
 *     summary="Update a War Prices by ID",
 *     description="Update War Prices",
 *     @OA\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          @OA\Schema(type="string")
 *     ),
 *     @OA\RequestBody(
 *          required=true,
 *          @OA\JsonContent(ref="#/components/schemas/War Prices"),
 *     ),
 *     @OA\Response(response=200, description="War Prices Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class UpdateWarPriceAPIDoc {
}
