<?php

namespace App\APIDoc;

/**
 * @OA\Schema(
 *     schema="Cms Payment Policy",
 *     description="Model Cms Payment Policy schema",
  *    required={ "template" },
 *     @OA\Property(
 *          property="template",
 *          description="template",
 *          type="string",
 *          example="text",
 *     ),
 * )
 */
class ModelCmsPaymentPolicyAPIDoc {
}
