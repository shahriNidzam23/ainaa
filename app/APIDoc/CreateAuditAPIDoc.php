<?php

namespace App\APIDoc;

/**
 * @OA\Post(
 *     path="/api/crud/audit",
 *     tags={"Audits"},
 *     summary="Store Audits",
 *     description="Store a Audits into database",
 *     @OA\RequestBody(
 *          required=true,
 *          @OA\JsonContent(ref="#/components/schemas/Audits"),
 *     ),
 *     @OA\Response(response=200, description="Audits Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class CreateAuditAPIDoc {
}
