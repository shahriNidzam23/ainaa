<?php

namespace App\APIDoc;

/**
 * @OA\Patch(
 *     path="/api/crud/war-asset-inbound/{id}",
 *     tags={"WarAssetInbound"},
 *     summary="Update a War Asset Inbound by ID",
 *     description="Update War Asset Inbound",
 *     @OA\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          @OA\Schema(type="string")
 *     ),
 *     @OA\RequestBody(
 *          required=true,
 *          @OA\JsonContent(ref="#/components/schemas/War Asset Inbound"),
 *     ),
 *     @OA\Response(response=200, description="War Asset Inbound Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class UpdateWarAssetInboundAPIDoc {
}
