<?php

namespace App\APIDoc;

/**
 * @OA\Patch(
 *     path="/api/crud/common-notification/{id}",
 *     tags={"CommonNotifications"},
 *     summary="Update a Common Notifications by ID",
 *     description="Update Common Notifications",
 *     @OA\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          @OA\Schema(type="string")
 *     ),
 *     @OA\RequestBody(
 *          required=true,
 *          @OA\JsonContent(ref="#/components/schemas/Common Notifications"),
 *     ),
 *     @OA\Response(response=200, description="Common Notifications Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class UpdateCommonNotificationAPIDoc {
}
