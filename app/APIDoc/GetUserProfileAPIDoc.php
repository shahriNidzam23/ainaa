<?php

namespace App\APIDoc;

/**
 * @OA\Get(
 *     path="/api/crud/user-profile",
 *     tags={"UserProfiles"},
 *     summary="Get User Profiles",
 *     description="Get list of User Profiles",
 *     @OA\Response(response=200, description="User Profiles Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class GetUserProfileAPIDoc {
} 
