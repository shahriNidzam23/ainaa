<?php

namespace App\APIDoc;

/**
 * @OA\Schema(
 *     schema="Ord Taxes",
 *     description="Model Ord Taxes schema",
  *    required={ "parent_type", "parent_id", "name", "value" },
 *     @OA\Property(
 *          property="parent_type",
 *          description="parent_type",
 *          type="string",
 *          example="string",
 *     ),
 *     @OA\Property(
 *          property="parent_id",
 *          description="parent_id",
 *          type="string",
 *          example="string",
 *     ),
 *     @OA\Property(
 *          property="name",
 *          description="name",
 *          type="string",
 *          example="string",
 *     ),
 *     @OA\Property(
 *          property="value",
 *          description="value",
 *          type="float",
 *          example="float",
 *     ),
 * )
 */
class ModelOrdTaxAPIDoc {
}
