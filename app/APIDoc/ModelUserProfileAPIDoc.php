<?php

namespace App\APIDoc;

/**
 * @OA\Schema(
 *     schema="User Profiles",
 *     description="Model User Profiles schema",
  *    required={ "user_id", "fullname" },
 *     @OA\Property(
 *          property="user_id",
 *          description="user_id",
 *          type="string",
 *          example="string",
 *     ),
 *     @OA\Property(
 *          property="fullname",
 *          description="fullname",
 *          type="string",
 *          example="string",
 *     ),
 *     @OA\Property(
 *          property="phone_number",
 *          description="phone_number",
 *          type="string",
 *          example="string",
 *     ),
 *     @OA\Property(
 *          property="photo_url",
 *          description="photo_url",
 *          type="string",
 *          example="string",
 *     ),
 * )
 */
class ModelUserProfileAPIDoc {
}
