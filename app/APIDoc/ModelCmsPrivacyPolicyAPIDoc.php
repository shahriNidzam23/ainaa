<?php

namespace App\APIDoc;

/**
 * @OA\Schema(
 *     schema="Cms Privacy Policy",
 *     description="Model Cms Privacy Policy schema",
  *    required={ "content" },
 *     @OA\Property(
 *          property="content",
 *          description="content",
 *          type="string",
 *          example="text",
 *     ),
 * )
 */
class ModelCmsPrivacyPolicyAPIDoc {
}
