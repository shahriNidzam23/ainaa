<?php

namespace App\APIDoc;

/**
 * @OA\Schema(
 *     schema="Order Lists",
 *     description="Model Order Lists schema",
 *     @OA\Property(
 *          property="name",
 *          description="name",
 *          type="string",
 *          example="string",
 *     ),
 * )
 */
class ModelOrderListAPIDoc {
}
