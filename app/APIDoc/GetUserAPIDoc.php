<?php

namespace App\APIDoc;

/**
 * @OA\Get(
 *     path="/api/crud/user",
 *     tags={"Users"},
 *     summary="Get Users",
 *     description="Get list of Users",
 *     @OA\Response(response=200, description="Users Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class GetUserAPIDoc {
} 
