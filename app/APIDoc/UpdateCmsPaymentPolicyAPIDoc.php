<?php

namespace App\APIDoc;

/**
 * @OA\Patch(
 *     path="/api/crud/cms-payment-policy/{id}",
 *     tags={"CmsPaymentPolicy"},
 *     summary="Update a Cms Payment Policy by ID",
 *     description="Update Cms Payment Policy",
 *     @OA\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          @OA\Schema(type="string")
 *     ),
 *     @OA\RequestBody(
 *          required=true,
 *          @OA\JsonContent(ref="#/components/schemas/Cms Payment Policy"),
 *     ),
 *     @OA\Response(response=200, description="Cms Payment Policy Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class UpdateCmsPaymentPolicyAPIDoc {
}
