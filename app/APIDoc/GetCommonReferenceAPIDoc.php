<?php

namespace App\APIDoc;

/**
 * @OA\Get(
 *     path="/api/crud/common-reference",
 *     tags={"CommonReferences"},
 *     summary="Get Common References",
 *     description="Get list of Common References",
 *     @OA\Response(response=200, description="Common References Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class GetCommonReferenceAPIDoc {
} 
