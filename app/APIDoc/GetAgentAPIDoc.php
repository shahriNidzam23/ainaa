<?php

namespace App\APIDoc;

/**
 * @OA\Get(
 *     path="/api/crud/agent",
 *     tags={"Agents"},
 *     summary="Get Agents",
 *     description="Get list of Agents",
 *     @OA\Response(response=200, description="Agents Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class GetAgentAPIDoc {
} 
