<?php

namespace App\APIDoc;

/**
 * @OA\Get(
 *     path="/api/crud/audit/{id}",
 *     tags={"Audits"},
 *     summary="Get a Audits by ID",
 *     description="Get Audits by id",
 *     @OA\Response(response=200, description="Audits Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 *     @OA\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          @OA\Schema(
 *              type="string"
 *          )
 *     ),
 * )
 */
class FirstAuditAPIDoc {
} 
