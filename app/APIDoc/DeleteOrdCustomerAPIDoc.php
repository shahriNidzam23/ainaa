<?php

namespace App\APIDoc;

/**
 * @OA\Delete(
 *     path="/api/crud/ord-customer/{id}",
 *     tags={"OrdCustomers"},
 *     summary="Delete a Ord Customers by ID",
 *     description="Delete Ord Customers",
 *     @OA\Response(response=200, description="Ord Customers Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 *     @OA\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *     ),
 * )
 */
class DeleteOrdCustomerAPIDoc {
} 
