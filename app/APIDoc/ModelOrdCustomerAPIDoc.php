<?php

namespace App\APIDoc;

/**
 * @OA\Schema(
 *     schema="Ord Customers",
 *     description="Model Ord Customers schema",
  *    required={ "sale_id", "name", "phone_number" },
 *     @OA\Property(
 *          property="sale_id",
 *          description="sale_id",
 *          type="string",
 *          example="string",
 *     ),
 *     @OA\Property(
 *          property="name",
 *          description="name",
 *          type="string",
 *          example="string",
 *     ),
 *     @OA\Property(
 *          property="phone_number",
 *          description="phone_number",
 *          type="string",
 *          example="string",
 *     ),
 *     @OA\Property(
 *          property="email",
 *          description="email",
 *          type="string",
 *          example="string",
 *     ),
 * )
 */
class ModelOrdCustomerAPIDoc {
}
