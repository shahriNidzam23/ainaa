<?php

namespace App\APIDoc;

/**
 * @OA\Get(
 *     path="/api/crud/order-list",
 *     tags={"OrderLists"},
 *     summary="Get Order Lists",
 *     description="Get list of Order Lists",
 *     @OA\Response(response=200, description="Order Lists Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class GetOrderListAPIDoc {
} 
