<?php

namespace App\APIDoc;

/**
 * @OA\Delete(
 *     path="/api/crud/war-stock/{id}",
 *     tags={"WarStocks"},
 *     summary="Delete a War Stocks by ID",
 *     description="Delete War Stocks",
 *     @OA\Response(response=200, description="War Stocks Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 *     @OA\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *     ),
 * )
 */
class DeleteWarStockAPIDoc {
} 
