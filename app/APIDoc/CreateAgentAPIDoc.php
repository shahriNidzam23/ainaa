<?php

namespace App\APIDoc;

/**
 * @OA\Post(
 *     path="/api/crud/agent",
 *     tags={"Agents"},
 *     summary="Store Agents",
 *     description="Store a Agents into database",
 *     @OA\RequestBody(
 *          required=true,
 *          @OA\JsonContent(ref="#/components/schemas/Agents"),
 *     ),
 *     @OA\Response(response=200, description="Agents Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class CreateAgentAPIDoc {
}
