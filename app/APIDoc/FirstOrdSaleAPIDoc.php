<?php

namespace App\APIDoc;

/**
 * @OA\Get(
 *     path="/api/crud/ord-sale/{id}",
 *     tags={"OrdSales"},
 *     summary="Get a Ord Sales by ID",
 *     description="Get Ord Sales by id",
 *     @OA\Response(response=200, description="Ord Sales Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 *     @OA\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          @OA\Schema(
 *              type="string"
 *          )
 *     ),
 * )
 */
class FirstOrdSaleAPIDoc {
} 
