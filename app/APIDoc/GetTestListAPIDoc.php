<?php

namespace App\APIDoc;

/**
 * @OA\Get(
 *     path="/api/crud/test-list",
 *     tags={"TestLists"},
 *     summary="Get Test Lists",
 *     description="Get list of Test Lists",
 *     @OA\Response(response=200, description="Test Lists Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class GetTestListAPIDoc {
} 
