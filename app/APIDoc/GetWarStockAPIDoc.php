<?php

namespace App\APIDoc;

/**
 * @OA\Get(
 *     path="/api/crud/war-stock",
 *     tags={"WarStocks"},
 *     summary="Get War Stocks",
 *     description="Get list of War Stocks",
 *     @OA\Response(response=200, description="War Stocks Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class GetWarStockAPIDoc {
} 
