<?php

namespace App\APIDoc;

/**
 * @OA\Post(
 *     path="/api/crud/common-refno",
 *     tags={"CommonRefnos"},
 *     summary="Store Common Refnos",
 *     description="Store a Common Refnos into database",
 *     @OA\RequestBody(
 *          required=true,
 *          @OA\JsonContent(ref="#/components/schemas/Common Refnos"),
 *     ),
 *     @OA\Response(response=200, description="Common Refnos Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class CreateCommonRefnoAPIDoc {
}
