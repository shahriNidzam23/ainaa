<?php

namespace App\APIDoc;

/**
 * @OA\Get(
 *     path="/api/crud/cms-payment-policy",
 *     tags={"CmsPaymentPolicy"},
 *     summary="Get Cms Payment Policy",
 *     description="Get list of Cms Payment Policy",
 *     @OA\Response(response=200, description="Cms Payment Policy Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class GetCmsPaymentPolicyAPIDoc {
} 
