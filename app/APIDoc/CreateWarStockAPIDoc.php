<?php

namespace App\APIDoc;

/**
 * @OA\Post(
 *     path="/api/crud/war-stock",
 *     tags={"WarStocks"},
 *     summary="Store War Stocks",
 *     description="Store a War Stocks into database",
 *     @OA\RequestBody(
 *          required=true,
 *          @OA\JsonContent(ref="#/components/schemas/War Stocks"),
 *     ),
 *     @OA\Response(response=200, description="War Stocks Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class CreateWarStockAPIDoc {
}
