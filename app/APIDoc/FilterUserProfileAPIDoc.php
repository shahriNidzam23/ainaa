<?php

namespace App\APIDoc;

use OpenApi\Attributes as OA;
use Symfony\Component\HttpFoundation\Response;

/**
 * @OA\Get(
 *     path="/api/crud/user-profile/filter",
 *     tags={"UserProfiles", "CRUD"},
 *     summary="General filter to get User Profiles. Just name the column name",
 *     description="Filter User Profiles Data",
 *     @OA\Response(response=200, description="User Profiles Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=401, description="Unauthorized", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=403, description="Forbidden", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=422, description="Unprocessable Content", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=500, description="Internal Server Error", @OA\MediaType(mediaType="application/json")),*     @OA\Parameter(
 *          name="user_id",
 *          in="query",
 *          required=true, 
 *          @OA\Schema(
 *              type=""
 *          ),
 *     ),
 *     @OA\Parameter(
 *          name="fullname",
 *          in="query",
 *          required=true, 
 *          @OA\Schema(
 *              type="string"
 *          ),
 *     ),
 *     @OA\Parameter(
 *          name="phone_number",
 *          in="query",
 *          required=false, 
 *          @OA\Schema(
 *              type="string"
 *          ),
 *     ),
 *     @OA\Parameter(
 *          name="photo_url",
 *          in="query",
 *          required=false, 
 *          @OA\Schema(
 *              type="string"
 *          ),
 *     ),
 * )
 */
class FilterUserProfileAPIDoc {
} 
