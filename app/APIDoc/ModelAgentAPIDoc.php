<?php

namespace App\APIDoc;

/**
 * @OA\Schema(
 *     schema="Agents",
 *     description="Model Agents schema",
  *    required={ "user_id", "upline_id" },
 *     @OA\Property(
 *          property="user_id",
 *          description="user_id",
 *          type="string",
 *          example="string",
 *     ),
 *     @OA\Property(
 *          property="upline_id",
 *          description="upline_id",
 *          type="string",
 *          example="string",
 *     ),
 * )
 */
class ModelAgentAPIDoc {
}
