<?php

namespace App\APIDoc;

/**
 * @OA\Patch(
 *     path="/api/crud/war-package-agent/{id}",
 *     tags={"WarPackageAgents"},
 *     summary="Update a War Package Agents by ID",
 *     description="Update War Package Agents",
 *     @OA\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          @OA\Schema(type="string")
 *     ),
 *     @OA\RequestBody(
 *          required=true,
 *          @OA\JsonContent(ref="#/components/schemas/War Package Agents"),
 *     ),
 *     @OA\Response(response=200, description="War Package Agents Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class UpdateWarPackageAgentAPIDoc {
}
