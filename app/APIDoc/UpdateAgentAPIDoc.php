<?php

namespace App\APIDoc;

/**
 * @OA\Patch(
 *     path="/api/crud/agent/{id}",
 *     tags={"Agents"},
 *     summary="Update a Agents by ID",
 *     description="Update Agents",
 *     @OA\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          @OA\Schema(type="string")
 *     ),
 *     @OA\RequestBody(
 *          required=true,
 *          @OA\JsonContent(ref="#/components/schemas/Agents"),
 *     ),
 *     @OA\Response(response=200, description="Agents Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class UpdateAgentAPIDoc {
}
