<?php

namespace App\APIDoc;

/**
 * @OA\Get(
 *     path="/api/crud/user-profile/{id}",
 *     tags={"UserProfiles"},
 *     summary="Get a User Profiles by ID",
 *     description="Get User Profiles by id",
 *     @OA\Response(response=200, description="User Profiles Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 *     @OA\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          @OA\Schema(
 *              type="string"
 *          )
 *     ),
 * )
 */
class FirstUserProfileAPIDoc {
} 
