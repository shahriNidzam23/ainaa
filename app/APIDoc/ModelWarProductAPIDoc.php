<?php

namespace App\APIDoc;

/**
 * @OA\Schema(
 *     schema="War Products",
 *     description="Model War Products schema",
  *    required={ "name", "sku", "quantity" },
 *     @OA\Property(
 *          property="name",
 *          description="name",
 *          type="string",
 *          example="string",
 *     ),
 *     @OA\Property(
 *          property="sku",
 *          description="sku",
 *          type="string",
 *          example="string",
 *     ),
 *     @OA\Property(
 *          property="quantity",
 *          description="quantity",
 *          type="int64",
 *          example="1",
 *     ),
 * )
 */
class ModelWarProductAPIDoc {
}
