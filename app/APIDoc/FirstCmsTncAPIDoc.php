<?php

namespace App\APIDoc;

/**
 * @OA\Get(
 *     path="/api/crud/cms-tnc/{id}",
 *     tags={"CmsTnc"},
 *     summary="Get a Cms Tnc by ID",
 *     description="Get Cms Tnc by id",
 *     @OA\Response(response=200, description="Cms Tnc Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 *     @OA\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          @OA\Schema(
 *              type="string"
 *          )
 *     ),
 * )
 */
class FirstCmsTncAPIDoc {
} 
