<?php

namespace App\APIDoc;

/**
 * @OA\Schema(
 *     schema="Ord Orders",
 *     description="Model Ord Orders schema",
  *    required={ "seller_id", "buyer_id", "payment_type", "status", "ref_no" },
 *     @OA\Property(
 *          property="seller_id",
 *          description="seller_id",
 *          type="string",
 *          example="string",
 *     ),
 *     @OA\Property(
 *          property="buyer_id",
 *          description="buyer_id",
 *          type="string",
 *          example="string",
 *     ),
 *     @OA\Property(
 *          property="payment_type",
 *          description="payment_type",
 *          type="string",
 *          example="string",
 *     ),
 *     @OA\Property(
 *          property="status",
 *          description="status",
 *          type="string",
 *          example="string",
 *     ),
 *     @OA\Property(
 *          property="ref_no",
 *          description="ref_no",
 *          type="string",
 *          example="string",
 *     ),
 *     @OA\Property(
 *          property="notes",
 *          description="notes",
 *          type="string",
 *          example="string",
 *     ),
 * )
 */
class ModelOrdOrderAPIDoc {
}
