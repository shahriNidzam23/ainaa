<?php

namespace App\APIDoc;

/**
 * @OA\Delete(
 *     path="/api/crud/order-list/{id}",
 *     tags={"OrderLists"},
 *     summary="Delete a Order Lists by ID",
 *     description="Delete Order Lists",
 *     @OA\Response(response=200, description="Order Lists Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 *     @OA\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *     ),
 * )
 */
class DeleteOrderListAPIDoc {
} 
