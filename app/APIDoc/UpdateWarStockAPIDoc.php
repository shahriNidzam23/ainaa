<?php

namespace App\APIDoc;

/**
 * @OA\Patch(
 *     path="/api/crud/war-stock/{id}",
 *     tags={"WarStocks"},
 *     summary="Update a War Stocks by ID",
 *     description="Update War Stocks",
 *     @OA\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          @OA\Schema(type="string")
 *     ),
 *     @OA\RequestBody(
 *          required=true,
 *          @OA\JsonContent(ref="#/components/schemas/War Stocks"),
 *     ),
 *     @OA\Response(response=200, description="War Stocks Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class UpdateWarStockAPIDoc {
}
