<?php

namespace App\APIDoc;

/**
 * @OA\Schema(
 *     schema="Roles",
 *     description="Model Roles schema",
  *    required={ "name", "description", "guard_name" },
 *     @OA\Property(
 *          property="name",
 *          description="name",
 *          type="string",
 *          example="string",
 *     ),
 *     @OA\Property(
 *          property="description",
 *          description="description",
 *          type="string",
 *          example="string",
 *     ),
 *     @OA\Property(
 *          property="guard_name",
 *          description="guard_name",
 *          type="string",
 *          example="string",
 *     ),
 * )
 */
class ModelRoleAPIDoc {
}
