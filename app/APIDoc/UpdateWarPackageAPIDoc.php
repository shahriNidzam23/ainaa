<?php

namespace App\APIDoc;

/**
 * @OA\Patch(
 *     path="/api/crud/war-package/{id}",
 *     tags={"WarPackages"},
 *     summary="Update a War Packages by ID",
 *     description="Update War Packages",
 *     @OA\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          @OA\Schema(type="string")
 *     ),
 *     @OA\RequestBody(
 *          required=true,
 *          @OA\JsonContent(ref="#/components/schemas/War Packages"),
 *     ),
 *     @OA\Response(response=200, description="War Packages Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class UpdateWarPackageAPIDoc {
}
