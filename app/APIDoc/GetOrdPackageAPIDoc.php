<?php

namespace App\APIDoc;

/**
 * @OA\Get(
 *     path="/api/crud/ord-package",
 *     tags={"OrdPackages"},
 *     summary="Get Ord Packages",
 *     description="Get list of Ord Packages",
 *     @OA\Response(response=200, description="Ord Packages Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class GetOrdPackageAPIDoc {
} 
