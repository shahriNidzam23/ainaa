<?php

namespace App\APIDoc;

/**
 * @OA\Get(
 *     path="/api/crud/war-package/{id}",
 *     tags={"WarPackages"},
 *     summary="Get a War Packages by ID",
 *     description="Get War Packages by id",
 *     @OA\Response(response=200, description="War Packages Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 *     @OA\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          @OA\Schema(
 *              type="string"
 *          )
 *     ),
 * )
 */
class FirstWarPackageAPIDoc {
} 
