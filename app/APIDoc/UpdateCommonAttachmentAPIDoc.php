<?php

namespace App\APIDoc;

/**
 * @OA\Patch(
 *     path="/api/crud/common-attachment/{id}",
 *     tags={"CommonAttachments"},
 *     summary="Update a Common Attachments by ID",
 *     description="Update Common Attachments",
 *     @OA\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          @OA\Schema(type="string")
 *     ),
 *     @OA\RequestBody(
 *          required=true,
 *          @OA\JsonContent(ref="#/components/schemas/Common Attachments"),
 *     ),
 *     @OA\Response(response=200, description="Common Attachments Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class UpdateCommonAttachmentAPIDoc {
}
