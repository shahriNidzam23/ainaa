<?php

namespace App\APIDoc;

/**
 * @OA\Get(
 *     path="/api/crud/cms-privacy-policy",
 *     tags={"CmsPrivacyPolicy"},
 *     summary="Get Cms Privacy Policy",
 *     description="Get list of Cms Privacy Policy",
 *     @OA\Response(response=200, description="Cms Privacy Policy Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class GetCmsPrivacyPolicyAPIDoc {
} 
