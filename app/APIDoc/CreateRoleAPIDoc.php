<?php

namespace App\APIDoc;

/**
 * @OA\Post(
 *     path="/api/crud/role",
 *     tags={"Roles"},
 *     summary="Store Roles",
 *     description="Store a Roles into database",
 *     @OA\RequestBody(
 *          required=true,
 *          @OA\JsonContent(ref="#/components/schemas/Roles"),
 *     ),
 *     @OA\Response(response=200, description="Roles Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class CreateRoleAPIDoc {
}
