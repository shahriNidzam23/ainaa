<?php

namespace App\APIDoc;

/**
 * @OA\Schema(
 *     schema="Common Refnos",
 *     description="Model Common Refnos schema",
  *    required={ "name", "prefix", "running_no" },
 *     @OA\Property(
 *          property="name",
 *          description="name",
 *          type="string",
 *          example="string",
 *     ),
 *     @OA\Property(
 *          property="prefix",
 *          description="prefix",
 *          type="string",
 *          example="string",
 *     ),
 *     @OA\Property(
 *          property="running_no",
 *          description="running_no",
 *          type="int64",
 *          example="1",
 *     ),
 * )
 */
class ModelCommonRefnoAPIDoc {
}
