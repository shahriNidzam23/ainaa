<?php

namespace App\APIDoc;

/**
 * @OA\Schema(
 *     schema="Ord Packages",
 *     description="Model Ord Packages schema",
  *    required={ "order_id", "name", "quantity", "buying_price" },
 *     @OA\Property(
 *          property="order_id",
 *          description="order_id",
 *          type="string",
 *          example="string",
 *     ),
 *     @OA\Property(
 *          property="name",
 *          description="name",
 *          type="string",
 *          example="string",
 *     ),
 *     @OA\Property(
 *          property="quantity",
 *          description="quantity",
 *          type="int64",
 *          example="1",
 *     ),
 *     @OA\Property(
 *          property="buying_price",
 *          description="buying_price",
 *          type="decimal",
 *          example="decimal",
 *     ),
 * )
 */
class ModelOrdPackageAPIDoc {
}
