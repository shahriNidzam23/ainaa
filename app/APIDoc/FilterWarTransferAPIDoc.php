<?php

namespace App\APIDoc;

use OpenApi\Attributes as OA;
use Symfony\Component\HttpFoundation\Response;

/**
 * @OA\Get(
 *     path="/api/crud/war-transfer/filter",
 *     tags={"WarTransfers", "CRUD"},
 *     summary="General filter to get War Transfers. Just name the column name",
 *     description="Filter War Transfers Data",
 *     @OA\Response(response=200, description="War Transfers Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=401, description="Unauthorized", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=403, description="Forbidden", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=422, description="Unprocessable Content", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=500, description="Internal Server Error", @OA\MediaType(mediaType="application/json")),*     @OA\Parameter(
 *          name="user_id",
 *          in="query",
 *          required=true, 
 *          @OA\Schema(
 *              type=""
 *          ),
 *     ),
 *     @OA\Parameter(
 *          name="batch_no",
 *          in="query",
 *          required=true, 
 *          @OA\Schema(
 *              type="string"
 *          ),
 *     ),
 *     @OA\Parameter(
 *          name="arrival",
 *          in="query",
 *          required=true, 
 *          @OA\Schema(
 *              type="date_format:Y-m-d H:i"
 *          ),
 *     ),
 * )
 */
class FilterWarTransferAPIDoc {
} 
