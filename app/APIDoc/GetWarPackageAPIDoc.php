<?php

namespace App\APIDoc;

/**
 * @OA\Get(
 *     path="/api/crud/war-package",
 *     tags={"WarPackages"},
 *     summary="Get War Packages",
 *     description="Get list of War Packages",
 *     @OA\Response(response=200, description="War Packages Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class GetWarPackageAPIDoc {
} 
