<?php

namespace App\APIDoc;

/**
 * @OA\Post(
 *     path="/api/crud/war-package-agent",
 *     tags={"WarPackageAgents"},
 *     summary="Store War Package Agents",
 *     description="Store a War Package Agents into database",
 *     @OA\RequestBody(
 *          required=true,
 *          @OA\JsonContent(ref="#/components/schemas/War Package Agents"),
 *     ),
 *     @OA\Response(response=200, description="War Package Agents Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class CreateWarPackageAgentAPIDoc {
}
