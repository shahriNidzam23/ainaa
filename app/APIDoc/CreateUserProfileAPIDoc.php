<?php

namespace App\APIDoc;

/**
 * @OA\Post(
 *     path="/api/crud/user-profile",
 *     tags={"UserProfiles"},
 *     summary="Store User Profiles",
 *     description="Store a User Profiles into database",
 *     @OA\RequestBody(
 *          required=true,
 *          @OA\JsonContent(ref="#/components/schemas/User Profiles"),
 *     ),
 *     @OA\Response(response=200, description="User Profiles Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class CreateUserProfileAPIDoc {
}
