<?php

namespace App\APIDoc;

/**
 * @OA\Schema(
 *     schema="War Asset Inbound",
 *     description="Model War Asset Inbound schema",
  *    required={ "product_id", "manufacturer", "batch_id", "receive_quantity", "rejected_quantity", "accepted_quantity" },
 *     @OA\Property(
 *          property="product_id",
 *          description="product_id",
 *          type="string",
 *          example="string",
 *     ),
 *     @OA\Property(
 *          property="manufacturer",
 *          description="manufacturer",
 *          type="string",
 *          example="string",
 *     ),
 *     @OA\Property(
 *          property="batch_id",
 *          description="batch_id",
 *          type="int64",
 *          example="1",
 *     ),
 *     @OA\Property(
 *          property="receive_quantity",
 *          description="receive_quantity",
 *          type="int64",
 *          example="1",
 *     ),
 *     @OA\Property(
 *          property="rejected_quantity",
 *          description="rejected_quantity",
 *          type="int64",
 *          example="1",
 *     ),
 *     @OA\Property(
 *          property="accepted_quantity",
 *          description="accepted_quantity",
 *          type="int64",
 *          example="1",
 *     ),
 * )
 */
class ModelWarAssetInboundAPIDoc {
}
