<?php

namespace App\APIDoc;

/**
 * @OA\Schema(
 *     schema="War Asset Outbound",
 *     description="Model War Asset Outbound schema",
  *    required={ "product_id", "manufacturer", "batch_id", "stockist_id", "quantity" },
 *     @OA\Property(
 *          property="product_id",
 *          description="product_id",
 *          type="string",
 *          example="string",
 *     ),
 *     @OA\Property(
 *          property="manufacturer",
 *          description="manufacturer",
 *          type="string",
 *          example="string",
 *     ),
 *     @OA\Property(
 *          property="batch_id",
 *          description="batch_id",
 *          type="int64",
 *          example="1",
 *     ),
 *     @OA\Property(
 *          property="stockist_id",
 *          description="stockist_id",
 *          type="int64",
 *          example="1",
 *     ),
 *     @OA\Property(
 *          property="quantity",
 *          description="quantity",
 *          type="int64",
 *          example="1",
 *     ),
 * )
 */
class ModelWarAssetOutboundAPIDoc {
}
