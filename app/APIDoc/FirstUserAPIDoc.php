<?php

namespace App\APIDoc;

/**
 * @OA\Get(
 *     path="/api/crud/user/{id}",
 *     tags={"Users"},
 *     summary="Get a Users by ID",
 *     description="Get Users by id",
 *     @OA\Response(response=200, description="Users Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 *     @OA\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          @OA\Schema(
 *              type="string"
 *          )
 *     ),
 * )
 */
class FirstUserAPIDoc {
} 
