<?php

namespace App\APIDoc;

/**
 * @OA\Schema(
 *     schema="War Transfers",
 *     description="Model War Transfers schema",
  *    required={ "user_id", "batch_no", "arrival" },
 *     @OA\Property(
 *          property="user_id",
 *          description="user_id",
 *          type="string",
 *          example="string",
 *     ),
 *     @OA\Property(
 *          property="batch_no",
 *          description="batch_no",
 *          type="string",
 *          example="string",
 *     ),
 *     @OA\Property(
 *          property="arrival",
 *          description="arrival",
 *          type="datetime",
 *          example="2020-01-01 13:00",
 *     ),
 * )
 */
class ModelWarTransferAPIDoc {
}
