<?php

namespace App\APIDoc;

/**
 * @OA\Patch(
 *     path="/api/crud/war-asset-outbound/{id}",
 *     tags={"WarAssetOutbound"},
 *     summary="Update a War Asset Outbound by ID",
 *     description="Update War Asset Outbound",
 *     @OA\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          @OA\Schema(type="string")
 *     ),
 *     @OA\RequestBody(
 *          required=true,
 *          @OA\JsonContent(ref="#/components/schemas/War Asset Outbound"),
 *     ),
 *     @OA\Response(response=200, description="War Asset Outbound Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class UpdateWarAssetOutboundAPIDoc {
}
