<?php

namespace App\APIDoc;

/**
 * @OA\Get(
 *     path="/api/crud/socmed-post/{id}",
 *     tags={"SocmedPosts"},
 *     summary="Get a Socmed Posts by ID",
 *     description="Get Socmed Posts by id",
 *     @OA\Response(response=200, description="Socmed Posts Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 *     @OA\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          @OA\Schema(
 *              type="string"
 *          )
 *     ),
 * )
 */
class FirstSocmedPostAPIDoc {
} 
