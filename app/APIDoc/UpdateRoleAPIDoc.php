<?php

namespace App\APIDoc;

/**
 * @OA\Patch(
 *     path="/api/crud/role/{id}",
 *     tags={"Roles"},
 *     summary="Update a Roles by ID",
 *     description="Update Roles",
 *     @OA\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          @OA\Schema(type="string")
 *     ),
 *     @OA\RequestBody(
 *          required=true,
 *          @OA\JsonContent(ref="#/components/schemas/Roles"),
 *     ),
 *     @OA\Response(response=200, description="Roles Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class UpdateRoleAPIDoc {
}
