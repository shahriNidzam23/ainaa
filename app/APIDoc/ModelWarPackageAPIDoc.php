<?php

namespace App\APIDoc;

/**
 * @OA\Schema(
 *     schema="War Packages",
 *     description="Model War Packages schema",
  *    required={ "product_id", "quantity", "name", "buying_price" },
 *     @OA\Property(
 *          property="product_id",
 *          description="product_id",
 *          type="string",
 *          example="string",
 *     ),
 *     @OA\Property(
 *          property="quantity",
 *          description="quantity",
 *          type="int64",
 *          example="1",
 *     ),
 *     @OA\Property(
 *          property="name",
 *          description="name",
 *          type="string",
 *          example="string",
 *     ),
 *     @OA\Property(
 *          property="buying_price",
 *          description="buying_price",
 *          type="decimal",
 *          example="decimal",
 *     ),
 * )
 */
class ModelWarPackageAPIDoc {
}
