<?php

namespace App\APIDoc;

/**
 * @OA\Get(
 *     path="/api/crud/war-asset-outbound/{id}",
 *     tags={"WarAssetOutbound"},
 *     summary="Get a War Asset Outbound by ID",
 *     description="Get War Asset Outbound by id",
 *     @OA\Response(response=200, description="War Asset Outbound Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 *     @OA\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          @OA\Schema(
 *              type="string"
 *          )
 *     ),
 * )
 */
class FirstWarAssetOutboundAPIDoc {
} 
