<?php

namespace App\APIDoc;

/**
 * @OA\Delete(
 *     path="/api/crud/war-transfer/{id}",
 *     tags={"WarTransfers"},
 *     summary="Delete a War Transfers by ID",
 *     description="Delete War Transfers",
 *     @OA\Response(response=200, description="War Transfers Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 *     @OA\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *     ),
 * )
 */
class DeleteWarTransferAPIDoc {
} 
