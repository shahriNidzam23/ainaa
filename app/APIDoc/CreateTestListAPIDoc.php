<?php

namespace App\APIDoc;

/**
 * @OA\Post(
 *     path="/api/crud/test-list",
 *     tags={"TestLists"},
 *     summary="Store Test Lists",
 *     description="Store a Test Lists into database",
 *     @OA\RequestBody(
 *          required=true,
 *          @OA\JsonContent(ref="#/components/schemas/Test Lists"),
 *     ),
 *     @OA\Response(response=200, description="Test Lists Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class CreateTestListAPIDoc {
}
