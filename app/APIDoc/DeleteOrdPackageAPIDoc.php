<?php

namespace App\APIDoc;

/**
 * @OA\Delete(
 *     path="/api/crud/ord-package/{id}",
 *     tags={"OrdPackages"},
 *     summary="Delete a Ord Packages by ID",
 *     description="Delete Ord Packages",
 *     @OA\Response(response=200, description="Ord Packages Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 *     @OA\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *     ),
 * )
 */
class DeleteOrdPackageAPIDoc {
} 
