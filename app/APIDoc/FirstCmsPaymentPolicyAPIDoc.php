<?php

namespace App\APIDoc;

/**
 * @OA\Get(
 *     path="/api/crud/cms-payment-policy/{id}",
 *     tags={"CmsPaymentPolicy"},
 *     summary="Get a Cms Payment Policy by ID",
 *     description="Get Cms Payment Policy by id",
 *     @OA\Response(response=200, description="Cms Payment Policy Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 *     @OA\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          @OA\Schema(
 *              type="string"
 *          )
 *     ),
 * )
 */
class FirstCmsPaymentPolicyAPIDoc {
} 
