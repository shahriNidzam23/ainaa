<?php

namespace App\APIDoc;

/**
 * @OA\Get(
 *     path="/api/crud/ord-tax",
 *     tags={"OrdTaxes"},
 *     summary="Get Ord Taxes",
 *     description="Get list of Ord Taxes",
 *     @OA\Response(response=200, description="Ord Taxes Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class GetOrdTaxAPIDoc {
} 
