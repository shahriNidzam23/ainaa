<?php

namespace App\APIDoc;

/**
 * @OA\Get(
 *     path="/api/crud/common-attachment/{id}",
 *     tags={"CommonAttachments"},
 *     summary="Get a Common Attachments by ID",
 *     description="Get Common Attachments by id",
 *     @OA\Response(response=200, description="Common Attachments Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 *     @OA\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          @OA\Schema(
 *              type="string"
 *          )
 *     ),
 * )
 */
class FirstCommonAttachmentAPIDoc {
} 
