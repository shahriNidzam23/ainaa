<?php

namespace App\APIDoc;

/**
 * @OA\Delete(
 *     path="/api/crud/common-attachment/{id}",
 *     tags={"CommonAttachments"},
 *     summary="Delete a Common Attachments by ID",
 *     description="Delete Common Attachments",
 *     @OA\Response(response=200, description="Common Attachments Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 *     @OA\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *     ),
 * )
 */
class DeleteCommonAttachmentAPIDoc {
} 
