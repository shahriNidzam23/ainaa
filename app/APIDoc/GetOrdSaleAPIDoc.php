<?php

namespace App\APIDoc;

/**
 * @OA\Get(
 *     path="/api/crud/ord-sale",
 *     tags={"OrdSales"},
 *     summary="Get Ord Sales",
 *     description="Get list of Ord Sales",
 *     @OA\Response(response=200, description="Ord Sales Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class GetOrdSaleAPIDoc {
} 
