<?php

namespace App\APIDoc;

/**
 * @OA\Get(
 *     path="/api/crud/user-socialmedia",
 *     tags={"UserSocialmedias"},
 *     summary="Get User Socialmedias",
 *     description="Get list of User Socialmedias",
 *     @OA\Response(response=200, description="User Socialmedias Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class GetUserSocialmediaAPIDoc {
} 
