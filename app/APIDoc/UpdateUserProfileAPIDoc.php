<?php

namespace App\APIDoc;

/**
 * @OA\Patch(
 *     path="/api/crud/user-profile/{id}",
 *     tags={"UserProfiles"},
 *     summary="Update a User Profiles by ID",
 *     description="Update User Profiles",
 *     @OA\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          @OA\Schema(type="string")
 *     ),
 *     @OA\RequestBody(
 *          required=true,
 *          @OA\JsonContent(ref="#/components/schemas/User Profiles"),
 *     ),
 *     @OA\Response(response=200, description="User Profiles Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class UpdateUserProfileAPIDoc {
}
