<?php

namespace App\APIDoc;

/**
 * @OA\Delete(
 *     path="/api/crud/user-socialmedia/{id}",
 *     tags={"UserSocialmedias"},
 *     summary="Delete a User Socialmedias by ID",
 *     description="Delete User Socialmedias",
 *     @OA\Response(response=200, description="User Socialmedias Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 *     @OA\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *     ),
 * )
 */
class DeleteUserSocialmediaAPIDoc {
} 
