<?php

namespace App\APIDoc;

/**
 * @OA\Schema(
 *     schema="Socmed Posts",
 *     description="Model Socmed Posts schema",
  *    required={ "url_posting", "social_media", "username" },
 *     @OA\Property(
 *          property="url_posting",
 *          description="url_posting",
 *          type="string",
 *          example="string",
 *     ),
 *     @OA\Property(
 *          property="social_media",
 *          description="social_media",
 *          type="string",
 *          example="string",
 *     ),
 *     @OA\Property(
 *          property="username",
 *          description="username",
 *          type="string",
 *          example="string",
 *     ),
 * )
 */
class ModelSocmedPostAPIDoc {
}
