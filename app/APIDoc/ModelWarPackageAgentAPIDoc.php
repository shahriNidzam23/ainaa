<?php

namespace App\APIDoc;

/**
 * @OA\Schema(
 *     schema="War Package Agents",
 *     description="Model War Package Agents schema",
  *    required={ "package_id", "user_id" },
 *     @OA\Property(
 *          property="package_id",
 *          description="package_id",
 *          type="string",
 *          example="string",
 *     ),
 *     @OA\Property(
 *          property="user_id",
 *          description="user_id",
 *          type="string",
 *          example="string",
 *     ),
 * )
 */
class ModelWarPackageAgentAPIDoc {
}
