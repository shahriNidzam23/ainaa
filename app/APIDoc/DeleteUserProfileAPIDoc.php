<?php

namespace App\APIDoc;

/**
 * @OA\Delete(
 *     path="/api/crud/user-profile/{id}",
 *     tags={"UserProfiles"},
 *     summary="Delete a User Profiles by ID",
 *     description="Delete User Profiles",
 *     @OA\Response(response=200, description="User Profiles Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 *     @OA\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *     ),
 * )
 */
class DeleteUserProfileAPIDoc {
} 
