<?php

namespace App\APIDoc;

/**
 * @OA\Post(
 *     path="/api/crud/war-price",
 *     tags={"WarPrices"},
 *     summary="Store War Prices",
 *     description="Store a War Prices into database",
 *     @OA\RequestBody(
 *          required=true,
 *          @OA\JsonContent(ref="#/components/schemas/War Prices"),
 *     ),
 *     @OA\Response(response=200, description="War Prices Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class CreateWarPriceAPIDoc {
}
