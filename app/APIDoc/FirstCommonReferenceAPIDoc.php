<?php

namespace App\APIDoc;

/**
 * @OA\Get(
 *     path="/api/crud/common-reference/{id}",
 *     tags={"CommonReferences"},
 *     summary="Get a Common References by ID",
 *     description="Get Common References by id",
 *     @OA\Response(response=200, description="Common References Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 *     @OA\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          @OA\Schema(
 *              type="string"
 *          )
 *     ),
 * )
 */
class FirstCommonReferenceAPIDoc {
} 
