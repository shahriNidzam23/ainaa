<?php

namespace App\APIDoc;

/**
 * @OA\Delete(
 *     path="/api/crud/user/{id}",
 *     tags={"Users"},
 *     summary="Delete a Users by ID",
 *     description="Delete Users",
 *     @OA\Response(response=200, description="Users Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 *     @OA\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *     ),
 * )
 */
class DeleteUserAPIDoc {
} 
