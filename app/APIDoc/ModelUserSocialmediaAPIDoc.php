<?php

namespace App\APIDoc;

/**
 * @OA\Schema(
 *     schema="User Socialmedias",
 *     description="Model User Socialmedias schema",
  *    required={ "user_id", "platform" },
 *     @OA\Property(
 *          property="user_id",
 *          description="user_id",
 *          type="string",
 *          example="string",
 *     ),
 *     @OA\Property(
 *          property="platform",
 *          description="platform",
 *          type="string",
 *          example="string",
 *     ),
 *     @OA\Property(
 *          property="username",
 *          description="username",
 *          type="string",
 *          example="string",
 *     ),
 * )
 */
class ModelUserSocialmediaAPIDoc {
}
