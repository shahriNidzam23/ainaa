<?php

namespace App\APIDoc;

/**
 * @OA\Schema(
 *     schema="Common Attachments",
 *     description="Model Common Attachments schema",
  *    required={ "parent_type", "parent_id", "name", "path" },
 *     @OA\Property(
 *          property="parent_type",
 *          description="parent_type",
 *          type="string",
 *          example="string",
 *     ),
 *     @OA\Property(
 *          property="parent_id",
 *          description="parent_id",
 *          type="string",
 *          example="string",
 *     ),
 *     @OA\Property(
 *          property="name",
 *          description="name",
 *          type="string",
 *          example="string",
 *     ),
 *     @OA\Property(
 *          property="path",
 *          description="path",
 *          type="string",
 *          example="string",
 *     ),
 * )
 */
class ModelCommonAttachmentAPIDoc {
}
