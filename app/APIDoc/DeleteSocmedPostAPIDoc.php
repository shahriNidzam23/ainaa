<?php

namespace App\APIDoc;

/**
 * @OA\Delete(
 *     path="/api/crud/socmed-post/{id}",
 *     tags={"SocmedPosts"},
 *     summary="Delete a Socmed Posts by ID",
 *     description="Delete Socmed Posts",
 *     @OA\Response(response=200, description="Socmed Posts Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 *     @OA\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *     ),
 * )
 */
class DeleteSocmedPostAPIDoc {
} 
