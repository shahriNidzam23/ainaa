<?php

namespace App\APIDoc;

/**
 * @OA\Post(
 *     path="/api/crud/socmed-post",
 *     tags={"SocmedPosts"},
 *     summary="Store Socmed Posts",
 *     description="Store a Socmed Posts into database",
 *     @OA\RequestBody(
 *          required=true,
 *          @OA\JsonContent(ref="#/components/schemas/Socmed Posts"),
 *     ),
 *     @OA\Response(response=200, description="Socmed Posts Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class CreateSocmedPostAPIDoc {
}
