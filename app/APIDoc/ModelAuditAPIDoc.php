<?php

namespace App\APIDoc;

/**
 * @OA\Schema(
 *     schema="Audits",
 *     description="Model Audits schema",
  *    required={ "event", "auditable_type", "auditable_id" },
 *     @OA\Property(
 *          property="user_type",
 *          description="user_type",
 *          type="string",
 *          example="string",
 *     ),
 *     @OA\Property(
 *          property="user_id",
 *          description="user_id",
 *          type="string",
 *          example="string",
 *     ),
 *     @OA\Property(
 *          property="event",
 *          description="event",
 *          type="string",
 *          example="string",
 *     ),
 *     @OA\Property(
 *          property="auditable_type",
 *          description="auditable_type",
 *          type="string",
 *          example="string",
 *     ),
 *     @OA\Property(
 *          property="auditable_id",
 *          description="auditable_id",
 *          type="string",
 *          example="string",
 *     ),
 *     @OA\Property(
 *          property="old_values",
 *          description="old_values",
 *          type="string",
 *          example="text",
 *     ),
 *     @OA\Property(
 *          property="new_values",
 *          description="new_values",
 *          type="string",
 *          example="text",
 *     ),
 *     @OA\Property(
 *          property="url",
 *          description="url",
 *          type="string",
 *          example="text",
 *     ),
 *     @OA\Property(
 *          property="ip_address",
 *          description="ip_address",
 *          type="string",
 *          example="string",
 *     ),
 *     @OA\Property(
 *          property="user_agent",
 *          description="user_agent",
 *          type="string",
 *          example="string",
 *     ),
 *     @OA\Property(
 *          property="tags",
 *          description="tags",
 *          type="string",
 *          example="string",
 *     ),
 * )
 */
class ModelAuditAPIDoc {
}
