<?php

namespace App\APIDoc;

/**
 * @OA\Get(
 *     path="/api/crud/war-transfer/{id}",
 *     tags={"WarTransfers"},
 *     summary="Get a War Transfers by ID",
 *     description="Get War Transfers by id",
 *     @OA\Response(response=200, description="War Transfers Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 *     @OA\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          @OA\Schema(
 *              type="string"
 *          )
 *     ),
 * )
 */
class FirstWarTransferAPIDoc {
} 
