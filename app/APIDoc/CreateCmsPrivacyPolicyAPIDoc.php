<?php

namespace App\APIDoc;

/**
 * @OA\Post(
 *     path="/api/crud/cms-privacy-policy",
 *     tags={"CmsPrivacyPolicy"},
 *     summary="Store Cms Privacy Policy",
 *     description="Store a Cms Privacy Policy into database",
 *     @OA\RequestBody(
 *          required=true,
 *          @OA\JsonContent(ref="#/components/schemas/Cms Privacy Policy"),
 *     ),
 *     @OA\Response(response=200, description="Cms Privacy Policy Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class CreateCmsPrivacyPolicyAPIDoc {
}
