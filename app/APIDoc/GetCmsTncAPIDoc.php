<?php

namespace App\APIDoc;

/**
 * @OA\Get(
 *     path="/api/crud/cms-tnc",
 *     tags={"CmsTnc"},
 *     summary="Get Cms Tnc",
 *     description="Get list of Cms Tnc",
 *     @OA\Response(response=200, description="Cms Tnc Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class GetCmsTncAPIDoc {
} 
