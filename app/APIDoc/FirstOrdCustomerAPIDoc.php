<?php

namespace App\APIDoc;

/**
 * @OA\Get(
 *     path="/api/crud/ord-customer/{id}",
 *     tags={"OrdCustomers"},
 *     summary="Get a Ord Customers by ID",
 *     description="Get Ord Customers by id",
 *     @OA\Response(response=200, description="Ord Customers Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 *     @OA\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          @OA\Schema(
 *              type="string"
 *          )
 *     ),
 * )
 */
class FirstOrdCustomerAPIDoc {
} 
