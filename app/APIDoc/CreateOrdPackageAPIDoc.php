<?php

namespace App\APIDoc;

/**
 * @OA\Post(
 *     path="/api/crud/ord-package",
 *     tags={"OrdPackages"},
 *     summary="Store Ord Packages",
 *     description="Store a Ord Packages into database",
 *     @OA\RequestBody(
 *          required=true,
 *          @OA\JsonContent(ref="#/components/schemas/Ord Packages"),
 *     ),
 *     @OA\Response(response=200, description="Ord Packages Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class CreateOrdPackageAPIDoc {
}
