<?php

namespace App\APIDoc;

/**
 * @OA\Get(
 *     path="/api/crud/war-package-agent",
 *     tags={"WarPackageAgents"},
 *     summary="Get War Package Agents",
 *     description="Get list of War Package Agents",
 *     @OA\Response(response=200, description="War Package Agents Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class GetWarPackageAgentAPIDoc {
} 
