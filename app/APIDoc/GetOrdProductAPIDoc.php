<?php

namespace App\APIDoc;

/**
 * @OA\Get(
 *     path="/api/crud/ord-product",
 *     tags={"OrdProducts"},
 *     summary="Get Ord Products",
 *     description="Get list of Ord Products",
 *     @OA\Response(response=200, description="Ord Products Module", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=400, description="Bad request", @OA\MediaType(mediaType="application/json")),
 *     @OA\Response(response=404, description="Resource Not Found", @OA\MediaType(mediaType="application/json")),
 * )
 */
class GetOrdProductAPIDoc {
} 
