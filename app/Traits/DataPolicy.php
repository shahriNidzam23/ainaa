<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Builder;
use Modules\Auth\Models\Role;

trait DataPolicy
{
    private static $bypassDataPolicy = false;
    public static function bootDataPolicy()
    {
        if (app()->runningInConsole()) return;

        self::createDataPolicy();
        self::viewDataPolicy();
        self::updateDataPolicy();
        self::deleteDataPolicy();
    }

    private static function getPermissions($action)
    {
        $user = auth()->user();
        $model = app(get_class());
        $prefix = $model->getTable();

        $permissions = $user->getAllPermissions();
        $roles = Role::with([
            'permissions' => function ($q) use ($prefix) {
                $q->where('name', 'like', $prefix . ':%');
            },
        ])->whereIn('name', $user->getRoleNames())->get();

        foreach ($roles as $key => $role) {
            foreach ($role->permissions as $key => $permission) {
                $permissions[] = $permission;
            }
        }


        $permissions = $permissions->pluck('name')
            ->filter(function ($permission) use ($prefix, $action) {
                return str($permission)->startsWith("$prefix:$action");
            });

        // Set priority: If <PERMISSION>:view found, disregard other permission
        foreach ($permissions as $key => $permission) {
            if ($permission == "$prefix:$action") {
                return collect(["$prefix:$action"]);
            }
        }

        return $permissions;
    }

    private static function getCondition($permissions)
    {

        return $permissions
            ->map(function ($name) {
                $name = str(str($name)->explode('|')[0]);
                if ($name->substrCount(':') > 1) {
                    return $name->afterLast(':')->explode(',');
                }

                return [];
            })
            ->flatten()->unique();
    }

    private static function getColumns($permissions)
    {

        return $permissions
            ->map(function ($name) {
                $name = str($name);
                // $name = str($name);
                if ($name->substrCount('|') > 0) {
                    return $name->afterLast('|')->explode(',');
                }

                return [];
            })
            ->flatten()->unique()
            ->toArray();
    }

    private static function getValue($user, $columnName)
    {
        $col = strpos($columnName, '=') ? explode('=', $columnName) : $columnName;
        $property = is_array($col) ? $col[1] : 'id';

        if (strpos($property, '.')) {
            $relations = explode('.', $property);
            $relative = $user;
            foreach ($relations as $key => $relation) {
                if ($key < count($relations) - 1) {
                    $relative->load($relation);
                }
                $value = $relative->{$relation};
                $relative = $relative->{$relation};
            }
        } else {
            if (empty($user->{$property})) {
                throw new Exception('Property ' . $property . ' not exist (DataPolicy)');
            }
            $value = $user->{$property};
        }

        return $value;
    }

    private static function getBypass()
    {
        return self::$bypassDataPolicy;
    }

    private static function createDataPolicy()
    {
        static::creating(function ($model) {
            if (self::getBypass()) return;
            if (empty(auth()->user())) return abort(401, 'Unauthenticated');

            $user = auth()->user();
            $permissions = self::getPermissions('create');

            if ($permissions->isEmpty()) {
                abort(403, 'User doesnt have create permission');
            }

            $permissions = self::getCondition($permissions);
            // If only <PERMISSION>:create exist, allow create record

            if ($permissions->isEmpty()) {
                return;
            }
            // Query based on column, if not available use ID in table User
            $isFound = true;
            foreach ($permissions as $columnName) {

                $col = strpos($columnName, '=') ? explode('=', $columnName) : $columnName;
                $column = is_array($col) ? $col[0] : $columnName;
                $value = self::getValue($user, $columnName);
                if ($value == $model->{$column}) {
                    $isFound = true;
                }
            }

            if (!$isFound) {
                abort(403, 'User doesnt have update permission');
            }
        });
    }

    private static function viewDataPolicy()
    {
        static::addGlobalScope('data_policy', function (Builder $builder) {

            if (self::getBypass()) return;
            if (empty(auth()->user())) return abort(401, 'Unauthenticated');


            $model = app(get_class());
            $user = auth()->user();
            $permissions = self::getPermissions('view');

            if ($permissions->isEmpty()) {
                $builder->whereRaw('1=0');

                return;
            }

            $conditions = self::getCondition($permissions);
            $columns = self::getColumns($permissions);

            // If only <PERMISSION>:view exist, result based on query
            if ($permissions->isEmpty()) {
                return;
            }

            $builder->where(function ($query) use ($conditions, $user, $model) {
                // Query based on column, if not available use ID in table User
                foreach ($conditions as $columnName) {
                    $col = strpos($columnName, '=') ? explode('=', $columnName) : $columnName;
                    $column = is_array($col) ? $col[0] : $columnName;
                    $value = self::getValue($user, $columnName);
                    $query->orWhere($model->table . '.' . $column, $value);
                }
            });

            if(!empty($columns)) {
                $builder->select($columns);
            }
        });
    }

    private static function updateDataPolicy()
    {
        static::updating(function ($model) {

            if (self::getBypass()) return;
            if (empty(auth()->user())) return abort(401, 'Unauthenticated');

            $user = auth()->user();
            $permissions = self::getPermissions('update');
            $model->withoutGlobalScope('data_policy');
            $originalAttributes = $model->getOriginal();

            if ($permissions->isEmpty()) {
                abort(403, 'User doesnt have update permission');
            }

            $permissions = self::getCondition($permissions);
            $columns = self::getColumns($permissions);
            // If only <PERMISSION>:update exist, allow update record
            if ($permissions->isEmpty()) {
                return;
            }

            // Query based on column, if not available use ID in table User
            $isFound = false;
            foreach ($permissions as $columnName) {

                $col = strpos($columnName, '=') ? explode('=', $columnName) : $columnName;
                $column = is_array($col) ? $col[0] : $columnName;
                $value = self::getValue($user, $columnName);
                if ($value == $originalAttributes[$column]) {
                    $isFound = true;
                }
            }


            if (!$isFound) {
                abort(403, 'User doesnt have update permission');
            }

            if (!empty($columns)) {
                foreach ($model->toArray() as $key => $column) {
                    // Check other columns changes or not, if yes abort
                    if(in_array($key, $columns) || !in_array($key, $model->getFillable())) continue;

                    if ($column != $originalAttributes[$key]) {
                        abort(403, "User doesnt have update column '$key' permission");
                    }
                }
            }
        });
    }

    private static function deleteDataPolicy()
    {
        static::deleting(function ($model) {
            if (self::getBypass()) return;
            if (empty(auth()->user())) return abort(401, 'Unauthenticated');

            $user = auth()->user();
            $permissions = self::getPermissions('delete');

            if ($permissions->isEmpty()) {
                abort(403, 'User doesnt have delete permission');
            }

            $permissions = self::getCondition($permissions);
            // If only <PERMISSION>:update exist, allow update record
            if ($permissions->isEmpty()) {
                return;
            }

            // Query based on column, if not available use ID in table User
            $isFound = false;
            foreach ($permissions as $columnName) {

                $col = strpos($columnName, '=') ? explode('=', $columnName) : $columnName;
                $column = is_array($col) ? $col[0] : $columnName;
                $value = self::getValue($user, $columnName);
                if ($value == $model->{$column}) {
                    $isFound = true;
                }
            }

            if (!$isFound) {
                abort(403, 'User doesnt have update permission');
            }
        });
    }

    public static function canByActionStatic($action, $model, $isThrow = true)
    {

        $user = auth()->user();
        $permissions = self::getPermissions($action);

        if ($permissions->isEmpty()) {
            if ($isThrow) {
                abort(403, "User doesnt have $action permission");
            } else {
                return false;
            }
        }

        $permissions = self::getCondition($permissions);
        // If only <PERMISSION>:update exist, allow update record
        if ($permissions->isEmpty()) {
            return true;
        }

        // Query based on column, if not available use ID in table User
        foreach ($permissions as $columnName) {

            $col = strpos($columnName, '=') ? explode('=', $columnName) : $columnName;
            $column = is_array($col) ? $col[0] : $columnName;
            $value = self::getValue($user, $columnName);
            if ($value != $model->{$column}) {
                if ($isThrow) {
                    abort(403, "User doesnt have $action permission");
                } else {
                    return false;
                }
            }
        }
    }

    public function canByAction($action, $isThrow = true)
    {
        $model = $this;

        return self::canByActionStatic($action, $model, $isThrow);
    }

    public static function bypassDataPolicy($closure)
    {
        self::$bypassDataPolicy = true;
        $ret = $closure();
        self::$bypassDataPolicy = false;
        return $ret;
    }
}
