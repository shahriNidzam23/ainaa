<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\{Model};
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;
use Kirschbaum\PowerJoins\PowerJoins;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use Rekamy\LaravelCore\Traits\GroupedRecord;

class BaseModel extends Model implements AuditableContract
{
    use Auditable, GroupedRecord, HasFactory, HasUuids, PowerJoins;

    protected static function boot()
    {

        static::creating(function ($model) {
            if (empty($model->{$model->getKeyName()})) {
                $model->{$model->getKeyName()} = (string) Str::orderedUuid();
            }

            if (auth()->check() && Schema::hasColumn($model->table, 'created_by')) {
                $model->created_by = auth()->id();
            }

            if (Schema::hasColumn($model->table, 'created_at')) {
                $model->created_at = now();
            }
        });

        static::updating(function ($model) {
            if (auth()->check() && Schema::hasColumn($model->table, 'updated_by')) {
                $model->updated_by = auth()->id();
            }

            if (Schema::hasColumn($model->table, 'updated_at')) {
                $model->updated_at = now();
            }
        });
        // Boot parent last to ensure traits run after BaseModel boot
        parent::boot();

    }
}
