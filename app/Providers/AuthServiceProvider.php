<?php

namespace App\Providers;

// use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Auth\Notifications\VerifyEmail;
use Illuminate\Support\HtmlString;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The model to policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        //
    ];

    /**
     * Register any authentication / authorization services.
     */
    public function boot(): void
    {
        $this->configureVerifyEmail();
        $this->configureResetPassword();
    }

    private function configureVerifyEmail()
    {
        VerifyEmail::toMailUsing(function (object $notifiable, string $url) {
            return (new MailMessage)
                ->greeting("Hello,")
                ->subject('XYZ Login')
                ->line('To login your account, please copy code below and paste to app')
                ->line(new HtmlString('<div style="border: 1px solid #2d3748; border-radius: 7px; padding: 10px; display: inline-block; margin: 10px 12px; width: 95%; word-wrap: break-word;">Code: ' . $notifiable->ott . '</div>'));
        });
    }

    private function configureResetPassword()
    {
        ResetPassword::toMailUsing(function (object $notifiable, string $token) {
            $expiryTime = config('auth.passwords.' . config('auth.defaults.passwords') . '.expire');

            return (new MailMessage)
                ->greeting("Hello,")
                ->subject('Password Reset Request')
                ->line('You are receiving this email because we have received a password reset request for your account.')
                ->line('To reset your password, please follow these steps:')
                ->line('1. Copy the following token:')
                ->line(new HtmlString('<div style="border: 1px solid #2d3748; border-radius: 7px; padding: 10px; display: inline-block; margin: 10px 12px; width: 95%; word-wrap: break-word;">' . $token . '</div>'))
                ->line('2. Paste the token into the ' . config('app.name') . ' Application to continue with the password reset.')
                ->line('This token will expire in ' . $expiryTime . ' minutes.')
                ->line('If you did not request a password reset, no further action is required.');
        });
    }
}
