<?php

namespace App\Providers;

use Illuminate\Auth\Events\{
    Verified,
    Login,
    Logout,
    PasswordReset,
};
use Modules\Auth\Events\{
    Registered,
};
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Modules\Agent\Listeners\{
    RegisterAgent
};
use Modules\Auth\Events\{
    RequestToken,
    SendEmailVerificationNotification
};
use Modules\Order\Events\{
    OrderCreated,
    OrderUpdated,
    OrderDeleted,
    SaleCreated,
    SaleUpdated,
};

use Modules\Warehouse\Listeners\{
    OrderTransferListener,
    SaleTransferListener
};

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event to listener mappings for the application.
     *
     * @var array<class-string, array<int, class-string>>
     */
    protected $listen = [
        Registered::class => [
            RegisterAgent::class
        ],
        RequestToken::class => [
            SendEmailVerificationNotification::class,
        ],
        Verified::class => [],
        Login::class => [],
        Logout::class => [],
        PasswordReset::class => [],
        OrderCreated::class => [
            OrderTransferListener::class
        ],
        OrderUpdated::class => [
            OrderTransferListener::class
        ],
        OrderDeleted::class => [],
        SaleCreated::class => [
            SaleTransferListener::class
        ],
        SaleUpdated::class => [
            SaleTransferListener::class
        ],
    ];

    /**
     * Register any events for your application.
     */
    public function boot(): void
    {
        //
    }

    /**
     * Determine if events and listeners should be automatically discovered.
     */
    public function shouldDiscoverEvents(): bool
    {
        return false;
    }
}
