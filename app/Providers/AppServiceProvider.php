<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Query\Builder;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {

        if ($this->app->environment('production') || $this->app->environment('staging')) {
            URL::forceScheme('https');
        }

        Blueprint::macro('auditable', function () {
            $this->datetime('created_at')->nullable();
            $this->datetime('updated_at')->nullable();
            $this->datetime('deleted_at')->nullable();
            $this->uuid('created_by')->nullable();
            $this->uuid('updated_by')->nullable();
            $this->uuid('deleted_by')->nullable();
        });

        Blueprint::macro('is', function ($key, $default = true, $prefix = 'is_') {
            return $this->boolean($prefix.$key)->default($default)->comment('Is it '.$key.'?');
        });

        Builder::macro('search', function ($fields, $keyword) {
            $this->where(function ($q) use ($fields, $keyword) {
                foreach ($fields as $index => $field) {
                    $q->orWhere($field, 'like', '%'.$keyword.'%');
                }
            });

            return $this;
        });



        $this->app->bind(
            \Illuminate\Pagination\LengthAwarePaginator::class,
            \Rekamy\LaravelCore\Override\LengthAwarePaginator::class
        );

        $this->app->bind(
            \Prettus\Repository\Criteria\RequestCriteria::class,
            \Rekamy\LaravelCore\Criteria\RequestCriteria::class
        );
    }
}
